<?php
/**
 * Plugin Name: WooCommerce LightSpeed POS
 * Plugin URI: http://woothemes.com/products/woocommerce-lightspeed-pos/
 * Description: WooCommerce LightSpeed POS allows you to integrate and import inventory from LightSpeed to WooCommerce and sync inventory across both systems. You need to sign up for a LightSpeed account to use this extension.
 * Version: 1.5.4
 * Author: WooCommerce
 * Author URI: http://woocommerce.com/
 * Developer: Rafi Y/RafiLabs
 * Developer URI: http://rafilabs.com/
 * Text Domain: woocommerce-lightspeed-pos
 * Domain Path: /languages
 * Requires at least: 4.5
 * Tested up to: 4.9.4
 *
 * Woo: 1210883:c839f6c97c36a944de1391cf5086874d
 * WC requires at least: 2.5.2
 * WC tested up to: 3.3.5
 * Stable tag: 1.5.4
 *
 * Copyright: © 2018 RafiLabs.
 * License: GNU General Public License v3.0
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Required functions
 */
if ( ! function_exists( 'woothemes_queue_update' ) ) {
	require_once( 'woo-includes/woo-functions.php' );
}

require_once( 'includes/wc-ls-constants.php' );

// Scripts & Migrations
foreach ( glob( dirname( __FILE__ ) . "/db/scripts/*.php" ) as $filename) { include $filename; }
foreach ( glob( dirname( __FILE__ ) . "/db/migrations/*.php" ) as $filename) {
	$classes = get_declared_classes();
	include $filename;
	$diff = array_diff( get_declared_classes(), $classes );
	$migration_class = reset( $diff );
	if( method_exists( $migration_class, 'run_migration') ) {
		$migration = new $migration_class();
		$migration->run_migration();
	}
}

/**
 * Plugin updates
 */
woothemes_queue_update( plugin_basename( __FILE__ ), 'c839f6c97c36a944de1391cf5086874d', '1210883' );

// Unschedule sync schedule if the plugin disabled
register_deactivation_hook( __FILE__, array( 'LSI_Synchronizer', 'wclsi_unschedule_sync' ) );

if ( ! class_exists( 'WC_LS_Integration' ) ):

	class WC_LS_Integration {

		public function __construct() {
			add_action( 'woocommerce_loaded', array( $this, 'init' ) );
			add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), array( $this, 'wclsi_action_links' ) );
		}

		function wclsi_action_links( $links ) {
			$href = add_query_arg(
				array(
					'page'    => 'wc-settings',
					'tab'     => 'integration',
					'section' => 'lightspeed-integration'
				),
				admin_url( 'admin.php' )
			);

			$links[] = sprintf( '<a href="%s">%s</a>', $href, __( 'Settings', 'woocommerce-lightspeed-pos' ) );

			return $links;
		}

		public function init() {

			// Checks if WooCommerce is installed.
			if ( class_exists( 'WC_Integration' ) ) {

				// Define global accessors
				$WCLSI_API       = null;
				$WCLSI_PRODS     = null;
				$WCLSI_SYNCER    = null;
				$WCLSI_WC_Logger = null;

				include_once 'includes/class-wp-mosapicall.php';
				include_once 'includes/class-wp-moscurl.php';
				include_once 'includes/wc-ls-core-functions.php';
				include_once 'includes/class-wclsi-item-image.php';
				include_once 'includes/class-wclsi-item-price.php';
				include_once 'includes/class-wclsi-item-shop.php';
				include_once 'includes/class-wclsi-item-e-commerce.php';
				include_once 'includes/class-wclsi-lightspeed-prod.php';
				include_once 'includes/class-wclsi-lightspeed-item-attribute-set.php';
				include_once 'class-wc-ls-upgrade-routines.php';
				include_once 'class-wc-ls-init-settings.php';
				include_once 'class-wc-ls-import-cats.php';
				include_once 'class-wc-ls-import-prod.php';
				include_once 'class-wc-ls-import-page.php';
				include_once 'class-wc-ls-import-table.php';
				include_once 'class-wc-ls-sync.php';

				// Register the integration.
				add_filter( 'woocommerce_integrations', array( $this, 'add_integration' ) );

				// Perform upgrade routines if necessary
				WC_LS_Upgrade_Routines::perform_upgrades();
			}
		}

		/**
		 * Add a new integration to WooCommerce.
		 */
		public function add_integration( $integrations ) {
			$integrations[] = 'LSI_Init_Settings';
			return $integrations;
		}
	}

	new WC_LS_Integration( __FILE__ );

endif;
