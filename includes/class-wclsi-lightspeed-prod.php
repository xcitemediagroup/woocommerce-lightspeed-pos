<?php
class WCLSI_Lightspeed_Prod{

	private $id = 0;
	public $item_id = null;
	public $item_matrix_id = null;
	private $item_e_commerce = array();
	private $item_shops = array();
	private $prices = array();
	private $images = array();
	private $persisted = false;

	/**
	 * @var WCLSI_Lightspeed_Prod[]
	 */
	private $variations = array();

	/**
	 * WCLSI_Lightspeed_Prod constructor.
	 *
	 * You can create a new Lighspeed Product by passing in a Lightspeed API object
	 * or an ID for a Lightspeed Product object that already exists in the `<wp_prefix>_wclsi_items` table.
	 *
	 * @param $id
	 */
	function __construct( $id = 0 ) {
		if ( $id > 0 ) {
			$this->init_via_id( $id );
		}
	}

	/**
	 * Lazy load member properties
	 */
	function __get( $name ) {
		switch ( true ) {
			case ($name == 'id'):
				return $this->id;
			case ($name == 'item_e_commerce' || $name == 'ItemECommerce'):
				return $this->get_item_e_commerce();
			case ($name == 'item_shops' || $name == 'ItemShops'):
				return $this->get_item_shops();
			case ($name == 'prices' || $name == 'Prices'):
				return $this->get_item_prices();
			case ($name == 'images' || $name == 'Images'):
				return $this->get_item_images();
			case ($name == 'variations'):
				return $this->get_variations();
			case ($name == 'persisted'):
				return $this->persisted;
		}

		return null;
	}

	/******* init methods *******/

	function init_via_id( $id, $clear_cache = false ) {
		$this->init_via_col_and_id( 'id', $id, $clear_cache );
	}

	function init_via_item_id( $item_id, $clear_cache = false ) {
		$this->init_via_col_and_id( 'item_id', $item_id, $clear_cache );
	}

	function init_via_item_matrix_id( $item_matrix_id, $clear_cache = false ) {
		$this->init_via_id(self::get_mysql_id(null, $item_matrix_id), $clear_cache);
	}

	function init_via_wc_prod_id( $wc_prod_id, $clear_cache = false ) {
		$this->init_via_col_and_id('wc_prod_id', $wc_prod_id, $clear_cache );
	}

	function reload() {
		if ( $this->id > 0 ) {
			$this->init_via_id( $this->id, true );

			if ( !empty( $this->get_variations() ) ) {
				foreach( $this->variations as $variation ) {
					$variation->reload();
				}
			}
		}
	}

	/******* get methods *******/

	private function get_variations() {
		if ( !empty( $this->variations ) ) { return $this->variations; }

		if ( !$this->persisted ) { return null; }

		if( $this->is_matrix_product() ) {
			global $wpdb, $WCLSI_ITEM_TABLE;
			$table_name = $wpdb->prefix . $WCLSI_ITEM_TABLE;
			$variation_ids = $wpdb->get_col(
				"SELECT id FROM $table_name WHERE item_matrix_id=$this->item_matrix_id AND item_id>0"
			);

			foreach ( $variation_ids as $id ) {
				$this->variations[] = new WCLSI_Lightspeed_Prod( $id );
			}
		}

		return $this->variations;
	}

	private function get_item_e_commerce() {
		if ( is_array( $this->item_e_commerce ) && isset( $this->item_e_commerce[0] ) ) {
			return $this->item_e_commerce[0];
		}

		if ( !$this->persisted ) { return null; }

		global $wpdb, $WCLSI_ITEM_E_COMMERCE_TABLE;
		$table_name = $wpdb->prefix . $WCLSI_ITEM_E_COMMERCE_TABLE;
		$this->item_e_commerce = $wpdb->get_results( "SELECT * FROM $table_name WHERE wclsi_item_id = $this->id" );

		// Some products may not have an item_e_commerce object associated with them
		if( empty( $this->item_e_commerce ) ) { return array(); }

		return $this->item_e_commerce[0];
	}

	private function get_item_shops() {
		if ( !empty( $this->item_shops ) ) { return $this->item_shops; }

		if ( !$this->persisted ) { return null; }

		global $wpdb, $WCLSI_ITEM_SHOP_TABLE;
		$table_name = $wpdb->prefix . $WCLSI_ITEM_SHOP_TABLE;
		$results = $wpdb->get_results( "SELECT * FROM $table_name WHERE wclsi_item_id = $this->id" );
		if( !empty( $results ) ) {
			foreach( $results as $key => $item_shop ) {
				$item_shop->metadata = maybe_unserialize( $item_shop->metadata );
				$results[ $key ] = $item_shop;
			}
		}
		$this->item_shops = $results;
		return $this->item_shops;
	}

	private function get_item_prices() {
		if ( !empty( $this->prices ) ) { return $this->prices; }

		if ( !$this->persisted ) { return null; }

		global $wpdb, $WCLSI_ITEM_PRICES_TABLE;
		$table_name = $wpdb->prefix . $WCLSI_ITEM_PRICES_TABLE;
		$this->prices = $wpdb->get_results( "SELECT * FROM $table_name WHERE wclsi_item_id = $this->id" );
		return $this->prices;
	}

	private function get_item_images() {
		if ( !empty( $this->images ) ) { return $this->images; }

		if ( !$this->persisted ) { return null; }

		global $wpdb, $WCLSI_ITEM_IMAGES_TABLE;
		$table_name = $wpdb->prefix . $WCLSI_ITEM_IMAGES_TABLE;
		$this->images = $wpdb->get_results( "SELECT * FROM $table_name WHERE wclsi_item_id = $this->id" );
		return $this->images;
	}

	/******* Update Methods *******/

	/**
	 * @return int|WP_Error
	 */
	public function update_via_api() {
		global $WCLSI_API, $WCLSI_WC_Logger;

		$api_data = wclsi_get_prod_api_path( $this );
		$endpoint = 'Account/' . $WCLSI_API->ls_account_id . $api_data['path'];

		$result = $WCLSI_API->make_api_call( $endpoint, 'Read', $api_data['params'] );

		if( is_wp_error( $result ) ) {
			return $result;
		}

		if ( $this->is_matrix_product() ) {
			$new_ls_api_item = $result->ItemMatrix;
		} elseif ( $this->is_simple_product() || $this->is_variation_product() ) {
			$new_ls_api_item = $result->Item;
		} else {
			$error_msg = __( 'Error: invalid Lightspeed Product. Could not complete update operation!', 'woocommerce-lightspeed-pos' );
			if( is_admin() ) {
				add_settings_error(
					'wclsi_settings',
					'invalid_ls_object',
					$error_msg
				);
			}
			return new WP_Error( 'invalid_ls_object', $error_msg, $result );
		}

		$this->copy_wclsi_fields( $new_ls_api_item );

		if( WP_DEBUG ) {
			$WCLSI_WC_Logger->add(
				WCLSI_WC_LOGGER_HANDLE,
				'Lightspeed API call result: ' . PHP_EOL . print_r( $new_ls_api_item, true )
			);
		}

		if ( $this->is_matrix_product() ) {
			global $WCLSI_SINGLE_PROD_SEARCH_PARAMS;

			$endpoint = 'Account/' . $WCLSI_API->ls_account_id . '/Item/';
			$search_params = array(
				'load_relations' => json_encode( $WCLSI_SINGLE_PROD_SEARCH_PARAMS ),
				'itemMatrixID' => $this->item_matrix_id
			);

			$new_variations = $WCLSI_API->make_api_call( $endpoint, 'Read', $search_params );

			if( is_wp_error( $new_variations ) ) {
				return $new_variations;
			}

			if ( !empty( $new_variations->Item ) ) {
				if ( is_array( $new_variations->Item ) ) {
					$new_variations = $new_variations->Item;
				} else if ( is_object( $new_variations->Item ) ) {
					$new_variations = array( $new_variations->Item );
				}

				$this->update_variations( $new_variations );
			}
		}

		self::update_via_api_item( $new_ls_api_item, $this );

		return $this->id;
	}

	/**
	 * This function will accommodate new variations that have only been added
	 * in Lightspeed after the created_at date of the record
	 *
	 * @param $new_variations
	 */
	private function update_variations( $new_variations ) {

		foreach( $new_variations as $ls_api_item ) {
			$item_mysql_id = self::get_mysql_id( $ls_api_item->itemID, $ls_api_item->itemMatrixID );

			/**
			 * If the variation exists, then use existing update() function,
			 * otherwise insert a new record
			 */
			if( $item_mysql_id > 0 ) {
				$old_item = new WCLSI_Lightspeed_Prod($item_mysql_id);
				self::update_via_api_item( $ls_api_item, $old_item );
			} else {
				self::insert_ls_api_item( $ls_api_item );
			}
		}
	}

	/******* Update Methods *******/

	public static function update_via_api_item( $new_item, $old_item ) {
		global $wpdb, $WCLSI_ITEM_TABLE;

		$new_item->wclsi_last_sync_date = current_time('mysql');

		$update_args = self::get_item_mysql_args( $new_item );
		$where_args['id'] = $old_item->id;

		$unset_fields = array(
			'created_at',
			'wc_prod_id',
			'wclsi_import_date',
			'wclsi_is_synced'
		);

		foreach( $unset_fields as $field ) {
			if( array_key_exists( $field, $update_args ) ) {
				unset( $update_args[ $field ] );
			}
		}

		$where_format  = array( '%d' );
		$update_format = array(
			'%d', '%d', '%s', '%s', '%s', '%f',
			'%f', '%d', '%d', '%d', '%s', '%d',
			'%s', '%d', '%d', '%d', '%s', '%s',
			'%d', '%d', '%d', '%d', '%d', '%d',
			'%d', '%d', '%s', '%s', '%s', '%d',
			'%s', '%s'
		);

		$table = $wpdb->prefix . $WCLSI_ITEM_TABLE;
		$wpdb->update( $table, $update_args, $where_args, $update_format, $where_format );

		WCLSI_Item_Price::update_item_prices( $new_item, $old_item );
		WCLSI_Item_Shop::update_item_shops( $new_item, $old_item );
		WCLSI_Item_Image::update_item_images( $new_item, $old_item );

		if( isset( $new_item->ItemECommerce ) ) {
			WCLSI_Item_E_Commerce::update_item_e_commerce( $new_item->ItemECommerce, $old_item->id );
		}
	}

	/******* Insert Methods *******/

	/**
	 * @param $item
	 * @param null $custom_id
	 * @param null $custom_value
	 *
	 * @return int|null|string
	 */
	public static function insert_ls_api_item( $item, $custom_id = null, $custom_value = null ){
		global $wpdb, $WCLSI_ITEM_TABLE;

		$result = self::item_exists( $item );
		if( $result > 0 ) {
			return $result;
		}

		$args = self::get_item_mysql_args( $item, $custom_id, $custom_value );

		$formatting = array(
			'%d', '%d', '%s', '%s', '%s', '%f',
			'%f', '%d', '%d', '%d', '%s', '%d',
			'%s', '%d', '%d', '%d', '%s', '%s',
			'%d', '%d', '%d', '%d', '%d', '%d',
			'%d', '%d', '%s', '%s', '%s', '%d',
			'%s', '%s', '%d', '%s', '%s', '%d'
		);

		$table = $wpdb->prefix . $WCLSI_ITEM_TABLE;

		$wpdb->insert( $table, $args, $formatting );

		$item->wclsi_item_id = $wpdb->insert_id;

		if( isset( $item->ItemECommerce ) ) {
			WCLSI_Item_E_Commerce::insert_item_e_commerce( $item->ItemECommerce, $item->wclsi_item_id );
		}

		WCLSI_Item_Shop::insert_item_shops( $item );
		WCLSI_Item_Price::insert_item_prices( $item );
		WCLSI_Item_Image::insert_item_images( $item );

		return $item->wclsi_item_id;
	}

	/******* Delete Methods *******/

	function delete( $delete_variations = false ) {
		global $wpdb, $WCLSI_ITEM_TABLE, $WCLSI_RELATED_ITEM_TABLES;
		$item_table = $wpdb->prefix . $WCLSI_ITEM_TABLE;

		foreach ( $WCLSI_RELATED_ITEM_TABLES as $table_name ) {
			$table = $wpdb->prefix . $table_name;
			$wpdb->query( "DELETE FROM $table WHERE wclsi_item_id = $this->id" );
		}

		if( $this->is_matrix_product() && $delete_variations ) {
			$this->delete_variations();
		}

		$wpdb->query( "DELETE FROM $item_table WHERE id = $this->id" );
	}

	function delete_variations() {
		if( empty( $this->get_variations() ) ) { return; }

		foreach( $this->variations as $variation ) {
			$variation->delete();
		}
	}

	/******* Utility Methods *******/

	private static function get_item_mysql_args( $item, $custom_id = null, $custom_value = null ) {

		$tags = null;
		if( isset( $item->Tags ) ) {
			if ( is_string( $item->Tags->tag ) ) {
				$tags = array( $item->Tags->tag );
			} else if ( is_array( $item->Tags->tag ) ) {
				$tags = $item->Tags->tag;
			}
		}

		$custom_field_values = null;
		if( isset( $item->CustomFieldValues ) ) {
			if ( is_object( $item->CustomFieldValues ) ) {
				$custom_field_values = array( $item->CustomFieldValues );
			} else if ( is_array( $item->CustomFieldValues ) ) {
				$custom_field_values = $item->CustomFieldValues;
			}
		}

		$args = array(
			'item_id'               => isset( $item->itemID ) ? $item->itemID : null,
			'item_matrix_id'        => isset( $item->itemMatrixID ) ? $item->itemMatrixID : null,
			'system_sku'            => isset( $item->systemSku ) ? $item->systemSku : null,
			'custom_sku'            => isset( $item->customSku ) ? $item->customSku : null,
			'manufacturer_sku'      => isset( $item->manufacturerSku ) ? $item->manufacturerSku : null,
			'default_cost'          => isset( $item->defaultCost ) ? $item->defaultCost : null,
			'avg_cost'              => isset( $item->avgCost ) ? $item->avgCost : null,
			'discountable'          => isset( $item->discountable ) && $item->discountable == 'true' ? true : false,
			'tax'                   => isset( $item->tax ) && $item->tax == 'true' ? true : false,
			'archived'              => isset( $item->archived ) && $item->archived == 'true' ? true : false,
			'item_type'             => isset( $item->itemType ) ? $item->itemType : null,
			'serialized'            => isset( $item->serialized ) ? $item->serialized : null,
			'description'           => isset( $item->description ) ? $item->description : null,
			'model_year'            => isset( $item->modelYear ) ? $item->modelYear : null,
			'upc'                   => isset( $item->upc ) ? $item->upc : null,
			'ean'                   => isset( $item->ean ) ? $item->ean : null,
			'create_time'           => isset( $item->createTime ) ? $item->createTime : null,
			'time_stamp'            => isset( $item->timeStamp ) ? $item->timeStamp : null,
			'category_id'           => isset( $item->categoryID ) ? $item->categoryID : null,
			'tax_class_id'          => isset( $item->taxClassID ) ? $item->taxClassID : null,
			'department_id'         => isset( $item->departmentID ) ? $item->departmentID : null,
			'manufacturer_id'       => isset( $item->manufacturerID ) ? $item->manufacturerID : null,
			'season_id'             => isset( $item->seasonID ) ? $item->seasonID : null,
			'default_vendor_id'     => isset( $item->defaultVendorID ) ? $item->defaultVendorID : null,
			'item_e_commerce_id'    => isset( $item->itemECommerceID ) ? $item->itemECommerceID : null,
			'item_attribute_set_id' => isset( $item->itemAttributeSetID ) ? $item->itemAttributeSetID : null,
			'item_attributes'       => isset( $item->ItemAttributes ) ? maybe_serialize( $item->ItemAttributes ) : null,
			'tags'                  => maybe_serialize( $tags ),
			'custom_field_values'   => maybe_serialize( $custom_field_values ),
			'custom_id'             => $custom_id,
			'custom_value'          => maybe_serialize( $custom_value ),
			'created_at'            => isset( $item->created_at ) ? $item->created_at : current_time('mysql'),
			'wc_prod_id'            => isset( $item->wc_prod_id ) ? $item->wc_prod_id : null,
			'wclsi_import_date'     => isset( $item->wclsi_import_date ) ? $item->wclsi_import_date : null,
			'wclsi_last_sync_date'  => isset( $item->wclsi_last_sync_date ) ? $item->wclsi_last_sync_date : null,
			'wclsi_is_synced'       => isset( $item->wclsi_is_synced ) ? $item->wclsi_is_synced : null
		);

		wclsi_format_empty_vals( $args );

		return $args;
	}

	private function init_via_col_and_id( $column_name, $id, $clear_cache = false ) {
		global $wpdb, $WCLSI_ITEM_TABLE;

		if( $clear_cache ) {
			$wpdb->flush();
			$this->item_e_commerce = null;
			$this->item_shops = null;
			$this->prices = null;
			$this->images = null;
			$this->variations = null;
		}

		$table_name = $wpdb->prefix . $WCLSI_ITEM_TABLE;
		$result = $wpdb->get_row( "SELECT * FROM $table_name WHERE $column_name = $id", ARRAY_A );

		if ( !empty( $result ) ) {
			foreach ( $result as $property => $value ) {
				$this->{$property} = maybe_unserialize( $value );
			}

			$this->persisted = true;
		}

		$booleans = array( 'discountable', 'tax', 'archived', 'serialized' );
		foreach( $booleans as $bool_property ) {
			$this->{$bool_property} = (bool) $this->{$bool_property};
		}
	}

	public static function get_mysql_id( $item_id, $item_matrix_id ) {
		global $wpdb, $WCLSI_ITEM_TABLE;
		$table_name = $wpdb->prefix . $WCLSI_ITEM_TABLE;

		if ( empty( $item_id ) && $item_matrix_id >= 0 ) {
			return $wpdb->get_var( "SELECT id FROM $table_name WHERE item_id IS NULL AND item_matrix_id = $item_matrix_id" );
		} else if ( $item_id > 0 && $item_matrix_id >= 0 ) {
			return $wpdb->get_var( "SELECT id FROM $table_name WHERE item_id = $item_id AND item_matrix_id = $item_matrix_id" );
		}

		return 0;
	}

	function is_matrix_product() {
		return is_null( $this->item_id ) && $this->item_matrix_id > 0;
	}

	function is_simple_product() {
		return $this->item_id > 0 && $this->item_matrix_id == 0;
	}

	function is_variation_product() {
		return $this->item_id > 0 && $this->item_matrix_id > 0;
	}

	/**
	 * Uses new 'wclsi_' prefix for wclsi-related columns
	 * @param $ls_api_item
	 */
	public static function map_wclsi_fields( &$ls_api_item ) {
		if( isset( $ls_api_item->last_import ) ) {
			$ls_api_item->wclsi_import_date = $ls_api_item->last_import;
		}

		if ( isset( $ls_api_item->last_sync_date) ) {
			$ls_api_item->wclsi_last_sync_date = $ls_api_item->last_sync_date;
		}

		if( isset( $ls_api_item->wc_prod_id ) ) {
			$is_synced = get_post_meta( $ls_api_item->wc_prod_id, WCLSI_SYNC_POST_META, true );
			$ls_api_item->wclsi_is_synced = $is_synced;
		}
	}

	/**
	 * Removes the wc_prod_id association
	 */
	function clear_wc_prod_association(){
		global $wpdb, $WCLSI_ITEM_TABLE;

		if ( $this->wc_prod_id > 0 ) {
			$table = $wpdb->prefix . $WCLSI_ITEM_TABLE;
			$wpdb->query("UPDATE $table SET wclsi_is_synced = NULL where wc_prod_id = $this->wc_prod_id");
			$wpdb->query("UPDATE $table SET wclsi_last_sync_date = NULL where wc_prod_id = $this->wc_prod_id");
			$wpdb->query("UPDATE $table SET wclsi_import_date = NULL where wc_prod_id = $this->wc_prod_id");
			$wpdb->query("UPDATE $table SET wc_prod_id = NULL where wc_prod_id = $this->wc_prod_id");

			if( $this->is_matrix_product() && !empty( $this->get_variations() ) ) {
				foreach( $this->variations as $variation ) {
					$variation->clear_wc_prod_association();
				}
			}
		}
	}

	function update_column( $column_name, $value ) {
		if ( !$this->persisted ) { return false; }

		global $wpdb, $WCLSI_ITEM_TABLE;
		$table = $wpdb->prefix . $WCLSI_ITEM_TABLE;

		return $wpdb->update(
			$table,
			array( $column_name => maybe_serialize( $value ) ),
			array( 'id' => $this->id ),
			array( wclsi_get_wpdb_field_type( $value ) ),
			array( '%d' )
		);
	}

	/**
	 * Copies over wclsi-related fields from an exist local db record to a
	 * ls_api_item that was just pulled from Lightspeed's api
	 * @param $ls_api_item
	 */
	function copy_wclsi_fields( &$ls_api_item ) {
		$ls_api_item->wclsi_import_date = $this->wclsi_import_date;
		$ls_api_item->wclsi_last_sync_date = $this->wclsi_last_sync_date;
		$ls_api_item->wclsi_is_synced = $this->wclsi_is_synced;
		$ls_api_item->wc_prod_id = $this->wc_prod_id;
	}

	/**
	 * @param $ls_api_item
	 *
	 * @return int|null|string
	 */
	public static function item_exists( $ls_api_item ){

		$item_id = isset( $ls_api_item->itemID ) ? $ls_api_item->itemID : null;
		$item_matrix_id = isset( $ls_api_item->itemMatrixID ) ? $ls_api_item->itemMatrixID : 0;

		return self::get_mysql_id( $item_id, $item_matrix_id );
	}

	function update_wclsi_last_sync_date() {
		global $wpdb, $WCLSI_ITEM_TABLE;
		$table = $wpdb->prefix . $WCLSI_ITEM_TABLE;

		$wpdb->update(
			$table,
			array( 'wclsi_last_sync_date' => current_time('mysql') ),
			array( 'id' => $this->id ),
			array( '%s' ),
			array( '%d' )
		);
	}
}
