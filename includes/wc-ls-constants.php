<?php
define( 'WC_LSI_VERSION', '1.5.4' );
define( 'WCLSI_DB_VERSION_OPTION', 'wclsi_db_version' );
define( 'WC_LSI_NAME', __( 'WooCommerce Lightspeed Integration', 'woocommerce-lightspeed-pos' ) );
define( 'WCLSI_MENU_NAME', __( 'Lightspeed', 'woocommerce-lightspeed-pos' ) );
define( 'WCLSI_ADMIN_PAGE_TITLE', __( 'Lightspeed', 'woocommerce-lightspeed-pos' ) );
define( 'WCLSI_ADMIN_URL', admin_url( 'admin.php?page=lightspeed-import-page' ) );
define( 'WCLSI_ADMIN_SETTINGS_URL', admin_url( 'admin.php?page=wc-settings&tab=integration&section=lightspeed-integration' ) );
define( 'WCLSI_CAT_CHUNK_PREFIX', 'wclsi_cat_chunk_' );
define( 'WCLSI_TOTAL_CAT_CHUNKS', 'wclsi_total_cat_chunks' );
define( 'WCLSI_PROD_CHUNK_PREFIX', 'wclsi_prod_chunk_' );
define( 'WCLSI_MATRIX_CHUNK_PREFIX', 'wclsi_matrix_chunk_' );
define( 'WCLSI_TOTAL_MATRIX_CHUNKS', 'wclsi_matrix_chunks' );
define( 'WCLSI_TOTAL_PROD_CHUNKS', 'wclsi_total_chunks' );
define( 'WCLSI_ATTRS_CACHE', 'wclsi_attrs_cache' );
define( 'WCLSI_SYNC_POST_META', '_wclsi_sync' );
define( 'WCLSI_LS_OBJ_POST_META', '_wclsi_ls_obj' );
define( 'WCLSI_LAST_SYNC_TIMESTAMP', 'wclsi_last_sync_timestamp' );
define( 'WCLSI_INVENTORY_SHOP_ID', 'ls_inventory_store' );
define( 'WCLSI_WC_LOGGER_HANDLE', 'wclsi-debugger.log' );
define( 'WCLSI_SCREEN_ID', 'woocommerce_page_lightspeed-import-page' );
define( 'WCLSI_SINGLE_ITEM_ID_POST_META', '_wclsi_item_id' );
define( 'WCLSI_MATRIX_ID_POST_META', '_wclsi_matrix_id' );
define( 'WCLSI_REFRESH_CONNECTOR_URL', 'https://connect.woocommerce.com/renew/lightspeed/' );
define( 'WCLSI_LAST_LOAD_TIMESTAMP', 'wclsi_load_timestamp' );
define( 'WCLSI_NEW_PROD_TRANSIENT', 'wclsi-sync-new-prods' );
define( 'WCLSI_LS_TO_WC_AUTOLOAD', 'ls_to_wc_auto_load' );
define( 'WCLSI_WC_SELECTIVE_SYNC', 'wclsi_wc_selective_sync' );
define( 'WCLSI_LS_SELECTIVE_SYNC', 'wclsi_ls_selective_sync' );
define( 'WCLSI_DOCS_URL', 'https://docs.woothemes.com/document/woocommerce-lightspeed-pos/' );

global $WCLSI_ITEM_TABLE, $WCLSI_ITEM_IMAGES_TABLE, $WCLSI_ITEM_SHOP_TABLE, $WCLSI_ITEM_PRICES_TABLE,
	   $WCLSI_ITEM_E_COMMERCE_TABLE, $WCLSI_ITEM_CATEGORIES_TABLE, $WCLSI_ITEM_ATTRIBUTE_SETS_TABLE,
	   $WCLSI_RELATED_ITEM_TABLES, $WCLSI_SINGLE_PROD_SEARCH_PARAMS, $WCLSI_MATRIX_PROD_SEARCH_PARAMS,
	   $WCLSI_ALL_TABLES, $WCLSI_objectL10n, $WC_PROD_SELECTIVE_SYNC_PROPERTIES, $LS_PROD_SELECTIVE_SYNC_PROPERTIES;

$WCLSI_ITEM_TABLE                = 'wclsi_items';
$WCLSI_ITEM_IMAGES_TABLE         = 'wclsi_item_images';
$WCLSI_ITEM_SHOP_TABLE           = 'wclsi_item_shops';
$WCLSI_ITEM_PRICES_TABLE         = 'wclsi_item_prices';
$WCLSI_ITEM_E_COMMERCE_TABLE     = 'wclsi_item_e_commerce';
$WCLSI_ITEM_CATEGORIES_TABLE     = 'wclsi_item_categories';
$WCLSI_ITEM_ATTRIBUTE_SETS_TABLE = 'wclsi_item_attribute_sets';

$WCLSI_RELATED_ITEM_TABLES = array(
	$WCLSI_ITEM_IMAGES_TABLE,
	$WCLSI_ITEM_SHOP_TABLE,
	$WCLSI_ITEM_PRICES_TABLE,
	$WCLSI_ITEM_E_COMMERCE_TABLE
);

$WCLSI_ALL_TABLES = array(
	$WCLSI_ITEM_TABLE,
	$WCLSI_ITEM_SHOP_TABLE,
	$WCLSI_ITEM_PRICES_TABLE,
	$WCLSI_ITEM_IMAGES_TABLE,
	$WCLSI_ITEM_E_COMMERCE_TABLE,
	$WCLSI_ITEM_CATEGORIES_TABLE,
	$WCLSI_ITEM_ATTRIBUTE_SETS_TABLE
);

$WCLSI_SINGLE_PROD_SEARCH_PARAMS = array(
	"ItemShops",
	"ItemECommerce",
	"Images",
	"Tags",
	"ItemAttributes",
	"CustomFieldValues"
);
$WCLSI_MATRIX_PROD_SEARCH_PARAMS = array(
	"ItemECommerce",
	"Tags",
	"Images"
);

$WCLSI_objectL10n = array(
	'reload_confirm'  => __( 'Products have already been loaded, are you sure you want to reload them?', 'woocommerce-lightspeed-pos' ),
	'importing_prods' => __( 'Importing products ... ', 'woocommerce-lightspeed-pos' ),
	'dont_close'      => __( 'This may take a while... please do not close this window while products are being imported or synced!', 'woocommerce-lightspeed-pos' ),
	'done_importing'  => sprintf( __( 'Import completed! Click <a href="%s">here</a> to view imported products', 'woocommerce-lightspeed-pos' ), admin_url( 'edit.php?post_type=product' ) ),
	'no_prods_error'  => __( 'Error: No products to import!', 'woocommerce-lightspeed-pos' ),
	'try_again'       => __( 'A connection could not be made to Lightspeed, please try again.', 'woocommerce-lightspeed-pos' ),
	'sync_error'      => __( 'Please import this item before attempting to sync it!', 'woocommerce-lightspeed-pos' ),
	'sync_success'    => __( 'Product successfully added to sync schedule.', 'woocommerce-lightspeed-pos' ),
	'relink_success'  => __( 'Relink successful! The associated Lightspeed product should now be viewable on the <a href="' . WCLSI_ADMIN_URL . '">Lightspeed Import page</a>.', 'woocommerce-lightspeed-pos' ),
	'sync_remove'     => __( 'Product successfully removed from sync schedule.', 'woocommerce-lightspeed-pos' ),
	'syncing'         => __( 'Syncing...', 'woocommerce-lightspeed-pos' ),
	'man_sync_success' => __( 'Successfully synced!', 'woocommerce-lightspeed-pos' ),
	'generic_error'    => __( 'Something went wrong! Please try again later.', 'woocommerce-lightspeed-pos' ),
	'provide_account_id'  => __( 'Please provide an account ID before submitting!', 'woocommerce-lightspeed-pos' ),
	'api_connection_good' => __( 'Connection successful!', 'woocommerce-lightspeed-pos' ),
	'api_connection_bad'  => __( 'A connection could not be made to your Lightspeed account!', 'woocommerce-lightspeed-pos' ),
	'incomplete_load' => __( 'Error: something went wrong with loading products from Lightspeed! Refresh to see if some products loaded successfully.', 'woocomerce-lightspeed-pos'),
	'loading_matrix_products' => __( 'Loading matrix products', 'woocomerce-lightspeed-pos'),
	'loading_categories' => __( 'Loading categories', 'woocomerce-lightspeed-pos'),
	'loading_item_attrs' => __( 'Loading item attribute sets', 'woocomerce-lightspeed-pos'),
	'upgrade_complete' => __( 'Upgrade successfully completed!', 'woocommerce-lightspeed-pos'),
	'bad_sync_to_ls' => __( 'The synchronization to Lightspeed experienced some issues. Please log into Lightspeed and verify your product was synced properly.')
);

$WC_PROD_SELECTIVE_SYNC_PROPERTIES = array(
	'name' => 'Name',
	'description' => 'Description',
	'short_description' => 'Short Description',
	'sku' => 'SKU',
	'regular_price' => 'Regular Price',
	'sale_price' => 'Sale Price',
	'weight' => 'Weight',
	'length' => 'Length',
	'width' => 'Width',
	'height' => 'Height',
	'stock_quantity' => 'Stock Quantity',
	'images' => 'Images (both featured and gallery images)'
);

$LS_PROD_SELECTIVE_SYNC_PROPERTIES = array(
	'description' => 'Name',
	'longDescription' => 'Description',
	'shortDescription' => 'Short Description',
	'customSku' => 'SKU (maps to Custom SKU in LightSpeed)',
	'regular_price' => 'Regular Price',
	'sale_price' => 'Sale Price',
	'weight' => 'Weight',
	'length' => 'Length',
	'width' => 'Width',
	'height' => 'Height',
	'stock_quantity' => 'Stock Quantity'
);
