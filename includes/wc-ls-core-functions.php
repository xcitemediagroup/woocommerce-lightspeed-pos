<?php
/**
 * Core functions useful for various parts of the plugin
 */

/**
 * Add logger for debugging
 * pluggable.php is needed for the logger, which is loaded before
 * 'plugins_loaded' action - 'woocommerce_loaded' is laoded before pluggable.php
 */
function wclsi_load_wc_logger() {
	global $WCLSI_WC_Logger;
	$WCLSI_WC_Logger = new WC_Logger();
}
add_action('plugins_loaded', 'wclsi_load_wc_logger');

/**
 * Creates an new wp_attachment based off of an existing file. The file
 * has to be located in wp-content/uploads in order for the function to work.
 *
 * @param $filename - path and name of the file to attach
 * @param $parent_id - PostID to attach the attachment to
 * @param $content
 * @param $auth_id - Author of attachment
 * @param $include_files - Whether to include image.php and media.php; may be inefficient if
 *                         the function gets called frequently
 *
 * @return int - The attachment ID
 */
function wc_ls_create_attachment($filename, $parent_id, $content = '', $auth_id = null, $include_files = true ) {

	$auth_id = empty( $auth_id ) ? get_current_user_id() : $auth_id;

	$wp_filetype = wp_check_filetype(basename($filename), null );
	$wp_upload_dir = wp_upload_dir();
	$post = get_post( $parent_id );
	$attachment = array(
		'guid' => $wp_upload_dir['url'] . '/' . basename( $filename ),
		'post_mime_type' => $wp_filetype['type'],
		'post_title' => get_the_title( $parent_id ),
		'post_content' => empty( $post->post_content ) ? basename( $filename ) : $post->post_content,
		'post_status' => 'inherit',
		'post_author' => $auth_id
	);
	$attach_id = wp_insert_attachment( $attachment, $filename, $parent_id );

	if ( $include_files ) {
		// you must first include the image.php file
		// for the function wp_generate_attachment_metadata() to work
		require_once( ABSPATH . 'wp-admin/includes/image.php' );
		require_once( ABSPATH . 'wp-admin/includes/media.php' );
	}

	$attach_data = wp_generate_attachment_metadata( $attach_id, $filename );
	wp_update_attachment_metadata( $attach_id, $attach_data );
	return $attach_id;
}

/**
 * Returns an array of the LightSpeed prod IDs. If bucket_matrix_prods = true,
 * it will consolidate all the matrix prods and add them to the matrix_ids array,
 * otherwise matrix_ids array will be empty.
 * @return array
 */
function wclsi_get_ls_prod_ids( $bucket_matrix_prods = false ) {
	global $wpdb, $WCLSI_ITEM_TABLE;
	$table = $wpdb->prefix . $WCLSI_ITEM_TABLE;

	$prod_ids = $wpdb->get_col( "SELECT id from $table WHERE item_id > 0 AND item_matrix_id = 0" );
	$matrix_ids = $wpdb->get_col( "SELECT id from $table WHERE item_id IS NULL AND item_matrix_id > 0" );

	return array( 'prod_ids' => $prod_ids, 'matrix_ids' => $matrix_ids );
}

/**
 * Given a LightSpeed product, returns its inventory value.
 * WooCommerce inventory takes precedence.
 * @param $item
 * @return int
 */
function wclsi_get_lightspeed_inventory( $item, $skip_wc_stock = false ) {
	global $WCLSI_API;

	$inventory = null;

	if ( $item->wc_prod_id > 0 && !$skip_wc_stock ) {
		$inventory = (int) get_post_meta( $item->wc_prod_id, '_stock', true );
	} elseif ( !is_null( $item->item_shops ) ) {
		if( isset( $WCLSI_API->settings[ WCLSI_INVENTORY_SHOP_ID ] ) ){
			$shop_id = $WCLSI_API->settings[ WCLSI_INVENTORY_SHOP_ID ];

			foreach( $item->item_shops as $item_shop ) {
				if ( $item_shop->shop_id == $shop_id ) {
					$inventory = (int) $item_shop->qoh;
					break;
				}
			}

			if( is_null( $inventory ) ){
				$inventory = 0;
			}
		} else {
			$inventory = (int) $item->item_shops[0]->qoh;
		}
	} else {
		$inventory = 0;
	}

	$inventory = apply_filters( 'wclsi_get_lightspeed_inventory', $inventory, $item );

	return $inventory;
}

function wclsi_get_lightspeed_price( $item, $default_use_type_id = 1 ) {

	$item_prices = $item->prices;

	if( empty( $item_prices ) ) {
		return 0;
	}

	foreach( $item_prices as $price ) {
		if( $price->use_type_id == $default_use_type_id ) {
			return $price->amount;
		}
	}

	return 0;
}

function wclsi_get_lightspeed_sale_price( $item, $sale_use_type = 'Sale' ) {

	$item_prices = $item->prices;

	if( empty( $item_prices ) ) {
		return null;
	}

	foreach( $item_prices as $price ) {
		if( $price->use_type == $sale_use_type ) {
			return $price->amount;
		}
	}

	return null;
}

/**
 * Returns the matrix products that belong to a specific matrix id
 * @param $matrix_id
 * @param $matrix_prod
 * @return array
 */
function wclsi_get_matrix_prods( $matrix_id, $matrix_prod = null ) {

	$matrix_prods = array();
	if( isset( $matrix_prod->wc_prod_id ) ){
		$variations = get_children(
			array(
				'post_parent' => $matrix_prod->wc_prod_id,
				'post_type'   => 'product_variation'
			)
		);

		if( !empty( $variations ) ){
			foreach ( $variations as $post_id => $variation ) {
				$matrix_prods[] = get_post_meta( $post_id, '_wclsi_ls_obj', true );
			}
		}
	} elseif ( $matrix_id > 0 ) {
		global $wpdb, $WCLSI_ITEM_TABLE;
		$table_name = $wpdb->prefix . $WCLSI_ITEM_TABLE;
		$matrix_ids = $wpdb->get_col("SELECT id FROM $table_name WHERE item_matrix_id=$matrix_id AND item_id>0");
		foreach ( $matrix_ids as $id ) {
			$matrix_prods[] = new WCLSI_Lightspeed_Prod( $id );
		}
	}

	return $matrix_prods;
}

/**
 * Given a LightSpeed prod, returns the API path for that product.
 * Also accepts an array of Lightspeed single item IDs.
 *
 * @param $prod_or_prod_ids object|array
 * @return string
 */
function wclsi_get_prod_api_path( $prod_or_prod_ids ) {
	global $WCLSI_SINGLE_PROD_SEARCH_PARAMS, $WCLSI_MATRIX_PROD_SEARCH_PARAMS;

	$search_string = '';
	$search_params = array();

	if( is_array( $prod_or_prod_ids ) ){
		$search_params = array(
			'load_relations' => json_encode( $WCLSI_SINGLE_PROD_SEARCH_PARAMS ),
			'itemID' => 'IN,' . json_encode( $prod_or_prod_ids )
		);
		$search_string = '/Item';
	} else if ( wclsi_is_simple_product( $prod_or_prod_ids ) ) {
		$search_params = array(
			'load_relations' => json_encode( $WCLSI_SINGLE_PROD_SEARCH_PARAMS ),
		);
		$search_string = '/Item/' . $prod_or_prod_ids->item_id;
	} elseif ( wclsi_is_matrix_product( $prod_or_prod_ids ) ) {
		$search_params = array(
			'load_relations' => json_encode( $WCLSI_MATRIX_PROD_SEARCH_PARAMS ),
		);
		$search_string = '/ItemMatrix/' . $prod_or_prod_ids->item_matrix_id;
	}
	return array( 'path' => $search_string, 'params' => $search_params );
}

/**
 * Predicate function to test whether a lightspeed object is a simple product
 * @param $item
 *
 * @return bool
 */
function wclsi_is_simple_product( $item ) {
	return isset( $item->item_id ) && $item->item_id > 0;
}

/**
 * Predicate function to test whether a lightspeed object is a matrix product
 * @param $item
 *
 * @return bool
 */
function wclsi_is_matrix_product( $item ){
	return !isset( $item->item_id ) && isset( $item->item_matrix_id ) && $item->item_matrix_id > 0;
}

function wclsi_oauth_enabled(){
	return (bool) get_option( 'wclsi_oauth_token' );
}

function wclsi_check_for_oauth(){
	global $WCLSI_API;
	if( !wclsi_oauth_enabled() && isset( $WCLSI_API->settings['api_key'] ) ){
		$class = 'notice notice-warning is-dismissible';
		$wclsi_settings_url = admin_url('admin.php?page=wc-settings&tab=integration&section=lightspeed-integration');

		$message =
			sprintf(
				'<p><b>%s</b></p><p>%s</p><p>%s</p>',
				__("Lightspeed API deprecation warning!", 'woocommerce-lightspeed-pos'),
				__("We've noticed you're using basic authentication to connect to the Lightseed API. ", 'woocommerce-lightspeed-pos'),
				__("Lightspeed is deprecating basic authentication by June 30th, 2016. We recommend you to switch over to the new OAuth connection type as soon as possible!", 'woocommerce-lightspeed-pos')
			);
		$message .= sprintf(
			'<a href="%s">%s</a>',
			$wclsi_settings_url,
			__("Click on the 'Connect to Lightspeed' button in the Lightspeed settings page to get started!", 'woocommerce-lightspeed-pos')
		);

		printf( '<div class="%1$s"><p>%2$s</p></div>', $class, $message );
	} else if ( wclsi_oauth_enabled() && isset( $_GET['lightspeed_access_token'] ) ) {
		$class = 'notice notice-success is-dismissible';
		$message = sprintf(
			'<p><b>%s</b></p><p>%s</p><a href="%s">%s</a>',
			__("Congratulations! You've succesfully authenticated with Lightspeed Retail's API!", 'woocommerce-lightspeed-pos'),
			__("You can now continue with syncing your WooCommerce and Lightspeed products.", 'woocommerce-lightspeed-pos'),
			WCLSI_ADMIN_URL,
			__("Click here to start importing products!")
		);
		printf( '<div class="%1$s"><p>%2$s</p></div>', $class, $message );
	}
}
add_action( 'admin_notices', 'wclsi_check_for_oauth' );

function wclsi_table_exists( $table_name ){
	global $wpdb;
	$sql = $wpdb->prepare( "SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = %s;", $table_name );
	$row = $wpdb->get_row($sql);
	return !is_null($row);
}

function wclsi_table_empty( $table_name ) {
	global $wpdb;
	$sql = esc_sql( "SELECT COUNT(*) as count FROM $table_name" );
	return $wpdb->get_var($sql) == 0;
}

function wclsi_format_empty_vals( &$args ) {
	foreach( $args as $key => $val ) {
		if ( $val == '' ) {
			$args[ $key ] = null;
		}

		if ( is_object( $val ) && count( get_object_vars( $val ) ) == 0 ) {
			$args[ $key ] = null;
		}
	}
}

/**
 * @param $ls_prod
 *
 * @return mixed
 */
function wclsi_get_ls_sku( $ls_prod ) {
	if( isset( $ls_prod->custom_sku ) && !empty( $ls_prod->custom_sku ) ) {
		return $ls_prod->custom_sku;
	}

	if( isset( $ls_prod->manufacturer_sku ) && !empty( $ls_prod->manufacturer_sku ) ) {
		return $ls_prod->manufacturer_sku;
	}

	if( isset( $ls_prod->system_sku ) && !empty( $ls_prod->system_sku ) ) {
		return $ls_prod->system_sku;
	}

	return '';
}

function wclsi_span_tooltip( $tooltip_msg = '' ) {
	$tooltip_src = esc_url(  WC()->plugin_url() . '/assets/images/help.png' );
	$tooltip_html =
		"<span class='tips' data-tip='$tooltip_msg' >" .
			"<img class='help_tip' src='$tooltip_src' height='16' width='16' >" .
		"</span>";

	return $tooltip_html;
}


function wclsi_verify_nonce() {
	$nonce = $_POST['wclsi_admin_nonce'];
	if ( ! wp_verify_nonce( $nonce, 'wclsi_nonce' ) ) {
		header( "HTTP/1.0 409 Security Check." );
		exit;
	}
}

function wclsi_get_wpdb_field_type( $value ) {
	if ( is_string( $value ) ) {
		if ( is_numeric( $value ) ) {
			if (strpos($value, '.') !== false) {
				return '%f';
			} else {
				return '%d';
			}
		} else {
			return '%s';
		}
	} elseif ( is_float($value) ) {
		return '%f';
	} elseif ( is_int( $value ) ) {
		return '%d';
	}

	return '%s';
}
