<?php
if ( !class_exists( 'WCLSI_Lightspeed_Item_Attribute_Set' ) ) :
	class WCLSI_Lightspeed_Item_Attribute_Set {

		public $id = 0;
		public $item_attribute_set_id;
		public $name;
		public $attribute_name_1;
		public $attribute_name_2;
		public $attribute_name_3;

		function __construct( $id = 0 ){
			if ( $id > 0 ) {
				$this->init_via_item_attribute_set_id( $id );
			}
		}

		private function init_via_item_attribute_set_id( $id ) {
			global $wpdb, $WCLSI_ITEM_ATTRIBUTE_SETS_TABLE;

			$table_name = $wpdb->prefix . $WCLSI_ITEM_ATTRIBUTE_SETS_TABLE;
			$result = $wpdb->get_row( "SELECT * FROM $table_name WHERE item_attribute_set_id = $id", ARRAY_A );

			if ( !empty( $result ) ) {
				foreach ( $result as $property => $value ) {
					$this->{$property} = maybe_unserialize( $value );
				}
			}
		}

		/******* Public Static Methods *******/

		public static function insert_item_attribute_set( $item_attribute_set ) {
			global $wpdb, $WCLSI_ITEM_ATTRIBUTE_SETS_TABLE;

			$args = array(
				'item_attribute_set_id' => isset( $item_attribute_set->itemAttributeSetID ) ? $item_attribute_set->itemAttributeSetID : null,
				'name' => isset( $item_attribute_set->name ) ? $item_attribute_set->name : null,
				'attribute_name_1' => isset( $item_attribute_set->attributeName1 ) ? $item_attribute_set->attributeName1 : null,
				'attribute_name_2' => isset( $item_attribute_set->attributeName2 ) ? $item_attribute_set->attributeName2 : null,
				'attribute_name_3' => isset( $item_attribute_set->attributeName3 ) ? $item_attribute_set->attributeName3 : null,
				'system' => isset( $item_attribute_set->system ) ? $item_attribute_set->system : null,
				'archived' => isset( $item_attribute_set->archived ) ? $item_attribute_set->archived : null,
				'created_at' => current_time('mysql')
			);

			self::format_empty_vals( $args );

			$table = $wpdb->prefix . $WCLSI_ITEM_ATTRIBUTE_SETS_TABLE;

			$formatting = array( '%d', '%s', '%s', '%s', '%s', '%d', '%d', '%s' );

			$wpdb->replace( $table, $args, $formatting );
		}

		public static function format_empty_vals( &$args ) {
			foreach( $args as $key => $val ) {
				if ( $val == '' ) {
					$args[ $key ] = null;
				}
			}
		}

		// @todo find somewhere to implement this!
		private static function check_for_new_attributes() {
			global $wpdb, $WCLSI_API, $WCLSI_ITEM_ATTRIBUTE_SETS_TABLE;

			$item_attr_sets = $WCLSI_API->make_api_call( 'Account/' . $WCLSI_API->ls_account_id . '/ItemAttributeSet', 'Read' );

			$api_item_attr_set_ids = array();

			if ( is_array( $item_attr_sets->ItemAttributeSet ) && !empty( $item_attr_sets->ItemAttributeSet ) ) {
				foreach( $item_attr_sets->ItemAttributeSet as $item_attr_set ) {
					$api_item_attr_set_ids[] = $item_attr_set->itemAttributeSetID;
				}
			}

			$table = $wpdb->prefix . $WCLSI_ITEM_ATTRIBUTE_SETS_TABLE;
			$local_item_attr_set_ids = $wpdb->get_col( "SELECT item_attribute_set_id FROM $table" );

			$diff = array_diff( $api_item_attr_set_ids, $local_item_attr_set_ids );

			if ( !empty( $diff ) ) {
				// @todo pull in new item_attr_set_ids
			}
		}
	}
endif;
