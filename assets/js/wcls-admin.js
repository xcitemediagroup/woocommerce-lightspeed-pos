/**
 * Created by ryagudin on 12/31/14.
 */
(function($) {
	var wclsAdmin = {

		/**
		 * Handles displaying errors
		 * @param notice
		 * @param type updated or error
		 * @param fadeOut
		 */
		displayNotice: function( notice, type, fadeOut ) {
			type = type || 'error'; // make it error by default
			if ( fadeOut === '' || fadeOut === undefined ) {
				fadeOut = false;
			}

			var noticeDiv = '';
			if ( type == 'error' ) {
				noticeDiv = $( '.error:first:visible' ); // WP 4.4.2 has a hidden error divs
			} else if ( type == 'udpated' ) {
				noticeDiv = $( '.updated:first:visible' );
			}

			if ( noticeDiv.length > 0 ) {
				noticeDiv.append('<p>' + notice + '</p>');
			} else {

				// Try to insert after h1 if we're in a post page (for meta box actions)
				var wp_header_end = $('.wp-header-end');

				// Insert the message into the DOM
				var msgDiv = $('<div class="' + type + '" data-wclsi="wclsi-error"><p>' + notice + '</p></div>');

				if( wp_header_end.length == 0 ) {
					// Prepend notice to wrap elem
					var wrap = $('.wrap') || $('.wpwrap');
					msgDiv.prependTo(wrap);
				} else {
					msgDiv.insertAfter(wp_header_end)
				}

				// Scroll to the error
				var target = $('.' + type + ':first:visible[data-wclsi="wclsi-error"]');

				$('html, body').animate({
					scrollTop: 0
				}, 100);

				// Fade it out if we need to
				if ( fadeOut ) {
					msgDiv.delay(5000).fadeOut( function() { $(this).remove() } );
				}
			}
		},

		/**
		 * Binds click even to the Test API button element and triggers the AJAX call
		 * @param initAPIButtonElem
		 */
		bindClicktoInitAPIButton: function( initAPIButtonElem ) {
			var self = this;
			initAPIButtonElem.click(function() {
				self.initAPIHandler( self.APIKeyField.val(), self.accountIDField.val() );
			} );
		},
		/**
		 * Initializes click listener for the 'Load LightSpeed Products' button.
		 * @param loadProdButton
		 */
		initLSLoadProds: function( loadProdButton ) {
			var self = this;
			loadProdButton.click(function() {

				// Ask the user if they're sure they want to re-load
				if ( $(this).data('reload') ) {
					if ( confirm( objectL10n.reload_confirm ) ) {
						$('#wclsi-load-progress').show();
						self.loadProds(0, 1, true);
					}
				} else {
					$('#wclsi-load-progress').show();
					self.loadProds(0, 1, true);
				}
			} );
		},
		/**
		 * Makes an AJAX request to get the count of total products,
		 * after which makes a request every second as to not exceed API throttling limits
		 */
		loadProds: function( offset, limit, getCount, getMatrix ) {
			var self = this;
			var pct_complete = 0;

			if( getMatrix == undefined || getMatrix == '' ) {
				getMatrix = false;
			}

			$.ajax({
				url	 : ajaxurl,
				type : 'POST',
				data : {
					wclsi_admin_nonce: self.nonce,
					action   : 'wclsi_load_prods_ajax',
					offset   : offset,
					limit    : limit,
					getCount : getCount,
					getMatrix: getMatrix
				},
				dataType : 'json',
				success  : function( data ) {

					if ( wclsi_admin.SCRIPT_DEBUG ) {
						console.log(data);
					}

					self.checkForErrors( data );

					if ( getCount ) {
						self.prodCount = data.count;
						self.loadProds( offset, 50, false, getMatrix );
					} else {
						offset = offset + limit;

						pct_complete = ( offset / self.prodCount ) * 100;
						$('#wclsi-progress-count').html( Math.floor( pct_complete )+ '%' ); // shows the progress

						if ( wclsi_admin.SCRIPT_DEBUG ) {
							console.log(offset + '/' + self.prodCount);
						}

						if ( offset < self.prodCount ) {
							self.loadProds( offset, limit, false, getMatrix );
						} else {
							if ( self.errors ) {
								$('#wclsi-load-progress').html('<div class="error">' + objectL10n.incomplete_load + '</div>');
							}

							if( !getMatrix ) {
								$('#wclsi-progress-count').html( '0%' ); // shows the progress
								$('#wclsi-progress-msg').html(objectL10n.loading_matrix_products);
								self.loadProds(0, 1, true, true);
							} else {
								location.replace(data.redirect); // We're done! Reload to view the results ...
							}
						}
					}
				},
				error    : function(jqXHR, statusText, errorThrown) {
					self.handleErrors(jqXHR, statusText, errorThrown);
					$('#wclsi-load-progress').hide();
				}
			} );
		},
		/**
		 * Initializes click listener for the 'Import LightSpeed Categories' button.
		 * @param importCatsButton
		 */
		initImportCats: function( importCatsButton ) {
			var self = this;
			importCatsButton.click(function() {
				$('#wclsi-import-cats-progress').html( '<p>Importing Lightspeed categories - 0% completed ... </p>' );
				self.getCatCount();
			} );
		},
		getCatCount: function() {
			var self = this;

			$.ajax({
				url: ajaxurl,
				type: 'POST',
				data: {
					wclsi_admin_nonce: self.nonce,
					action: 'get_category_count'
				},
				dataType: 'json',
				success: function (data) {
					if ( wclsi_admin.SCRIPT_DEBUG ) {
						console.log(data);
					}

					self.checkForErrors( data );

					self.catCount = data.ciel_count;

					self.loadCats(0, 100);
				},
				error: function (jqXHR, statusText, errorThrown) {
					if (statusText == 'parsererror') {
						self.displayNotice(jqXHR.responseText, 'error');
					}
					self.displayNotice(errorThrown, 'error');
				}
			});
		},
		/**
		 * @param offset
		 * @param limit
		 */
		loadCats: function( offset, limit ) {
			var self = this;
			var pct_complete = 0;

			$.ajax({
				url: ajaxurl,
				type: 'POST',
				data: {
					wclsi_admin_nonce: self.nonce,
					action: 'import_ls_categories',
					offset: offset,
					limit: limit
				},
				dataType: 'json',
				success: function( data ) {
					if ( wclsi_admin.SCRIPT_DEBUG ) {
						console.log(data);
					}

					self.checkForErrors( data );

					offset = offset + limit;

					if ( wclsi_admin.SCRIPT_DEBUG ) {
						console.log(offset + '/' + self.catCount);
					}

					if ( offset < self.catCount ) {
						pct_complete = ( (offset + 100) / self.catCount ) * 100;
						$('#wclsi-import-cats-progress').html( '<p>Importing Lightspeed categories - ' + Math.floor( pct_complete ) + '% completed ... </p>' );
						self.loadCats( offset, limit );
					} else {
						if ( data.load_complete && data.errors.length === 0 ) {
							$('#wclsi-import-cats-progress').html(
								'<p>Category import complete! Click <a href="' + data.prod_cat_link + '">here</a> to view the imported categories.</p>'
							);
						} else if ( data.load_complete && data.errors.length > 0 ) {
							$('#wclsi-import-cats-progress').html(
								'<div class="error">' +
									'Category import completed but not all categories were succesfully imported. ' +
									'See errors above for more details. ' +
									'Click <a href="' + data.prod_cat_link + '">here</a> to view imported categories.' +
								'</div>'
							);
						} else if ( data.errors.length > 0 ) {
							$('#wclsi-import-cats-progress').html('<div class="error">' + objectL10n.incomplete_load + '</div>');
						}
					}
				},
				error: function(jqXHR, statusText, errorThrown) {
					self.handleErrors(jqXHR, statusText, errorThrown);
				}
			} );
		},
		/**
		 * Handles click operation for the import all products button
		 * @param importAllButton
		 */
		importAllProdsButton: function( importAllButton ) {
			var self = this;
			importAllButton.click(function() {
				var result = confirm( 'Are you sure you want to import all ' + $('.displaying-num').first().text() + '?' );
				if ( result ) {
					self.importAllButton.prop('disabled', true);
					self.importAllProds( true ); // pass true for initial call
				}
			} );
		},
		initProgressBar: function() {
			var self = this;
			self.progressDivID       = 'wclsi_sync_progress';
			self.progressCountSpanID = 'wclsi_progress_count';

			$('.wrap').children('h1:first').after(
				'<div id="' + self.progressDivID + '" class="updated">' +
				'<p><span class="spinner wclsi-spinner"></span>' + objectL10n.importing_prods +
					'<strong>' +
						'<span id="' + self.progressCountSpanID + '">' +
						'0/' + self.totalProds +
						'</span>' +
				'</strong></p>' +
				'<p><b>' + objectL10n.dont_close + '</b></p>' +
				'</div>'
			).hide().fadeIn().focus();
		},
		/**
		 * AJAX function that calls methods to import all the loaded products
		 * from LightSpeed into WooCommerce.
		 */
		importAllProds: function( initImport ) {
			var self = this;

			// Default is false
			if (initImport == undefined || initImport == '') {
				initImport = false;
			}

			$.ajax({
				url	 : ajaxurl,
				type : 'POST',
				data : {
					wclsi_admin_nonce: self.nonce,
					action: 'get_prod_ids_ajax',
					init_import: initImport
				},
				dataType : 'json',
				success  : function( data ) {

					self.checkForErrors( data );

					if ( data.prod_ids.length > 0 ) {
						self.totalProds       = data.prod_ids.length + data.matrix_ids.length;
						self.totalSingleProds = data.prod_ids.length;
						self.totalMatrixProds = data.matrix_ids.length;
						self.prodIds          = data.prod_ids;
						self.matrixIds        = data.matrix_ids;
						self.importProgress   = 0;

						self.initProgressBar();

						self.runSingleProdAction( 'import_all_lightspeed_products_ajax', 'false', self.totalSingleProds, 0, function(){ location.reload( true ) } );
					} else {
						self.displayNotice( objectL10n.no_prods_error, 'error' );
					}
				},
				error    : function(jqXHR, statusText, errorThrown) {
					self.handleErrors(jqXHR, statusText, errorThrown);
				}
			} );
		},
		/**
		 * Used to import a single product
		 * @param action          - the action to perform on the product
		 * @param isMatrix        - whether this is a matrix product
		 * @param totalLocalProds - optional, if given, will make a recursive call "totalProds" number of times
		 * @param prodCount       - the current prod count so we don't exceed totalProds
		 * @param callback        - callback when action is complete
		 */
		runSingleProdAction: function(action, isMatrix, totalLocalProds, prodCount, callback) {
			var self = this;

			// Stop condition for recursive calls
			if ( totalLocalProds === undefined || totalLocalProds === '') { return; }

			if ( self.sync_prods === undefined ) { self.sync_prods = true; }

			var prodID;
			if ( isMatrix == 'true' ) {
				prodID = self.matrixIds[ prodCount ];
			} else {
				prodID = self.prodIds[ prodCount ];
			}

			$.ajax({
				url	 : ajaxurl,
				type : 'POST',
				data : {
					wclsi_admin_nonce: self.nonce,
					action: action,
					prod_id: prodID,
					import_progress: self.importProgress, //How many products have been called
					is_matrix: isMatrix
				},
				dataType : 'json',
				success  : function( data ) {
					if ( wclsi_admin.SCRIPT_DEBUG ) {
						console.log(data);
					}

					self.checkForErrors( data );

					if ( wclsi_admin.SCRIPT_DEBUG ) {
						console.log('progress:' + self.importProgress);
						console.log('totalProds:' + self.totalProds);
					}

					if ( self.importProgress == self.totalProds-1 ) {
						self.importAllButton.prop('disabled', false);
						$('#' +  self.progressDivID).html( '<p>'+ objectL10n.done_importing + '</p>' );
						if( typeof callback == 'function' ){
							callback();
						}
					} else {
						$('#' + self.progressCountSpanID).html( self.importProgress + '/' + self.totalProds );
					}

					// Make recursive calls until we're done
					prodCount++;

					if ( (prodCount < totalLocalProds) && ( isMatrix == 'false' ) ) {
						self.runSingleProdAction( action, isMatrix, totalLocalProds, prodCount, callback ); // Recursive call for single prods
					} else if ( (prodCount <  totalLocalProds) && ( isMatrix == 'true' ) ) {
						self.runSingleProdAction( action, isMatrix, totalLocalProds, prodCount, callback ); // Recursive call for matrix prods
					} else if (  (prodCount >= totalLocalProds) && (isMatrix == 'false') ) {
						if( self.totalMatrixProds > 0 ) {
							self.runSingleProdAction(action, 'true', self.totalMatrixProds, 0, callback); // Initial recursive call for matrix prods after single prods are done
						}
					}

					self.importProgress++;
				},
				error: function(jqXHR, statusText, errorThrown) {
					self.importProgress++; // increment progress even if there's an error...
					self.handleErrors(jqXHR, statusText, errorThrown);
				}
			} );
		},
		/**
		 * Handler for checking/unchecking sync checkbox(es)
		 * @param checkBoxElem
		 */
		syncCheckBoxHandler: function( checkBoxElem ) {
			var self = this;
			checkBoxElem.click( function(e) {
				var prodID = $(this).data( 'prodid' );
				if ( prodID == "" || prodID == undefined ) {
					alert( objectL10n.sync_error, 'error', true );
					e.preventDefault();
				} else {
					if ($(this).is(':checked')) {
						// Enable sync flag
						self.setProdSync(prodID, true);
					} else {
						// Disable sync flag
						self.setProdSync(prodID, false);
					}
				}
			} )
		},
		/**
		 * Sets a product's sync flag to true or false
		 * @param prodID
		 * @param syncFlag Boolean
		 */
		setProdSync: function( prodID, syncFlag ) {
			var self = this;
			$.ajax({
				url	 : ajaxurl,
				type : 'POST',
				data : {
					wclsi_admin_nonce: self.nonce,
					action   : 'set_prod_sync_ajax',
					prod_id: prodID,
					sync_flag: syncFlag
				},
				dataType : 'json',
				success  : function( data ) {
					self.checkForErrors( data );

					if ( syncFlag ) {
						alert(objectL10n.sync_success);
					} else {
						alert(objectL10n.sync_remove);
					}
				},
				error    : function(jqXHR, statusText, errorThrown) {
					self.handleErrors(jqXHR, statusText, errorThrown);
				}
			} );
		},
		/**
		 * Handler for updated a product via the "Manual Update" button in the product page via the metabox
		 * @param buttonElem
		 */
		manualProdUpdateHander: function( buttonElem ) {
			var self = this;
			buttonElem.click( function() {
				var prodID = $(this).data( 'prodid' );
				var noticeID = 'wclsi-sync-notice';
				$(this).attr( 'disabled', 'disabled' );
				$('<p id="' + noticeID + '"><span class="spinner wclsi-spinner" id="wclsi-sync-spinner" style="visibility: visible; top: 0px;"></span>' + objectL10n.syncing + '</p>').insertAfter( $(this) );

				$.ajax({
					url	 : ajaxurl,
					type : 'POST',
					data : {
						wclsi_admin_nonce: self.nonce,
						action   : 'manual_prod_update_ajax',
						prod_id: prodID
					},
					dataType : 'json',
					success  : function( data ) {
						self.checkForErrors( data );

						if ( data.success ) {
							if( data.errors.length === 0 ) {
								$('#' + noticeID).html( objectL10n.man_sync_success );
								location.reload( true );
							} else {
								$('#' + noticeID).html( objectL10n.generic_error );
							}
						} else {
							self.displayNotice( objectL10n.generic_error, 'error' );
						}
					},
					error: function(jqXHR, statusText, errorThrown) {
						self.handleErrors(jqXHR, statusText, errorThrown);
					}
				} );
			} );
		},
		/**
		 * Checks errors returns for AJAX results
		 * @param data
		 */
		checkForErrors: function( data ) {
			var self = this;

			if ( data.errors ) {
				if ( $.type( data.errors ) === "array" ) {
					data.errors.forEach( function( e ) {
						self.displayNotice( e.message, 'error' );
					} );
				} else if ( $.type( data.errors ) === "string" ) {
					self.displayNotice( data.errors, 'error' );
				} else {
					self.displayNotice( objectL10n.generic_error, 'error' );
				}
			}
		},
		/**
		 * Binds click event to sync WC prod to LightSpeed.
		 * @param button
		 */
		bindClickToSyncToLSButton: function( button ) {
			var self = this;
			button.click(function() {
				prodID = $( this ).data( 'prodid' );
				 self.syncProdToLS( prodID );
			} );
		},
		/**
		 * AJAX call to sync product with LightSpeed.
		 * @param prodID
		 */
		syncProdToLS: function( prodID ) {
			var self = this;

			// Display status
			var statusDivID   = 'wclsi-sync-status';
			var noticeID = 'wclsi-sync-status-msg';
			var statusDiv     = $( '#' + statusDivID );

			var statusHTML = '<div id="' + statusDivID + '" style="display: inline-block; padding: 5px;">' +
							   '<span class="spinner wclsi-spinner" style="vertical-align: bottom;"></span> ' +
							   '<span id="' + noticeID + '">' + objectL10n.syncing  + '</span>' +
							 '</div>';

			if ( statusDiv.length > 0 ) {
				statusDiv.replaceWith( statusHTML );
			} else {
				$( statusHTML ).insertAfter( self.syncProdToLSButton );
			}

			$.ajax({
				url	 : ajaxurl,
				type : 'POST',
				data : {
					wclsi_admin_nonce: self.nonce,
					action: 'sync_prod_to_ls',
					wc_prod_id: prodID
				},
				dataType : 'json',
				success  : function( data ) {
					self.checkForErrors( data );

					if ( data.success ) {
						$('#' + noticeID).html( objectL10n.man_sync_success );

						if( data.errors == undefined || data.errors.length == 0 ) {
							location.reload(true);
						}
					} else {
						self.displayNotice( objectL10n.generic_error, 'error' );
						$('#' + noticeID).html( objectL10n.generic_error );
					}
				},
				error    : function(jqXHR, statusText, errorThrown) {
					self.handleErrors(jqXHR, statusText, errorThrown);
				}
			} );
		},
		/**
		 * Right now will only work with import_and_sync and import
		 */
		overrideBulkSubmit: function( form ) {
			var self = this;

			form.submit( function(e) {
				var action = self.topBulkActionSelector.val();

				if( action == 'import_and_sync' || action == 'import'){
					e.preventDefault();
				} else {
					return true;
				}

				var inputs = form.find('input:checked[name^=wc_ls_imported_prod]');

				var prod_data = {};
				prod_data.prod_ids = [];
				prod_data.matrix_ids = [];

				for (var i = 0, len = inputs.length; i < len; i++) {
					var prod = $(inputs[i]);
					if( prod.data('prod-type') == 'matrix' ){
						formatted_id = prod.val().split('-')[0];
						prod_data.matrix_ids.push( formatted_id );
					}
					if( prod.data('prod-type') == 'single' ){
						prod_data.prod_ids.push( prod.val() )
					}
				}
				self.executeBulkAction( action, prod_data );
				return false;
			})
		},
		executeBulkAction: function( action, prod_data ) {
			var self = this;
			if( wclsi_admin.SCRIPT_DEBUG ) {
				console.log(action);
				console.log(prod_data);
			}

			self.totalProds       = prod_data.prod_ids.length + prod_data.matrix_ids.length;
			self.totalSingleProds = prod_data.prod_ids.length;
			self.totalMatrixProds = prod_data.matrix_ids.length;
			self.prodIds          = prod_data.prod_ids;
			self.matrixIds        = prod_data.matrix_ids;
			self.importProgress   = 0;

			self.initProgressBar();

			var reload = function(){ location.reload( true ) };

			// Actions: import_and_sync, import, sync, update, delete (append "_ajax" to refer to back-end wp functions)
			if(self.totalSingleProds > 0) {
				self.runSingleProdAction(action + '_product_ajax', 'false', self.totalSingleProds, 0, reload);
			} else {
				self.runSingleProdAction(action + '_product_ajax', 'true', self.totalMatrixProds, 0, reload);
			}
		},
		bindRelinkElem: function( relinkElem ) {
			var self = this;

			relinkElem.click(function(){
				var prod_id = $(this).data('prod-id');
				var spinner = $('<p>Relinking ...<span class="spinner wclsi-spinner" id="wclsi-sync-spinner" style="float: none; vertical-align: initial;"></span></p>');

				if ( $('#wclsi-sync-spinner').length === 0 ) {
					spinner.insertAfter( $('#wclsi-relink-wrapper') );
				}

				self.relinkProd( prod_id, function(){ spinner.remove() } );

				return false;
			});
		},
		relinkProd: function( prod_id, callback ) {
			var self = this;

			$.ajax({
				url	 : ajaxurl,
				type : 'POST',
				data : {
					wclsi_admin_nonce: self.nonce,
					action: 'relink_wc_prod_ajax',
					wc_prod_id: prod_id
				},
				dataType : 'json',
				success  : function( data ) {
					self.checkForErrors( data );

					if ( data.success ) {
						self.displayNotice( objectL10n.relink_success, 'updated' );
					} else if ( !data.success && ( undefined === data.errors || 0 === data.errors.length ) ) {
						self.displayNotice( objectL10n.generic_error, 'error');
					}

					callback();
				},
				error    : function(jqXHR, statusText, errorThrown) {
					self.handleErrors(jqXHR, statusText, errorThrown);
					callback();
				}
			} );
		},
		handleErrors: function(jqXHR, statusText, errorThrown) {
			var self = this;

			if ( statusText == 'parsererror' ) {
				self.displayNotice( jqXHR.responseText, 'error' );
			}
			self.displayNotice( errorThrown, 'error' );
		},
		selectAllCheckboxesButton: function(selectAllElem, checkboxes) {
			var self = this;
			selectAllElem.click(function() {
				checkboxes.prop("checked", !checkboxes.prop("checked"));
			});
		},
		/**
		 * Initializes JS for the admin page
		 */
		init: function() {
			var self = this;

			// Create handles to important elements
			self.nonce = $( '#wclsi_admin_nonce' ).val();
			self.accountIDField = $( '#' + wclsi_admin.PLUGIN_PREFIX_ID + 'ls_account_id' );
			self.initAPIButton  = $( '#' + wclsi_admin.PLUGIN_PREFIX_ID + 'init_api_settings_button' );
			self.importAllButton  = $( '#wc-import-all-prods' );
			self.loadLSProdButton = $( '#wc-ls-load-prods' );
			self.submitAPIButton = $( '.submit input:submit' );
			self.syncCheckBoxes  = $( '.wclsi-sync-cb' );
			self.prodManualSyncButton = $( '#wclsi-manual-sync' );
			self.syncProdToLSButton = $( '#wclsi-sync-to-ls' );
			self.importAndSyncLinks = $('.wc_ls_imported_prods .import_and_sync a');
			self.importLinks = $('.wc_ls_imported_prods .import a');
			self.prodTableForm = $('#wc-imported-prods-filter');
			self.topBulkActionSelector = $('#bulk-action-selector-top');
			self.relinkElem = $('#wclsi-relink');
			self.importLSCats = $('#wclsi-import-cats');

			// Initialize Methods
			self.bindClicktoInitAPIButton( self.initAPIButton );
			self.initLSLoadProds( self.loadLSProdButton );
			self.importAllProdsButton( self.importAllButton );
			self.syncCheckBoxHandler( self.syncCheckBoxes );
			self.manualProdUpdateHander( self.prodManualSyncButton );
			self.bindClickToSyncToLSButton( self.syncProdToLSButton );
			self.overrideBulkSubmit( self.prodTableForm );
			self.bindRelinkElem( self.relinkElem );
			self.initImportCats( self.importLSCats );
			self.selectAllCheckboxesButton(
				$( '#wclsi_select_all_prod_sync_properties' ),
				$( '#wc_prod_selective_sync_properties input[type=checkbox]' )
			);

			self.selectAllCheckboxesButton(
				$( '#wclsi_select_all_ls_prod_sync_properties' ),
				$( '#ls_prod_selective_sync_properties input[type=checkbox]' )
			);

			// Tooltips!
			$( '#tiptip_holder' ).removeAttr( 'style' );
			$( '#tiptip_arrow' ).removeAttr( 'style' );
			$( '.tips' ).tipTip({
				'attribute': 'data-tip',
				'fadeIn': 50,
				'fadeOut': 50,
				'delay': 200
			} );

			wclsi_admin.displayNotice = self.displayNotice;
			wclsi_admin.checkForErrors = self.checkForErrors;
		}
	};

	$( document ).ready( function() {
		wclsAdmin.init();
	} );

} )( jQuery );
