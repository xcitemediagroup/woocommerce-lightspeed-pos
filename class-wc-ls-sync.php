<?php
/**
 * @class LSI_Import_page
 * wclsi_cache_sync_expiration filter - can be used to adjust the expiration of the cache
 * wclsi_inventory_sync_error filter - can be used to modify the error message that shows up if
 *                            we cannot update the inventory via LightSpeed.
 */

if ( ! class_exists( 'LSI_Synchronizer' ) ) :
	class LSI_Synchronizer {

		const DEFAULT_LS_ATTR_SET_ID = 4;

		function __construct() {
			add_filter( 'cron_schedules', array( $this, 'wclsi_add_every_five_minutes' ) );
			add_action( 'wclsi_daily_sync', array( $this, 'wclsi_daily_sync' ) );

			// To-do: remove dependency on transients
			add_action( 'woocommerce_update_product', array( $this, 'update_ls_prod' ), 1 );
			add_action( 'woocommerce_update_product_variation', array( $this, 'update_ls_variation_prod' ), 1 );
			add_action( 'woocommerce_product_set_stock', array( $this, 'sync_prod_inventory' ), 10 );
			add_action( 'woocommerce_variation_set_stock', array( $this, 'sync_prod_inventory' ), 10 );
			add_filter( 'woocommerce_add_to_cart_product_id', array( $this, 'update_inventory_on_add_to_cart' ) );
			add_action( 'woocommerce_before_single_product', array( $this, 'update_inventory_on_page_view' ) );
			add_action( 'woocommerce_before_checkout_process', array( $this, 'update_inventory_before_checkout' ) );
			add_action( 'http_api_curl', array( $this, 'fix_curl_opts_for_img_upload' ), 10, 3 );
			add_action( 'wp_ajax_sync_prod_to_ls', array( $this, 'sync_prod_to_ls' ) );
		}

		/**
		 * Unschedules the sync schedule on deactivation
		 */
		public static function wclsi_unschedule_sync() {
			$timestamp = wp_next_scheduled( 'wclsi_daily_sync' );
			wp_unschedule_event( $timestamp, 'wclsi_daily_sync' );
		}

		/**
		 * On an early action hook, check if the hook is scheduled - if not, schedule it.
		 */
		function wclsi_sync_setup_schedule() {
			$run_job = apply_filters( 'wlcsi_run_daily_sync', true );
			if ( $run_job && ! wp_next_scheduled( 'wclsi_daily_sync' ) ) {
				$interval   = apply_filters( 'wclsi_daily_sync_interval', 'five_minutes' );
				$start_time = apply_filters( 'wlcsi_daily_sync_start_time', strtotime( 'now' ) );
				wp_schedule_event( $start_time, $interval, 'wclsi_daily_sync' );
			}
		}

		function wclsi_add_every_five_minutes( $schedules ) {
			$schedules['five_minutes'] = array(
				'interval' => 60 * 5,
				'display'  => __( 'Every five minutes' )
			);

			return $schedules;
		}

		/**
		 * The daily "sync", this is more of a one-directional sync from LightSpeed.
		 * LightSpeed will always win and overwrite the WooCommerce products.
		 */
		function wclsi_daily_sync() {
			$last_check = get_option( WCLSI_LAST_SYNC_TIMESTAMP );
			if ( false !== $last_check ) {
				$check_for_updates_since = $last_check;
			} else {
				$check_for_updates_since = date( DATE_ATOM, strtotime( '-5 minutes' ) );
			}

			$this->update_single_items_via_autosync( $check_for_updates_since );
			$this->update_matrix_items_via_autosync( $check_for_updates_since );

			update_option( WCLSI_LAST_SYNC_TIMESTAMP, date( DATE_ATOM, strtotime( 'now' ) ) );
		}

		/**
		 * @param $since
		 */
		function update_single_items_via_autosync( $since ) {
			global $WCLSI_API, $WCLSI_SINGLE_PROD_SEARCH_PARAMS, $WCLSI_WC_Logger;

			$search_params = array(
				'load_relations' => json_encode( $WCLSI_SINGLE_PROD_SEARCH_PARAMS ),
				'timeStamp'      => '>,' . $since
			);

			$result = $WCLSI_API->make_api_call( 'Account/' . $WCLSI_API->ls_account_id . '/Item', 'Read', $search_params );

			if ( is_wp_error( $result ) ) {
				$WCLSI_WC_Logger->add(
					WCLSI_WC_LOGGER_HANDLE,
					'Automated sync failed for simple items:' . PHP_EOL . $result->get_error_message()
				);
			} else {
				$WCLSI_WC_Logger->add(
					WCLSI_WC_LOGGER_HANDLE,
					'Running automated sync for simple/single items:' . PHP_EOL . print_r( $result, true )
				);

				if ( isset( $result->Item ) ) {
					if ( is_array( $result->Item ) ) {
						foreach ( $result->Item as $item ) {
							$this->handle_item_update_via_autosync( $item );
						}
					} elseif ( is_object( $result->Item ) ) {
						$this->handle_item_update_via_autosync( $result->Item );
					}
				}
			}
		}

		/**
		 * Helper method for updating single items via autosync
		 *
		 * @param $new_ls_item
		 */
		private function handle_item_update_via_autosync( $new_ls_item ) {

			/**
			 * No need to update matrix variations as it should be covered
			 * by the update_single_items_via_autosync function
			 */
			$update_variations = null;

			if ( property_exists( $new_ls_item, 'itemID' ) ) {
				$item_id = $new_ls_item->itemID;
			} else {
				$item_id = null;
			}

			$item_mysql_id = WCLSI_Lightspeed_Prod::get_mysql_id( $item_id, $new_ls_item->itemMatrixID );

			if ( ! empty( $item_mysql_id ) ) {
				$ls_item = new WCLSI_Lightspeed_Prod( $item_mysql_id );

				if ( $ls_item->wc_prod_id > 0 ) {
					$wclsi_sync = (bool) get_post_meta( $ls_item->wc_prod_id, WCLSI_SYNC_POST_META, true );

					if ( $wclsi_sync ) {
						global $WCLSI_PRODS;
						WCLSI_Lightspeed_Prod::update_via_api_item( $new_ls_item, $ls_item );

						$ls_item->reload();

						if ( $ls_item->is_matrix_product() ) {
							$update_variations = false;
						}

						$WCLSI_PRODS->update_wc_prod( $ls_item, $update_variations );
					}
				}
			}
		}

		/**
		 * @param $since
		 */
		function update_matrix_items_via_autosync( $since ) {
			global $WCLSI_API, $WCLSI_MATRIX_PROD_SEARCH_PARAMS, $WCLSI_WC_Logger;

			$search_params = array(
				'load_relations' => json_encode( $WCLSI_MATRIX_PROD_SEARCH_PARAMS ),
				'timeStamp'      => '>,' . $since
			);

			$result = $WCLSI_API->make_api_call( 'Account/' . $WCLSI_API->ls_account_id . '/ItemMatrix', 'Read', $search_params );

			if ( is_wp_error( $result ) ) {
				$WCLSI_WC_Logger->add(
					WCLSI_WC_LOGGER_HANDLE,
					'Automated sync failed for matrix items:' . PHP_EOL . $result->get_error_message()
				);
			} else {
				$WCLSI_WC_Logger->add(
					WCLSI_WC_LOGGER_HANDLE,
					'Running automated sync for matrix items:' . PHP_EOL . print_r( $result, true )
				);

				if ( isset( $result->ItemMatrix ) ) {
					if ( is_array( $result->ItemMatrix ) ) {
						foreach ( $result->ItemMatrix as $item ) {
							$this->handle_item_update_via_autosync( $item );
						}
					} elseif ( is_object( $result->ItemMatrix ) ) {
						$this->handle_item_update_via_autosync( $result->ItemMatrix );
					}
				}
			}
		}

		/**
		 * Updates inventory before checkout.
		 */
		function update_inventory_before_checkout() {
			$cart_items = WC()->cart->cart_contents;

			if ( empty( $cart_items ) ) {
				return;
			}

			foreach ( $cart_items as $item_array ) {
				$lookup_id = $item_array['variation_id'] > 0 ? $item_array['variation_id'] : $item_array['product_id'];
				$this->pull_item_inventory( $lookup_id, false );

				$product = wc_get_product( $lookup_id );

				// Set error messages to stop order creation process
				if ( false === $product->is_in_stock() ) {
					$msg = sprintf(
						__( 'Sorry, "%s" is not in stock. Please edit your cart and try again. We apologise for any inconvenience caused.', 'woocommerce-lightspeed-pos' ),
						get_the_title( $lookup_id )
					);

					wc_add_notice( $msg, 'error' );
				}
			}
		}

		/**
		 * Pulls in inventory data (from LightSpeed) if a product page is being viewed
		 */
		function update_inventory_on_page_view() {
			global $product;
			if ( is_product() && !is_null( $product) ) {
				$prod_id = $product->get_id();
				if ( $product->is_type( 'variable' ) ) {
					$this->pull_matrix_inventory( $prod_id );
				} else if ( $product->is_type( 'simple' ) ) {
					$this->pull_item_inventory( $prod_id);
				}
			}
		}

		/**
		 * Pulls in lightspeed inventory on adding to cart action.
		 */
		function update_inventory_on_add_to_cart( $prod_id ) {
			/**
			 * Check the matrix cache here due to the way we cache a
			 * matrix prod's sync (we only cache the parent_id, not the variation id)
			 */
			if ( isset( $_POST['variation_id'] ) && ( false !== get_transient( 'wclsi-sync-' . $prod_id ) ) ) {
				return $prod_id;
			}

			$lookup_prod_id = isset( $_POST['variation_id'] ) ? absint( $_POST['variation_id'] ) : $prod_id;

			$this->pull_item_inventory( $lookup_prod_id );

			return $prod_id;
		}

		/**
		 * Pulls a matrix product's Lightspeed inventory
		 *
		 * @param $wc_prod_id int
		 * @param $check_cache boolean
		 * @param $wclsi_cache_expiration int
		 */
		function pull_matrix_inventory( $wc_prod_id, $check_cache = true, $wclsi_cache_expiration = 30 ) {

			if ( ! get_post_meta( $wc_prod_id, WCLSI_SYNC_POST_META, true ) ) {
				return;
			}

			if ( $check_cache && ( false !== get_transient( 'wclsi-sync-' . $wc_prod_id ) ) ) {
				return;
			}

			$ls_item = new WCLSI_Lightspeed_Prod();
			$ls_item->init_via_wc_prod_id( $wc_prod_id );

			$result = $ls_item->update_via_api();

			if ( is_wp_error( $result ) ) {
				$error_msg =
					__(
						'Warning: there was an error with syncing the inventory of this product: ',
						'woocommerce-lightspeed-pos'
					);
				$error_msg .= $result->get_error_message();

				wc_add_notice(
					apply_filters( 'wclsi_inventory_sync_error', $error_msg ),
					'error'
				);
			} else {
				$ls_item->reload();
				$variations = $ls_item->variations;
				if ( ! empty( $variations ) ) {
					foreach ( $variations as $variation ) {
						if ( $variation->wc_prod_id > 0 ) {
							$this->set_wc_prod_stock( $variation );
						}
					}
				}
				set_transient(
					'wclsi-sync-' . $wc_prod_id,
					true,
					apply_filters( 'wclsi_cache_sync_expiration', $wclsi_cache_expiration )
				);
			}
		}

		/**
		 * Pulls a single product's Lightspeed inventory
		 *
		 * @param $wc_prod_id int
		 * @param $check_cache
		 * @param $wclsi_cache_expiration
		 *
		 * @return void
		 */
		function pull_item_inventory( $wc_prod_id, $check_cache = true, $wclsi_cache_expiration = 30 ) {

			if ( ! get_post_meta( $wc_prod_id, WCLSI_SYNC_POST_META, true ) ) {
				return;
			}

			if ( $check_cache && ( false !== get_transient( 'wclsi-sync-' . $wc_prod_id ) ) ) {
				return;
			}

			$ls_item = new WCLSI_Lightspeed_Prod();
			$ls_item->init_via_wc_prod_id( $wc_prod_id );

			if ( $ls_item->id > 0 ) {
				$result = $ls_item->update_via_api();

				if ( is_wp_error( $result ) ) {
					$error_msg = __(
						'Warning: there was an error with syncing the inventory of this product: ',
						'woocommerce-lightspeed-pos'
					);
					$error_msg .= $result->get_error_message();

					wc_add_notice(
						apply_filters( 'wclsi_inventory_sync_error', $error_msg ),
						'error'
					);
				} else {
					$ls_item->reload();
					$this->set_wc_prod_stock( $ls_item );
					set_transient(
						'wclsi-sync-' . $wc_prod_id,
						true,
						apply_filters( 'wclsi_cache_sync_expiration', $wclsi_cache_expiration )
					);
				}
			}
		}

		/**
		 * Sets a products status and updates the existing lightspeed objects in the post meta
		 * with the new inventory.
		 *
		 * @param $ls_item
		 */
		private function set_wc_prod_stock( $ls_item ) {
			$inventory    = wclsi_get_lightspeed_inventory( $ls_item, true );
			$wc_inventory = get_post_meta( $ls_item->wc_prod_id, '_stock', true );

			do_action( 'wclsi_update_wc_stock', $ls_item->wc_prod_id, $inventory );

			//if inventory has changed ... if not, don't do anything!
			if ( absint( $inventory ) === absint( $wc_inventory ) ) {
				return;
			}

			update_post_meta( $ls_item->wc_prod_id, '_stock', $inventory );

			if ( $inventory > 0 ) {
				update_post_meta( $ls_item->wc_prod_id, '_stock_status', 'instock' );
			} else if ( $inventory == 0 ) {
				update_post_meta( $ls_item->wc_prod_id, '_stock_status', 'outofstock' );
			}
		}

		function build_ls_update_payload( WC_Product $wc_prod, $ls_item_id ) {
			$wclsi_item = new WCLSI_Lightspeed_Prod;

			if ( false == $ls_item_id ) { return; }

			if ( $wc_prod->is_type( 'variable' ) ) {
				$wclsi_item->init_via_item_matrix_id( $ls_item_id );
			} else {
				$wclsi_item->init_via_item_id( $ls_item_id );
			}

			if ( $wclsi_item->id > 0 ) {
				$ls_payload              = new stdClass();
				$ls_payload->customSku   = $wc_prod->get_sku();
				$ls_payload->description = $wc_prod->get_title();

				if ( !$wclsi_item->is_matrix_product() ) {
					$ls_payload->ItemShops =
						$this->build_item_shops( $wclsi_item, $wc_prod->get_stock_quantity() );
				}

				$this->build_item_e_commerce( $ls_payload, $wc_prod );
				$this->build_pricing( $ls_payload, $wc_prod );
				$this->handle_ls_selective_sync( $ls_payload );
				return $ls_payload;
			} else {
				return new WP_Error(
					'no_ls_product',
					"Could not find Lightspeed Product with item id {$ls_item_id}",
					$ls_item_id
				);
			}
		}

		function update_ls_variation_prod( $wc_prod_id ) {
			$this->update_ls_prod( $wc_prod_id );
		}

		function handle_ls_selective_sync( &$ls_payload ) {
			global $WCLSI_API;
			$selective_sync = $WCLSI_API->settings[ WCLSI_LS_SELECTIVE_SYNC ];

			$unset_prop =
				function( $prop_name, &$prop_parent, $prop_ls_key = null ) use ( $selective_sync ) {
					$prop_ls_key = is_null( $prop_ls_key ) ? $prop_name : $prop_ls_key;

					if ( is_array( $prop_parent ) ) {
						if ( !is_null( $prop_parent[$prop_ls_key] ) && empty( $selective_sync[ $prop_name ] ) ) {
							unset( $prop_parent[$prop_ls_key] );
						}
					} elseif ( is_object( $prop_parent ) ) {
						if ( !isset( $prop_parent->{$prop_ls_key} ) ) { return; }
						if ( !is_null( $prop_parent->{$prop_ls_key} ) && empty( $selective_sync[ $prop_name ] ) ) {
							unset( $prop_parent->{$prop_ls_key} );
						}
					}
				};

			$unset_prop( 'description',  $ls_payload );
			$unset_prop( 'customSku',  $ls_payload );
			$unset_prop( 'longDescription',  $ls_payload->ItemECommerce );
			$unset_prop( 'shortDescription',  $ls_payload->ItemECommerce );
			$unset_prop( 'weight',  $ls_payload->ItemECommerce );
			$unset_prop( 'length',  $ls_payload->ItemECommerce );
			$unset_prop( 'width',  $ls_payload->ItemECommerce );
			$unset_prop( 'height',  $ls_payload->ItemECommerce );
			$unset_prop( 'stock_quantity',  $ls_payload, 'ItemShops' );

			// Price index mapping: 0: 'default', 1: 'MSRP', 2: 'Sale'
			$unset_prop( 'regular_price',  $ls_payload->Prices, 0 );
			$unset_prop( 'regular_price',  $ls_payload->Prices, 1 );
			$unset_prop( 'sale_price',  $ls_payload->Prices, 2 );

			foreach($ls_payload as $key => $value) {
				if ( empty( (array) $ls_payload->{$key} ) ) {
					unset( $ls_payload->{$key} );
				}
			}
		}

		function update_ls_prod( $wc_prod_id ) {
			$wc_prod = wc_get_product( $wc_prod_id );

			// Protects against multiple inventory syncs
			if ( false !== get_transient( "wclsi_ls_sync_triggered_via_{$wc_prod_id}" ) ) { return false; }

			if ( $wc_prod->is_type( 'variable' ) ) {
				$ls_item_id = get_post_meta( $wc_prod->get_id(), WCLSI_MATRIX_ID_POST_META, true );
			} else {
				$ls_item_id = get_post_meta( $wc_prod->get_id(), WCLSI_SINGLE_ITEM_ID_POST_META, true );
			}

			$ls_payload = $this->build_ls_update_payload( $wc_prod, $ls_item_id );

			// prepare payload
			global $WCLSI_API;
			$ls_payload = apply_filters( 'wclsi_update_ls_prod', $ls_payload );
			$resource = $wc_prod->is_type( 'variable' ) ? 'Account.ItemMatrix' : 'Account.Item';
			$relations = array( 'load_relations' => json_encode( 'all' ) );
			$result = $WCLSI_API->make_api_call(
				$resource, 'Update', $relations, json_encode( $ls_payload ), $ls_item_id
			);

			set_transient( "wclsi_ls_sync_triggered_via_{$wc_prod_id}", true, 5 );

			return $result;
		}

		/**
		 * Updates inventory in LightSpeed
		 */
		function sync_prod_inventory( WC_Product $wc_prod ) {
			// update_ls_prod() will now handle admin syncing
			if ( is_admin() ) { return; }

			global $WCLSI_API;

			$wc_prod_id = $wc_prod->get_id();

			if ( ! empty( $wc_prod_id ) ) {

				$wclsi_sync = (bool) get_post_meta( $wc_prod_id, WCLSI_SYNC_POST_META, true );

				if ( $wclsi_sync ) {
					$ls_prod = new WCLSI_Lightspeed_Prod();
					$ls_prod->init_via_wc_prod_id( $wc_prod_id );

					if ( $ls_prod->id > 0 ) {

						// This will be JSON encoded and used in our PUT request
						$payload           = array();
						$payload['itemID'] = $ls_prod->item_id;

						$inventory            = $wc_prod->get_stock_quantity();
						$payload['ItemShops'] = $this->build_item_shops( $ls_prod, $inventory );

						$payload = apply_filters( 'wclsi_push_wc_inventory_to_ls', $payload, $inventory, $wc_prod_id );

						// Sync with LightSpeed
						$WCLSI_API->make_api_call(
							'Account.Item',
							'Update',
							array( 'load_relations' => json_encode( 'all' ) ),
							$payload,
							$ls_prod->item_id
						);
					}
				}
			}
		}

		/**
		 * @param WCLSI_Lightspeed_Prod $ls_prod
		 * @param $inventory
		 *
		 * @return array
		 */
		function build_item_shops( WCLSI_Lightspeed_Prod $ls_prod, $inventory ) {
			$item_shops_payload = array();

			$item_shops = $ls_prod->item_shops;

			if ( ! empty( $item_shops ) ) {
				global $WCLSI_API;

				if ( isset( $WCLSI_API->settings[ WCLSI_INVENTORY_SHOP_ID ] ) ) {
					foreach ( $item_shops as $item_shop_key => $item_shop ) {
						if ( $WCLSI_API->settings[ WCLSI_INVENTORY_SHOP_ID ] == $item_shop->shop_id ) {

							$new_item_shop = $this->setup_new_item_shop(
								$item_shop->item_shop_id,
								$item_shop->shop_id,
								$inventory
							);

							$item_shops_payload['ItemShop'][] = $new_item_shop;

							break;
						}
					}
				} else {
					// The case where it's a single store, but for whatever reason it's still an array of shopIDs
					foreach ( $item_shops as $item_shop_key => $item_shop ) {
						if ( 0 != $item_shop->shop_id ) {
							$new_item_shop = $this->setup_new_item_shop(
								$item_shop->item_shop_id,
								$item_shop->shop_id,
								$inventory
							);

							$item_shops_payload['ItemShop'][] = $new_item_shop;
						}
					}
				}
			}

			// Place the item shops under the proper structure...
			return $item_shops_payload;
		}

		/**
		 * Creates a new item shop array given an id and stock
		 *
		 * @param $item_shop_id
		 * @param $stock
		 *
		 * @return array
		 */
		private function setup_new_item_shop( $item_shop_id, $shop_id, $stock ) {
			$new_item_shop               = array();
			$new_item_shop['itemShopID'] = $item_shop_id;
			$new_item_shop['qoh']        = $stock;
			$new_item_shop['shopID']     = $shop_id;

			return $new_item_shop;
		}

		/**
		 * AJAX function to push a WC prod to LS.
		 */
		function sync_prod_to_ls() {
			wclsi_verify_nonce();

			if ( isset( $_POST['wc_prod_id'] ) ) {
				$wc_prod_id = (int) $_POST['wc_prod_id'];
			} else {
				header( "HTTP/1.0 409 " . __( 'Could not find a product ID  to sync with.' ) );
				exit;
			}

			$this->push_prod_to_lightspeed( $wc_prod_id );

			$errors = get_settings_errors( 'wclsi_settings' );
			$success = empty( $errors ) ? true : false;

			echo wp_json_encode(
				array(
					'success' => $success,
					'errors'  => $errors
				)
			);

			exit;
		}

		/**
		 * @param $wc_prod_id
		 *
		 * @return false on success, false if something went wrong.
		 */
		function push_prod_to_lightspeed( $wc_prod_id ) {
			$wc_prod = wc_get_product( $wc_prod_id );
			if ( $wc_prod->is_type( 'variable' ) ) {
				return $this->push_matrix_prod( $wc_prod );
			} else if ( $wc_prod->is_type( 'simple' ) ) {
				return $this->push_simple_prod( $wc_prod );
			}

			return false;
		}

		/**
		 * Pushes a variable WC product to LightSpeed (and transforms it to a matrix product).
		 *
		 * @param WC_Product_Variable $wc_prod
		 *
		 * @return bool
		 */
		function push_matrix_prod( WC_Product_Variable $wc_prod ) {
			global $WCLSI_API;

			$wc_attributes = $wc_prod->get_attributes();
			if ( count( $wc_attributes ) > 3 ) {
				add_settings_error(
					'wclsi_settings',
					'wclsi_too_many_attributes',
					__( 'Lightspeed allows a maximum of 3 attributes for matrix products, your product has more than 3 attributes.',
						'woocommerce-lightspeed-pos' ),
					'error'
				);

				return false;
			}

			$matrix_prod_json                     = $this->ls_prod_json_builder( $wc_prod );
			$matrix_prod_json->itemAttributeSetID = self::DEFAULT_LS_ATTR_SET_ID;
			$result                               = $WCLSI_API->make_api_call( 'Account.ItemMatrix', 'Create', '', json_encode( $matrix_prod_json ) );

			if ( ! is_wp_error( $result ) && isset( $result->ItemMatrix->itemMatrixID ) ) {
				$this->handle_img_uploads( $result->ItemMatrix, $wc_prod );
				$this->persist_ls_data( $result->ItemMatrix, $wc_prod->get_id() );
			} else {
				return false;
			}

			add_filter( 'pre_option_woocommerce_hide_out_of_stock_items', '__return_empty_string' );
			$variations = $wc_prod->get_available_variations();

			if ( ! empty( $variations ) && isset( $result->ItemMatrix->itemMatrixID ) ) {
				foreach ( $variations as $variation ) {

					$variation_prod = wc_get_product( $variation['variation_id'] );
					$ls_prod_json   = $this->ls_prod_json_builder( $variation_prod, $result->ItemMatrix->itemMatrixID );

					$this->build_item_attrs( $ls_prod_json, $variation_prod->get_variation_attributes() );

					$variation_result = $WCLSI_API->make_api_call( 'Account.Item', 'Create', '', json_encode( $ls_prod_json ) );

					if ( ! is_wp_error( $variation_result ) && isset( $variation_result->Item ) ) {
						$this->handle_img_uploads( $variation_result->Item, $variation_prod );
						$this->persist_ls_data( $variation_result->Item, $variation['variation_id'] );
					}
				}
			}

			return true;
		}

		/**
		 * Build ItemAttributes object to a LS matrix product.
		 *
		 * @param &ls_prod
		 * @param $wc_prod_attrs
		 */
		private function build_item_attrs( &$ls_prod, $wc_prod_attrs ) {

			$ItemAttributes                     = new stdClass();
			$ItemAttributes->itemAttributeSetID = self::DEFAULT_LS_ATTR_SET_ID;

			// create an array with mappings - i.e. attribute1 => "color", attribute2 => "size"
			$id = 1;
			foreach ( $wc_prod_attrs as $key => $attr_val ) {
				$ItemAttributes->{'attribute' . $id ++} = $attr_val;
			}

			for ( $i = 1; $i < 4; $i ++ ) {
				if ( ! isset( $ItemAttributes->{'attribute' . $i} ) ) {
					$ItemAttributes->{'attribute' . $i} = "";
				}
			}

			$ls_prod->ItemAttributes = $ItemAttributes;
		}

		/**
		 * Pushes a simple WC product to LightSpeed.
		 *
		 * @param WC_Product $wc_prod
		 *
		 * @return bool
		 */
		function push_simple_prod( WC_Product $wc_prod ) {
			global $WCLSI_API;

			$ls_prod_json = $this->ls_prod_json_builder( $wc_prod );

			$result = $WCLSI_API->make_api_call( 'Account.Item', 'Create', '', json_encode( $ls_prod_json ) );

			if ( ! is_wp_error( $result ) && isset( $result->Item->itemID ) ) {
				$this->handle_img_uploads( $result->Item, $wc_prod );
			} else {
				add_settings_error(
					'wclsi_settings',
					'bad_wc_to_ls_sync',
					$result->get_error_message(),
					'error'
				);

				return false;
			}

			$this->persist_ls_data( $result->Item, $wc_prod->get_id() );
		}

		/**
		 * Helper function for handling image uploads.
		 *
		 * @param $result
		 * @param WC_Product $wc_prod
		 */
		private function handle_img_uploads( &$result, WC_Product $wc_prod ) {

			$wc_imgs     = array();
			$img_results = array();

			if ( ! $wc_prod->is_type( 'variation' ) ) {
				$wc_imgs = $wc_prod->get_gallery_image_ids();
			} else {
				$variation_id = $wc_prod->variation_id;
			}

			// Add the featured image
			array_unshift( $wc_imgs, $wc_prod->get_image_id() );

			if ( empty( $wc_imgs ) ) {
				return;
			}

			$matrix_id = 0;
			if ( !isset( $result->itemID ) && isset( $result->itemMatrixID ) && $result->itemMatrixID > 0 ) {
				$id_ref    = "itemMatrixID";
				$matrix_id = $result->itemMatrixID;
			} elseif ( $result->itemID > 0 ) {
				$id_ref = "itemID";
			}

			// upload gallery images
			$img_errors = array();
			if ( ! empty( $wc_imgs ) && is_array( $wc_imgs ) && ! empty( $id_ref ) ) {
				foreach ( $wc_imgs as $img_id ) {
					$img_result = $this->upload_img( $result->{$id_ref}, $img_id, $matrix_id );

					if ( is_wp_error( $img_result ) ) {
						$img_errors[] = $img_result;
					} else {
						$img_results[] = $img_result;
					}
				}
			} else {
				/**
				 * We have to use get_post_thumbnail() for variation prods since
				 * they extend WC_Product which will default to the parent thumb id.
				 */
				$single_img_id = isset( $variation_id ) ? get_post_thumbnail_id( $variation_id ) : $wc_prod->get_image_id();

				if ( $single_img_id > 0 && ! empty( $id_ref ) ) {
					$img_result = $this->upload_img( $result->{$id_ref}, $single_img_id, $matrix_id );
					if ( is_wp_error( $img_result ) ) {
						$img_errors[] = $img_result;
					} else {
						$img_results[] = $img_result;
					}
					$img_results[] = $img_result;
				}
			}

			if ( ! empty( $img_results ) ) {
				$result->Images = new stdClass();
				if ( count( $img_results ) > 1 ) {
					$result->Images->Image = $img_results;
				} else if ( count( $img_results ) == 1 ) {
					$result->Images->Image = $img_results[0];
				}
			}

			if ( ! empty( $img_errors ) ) {
				foreach ( $img_errors as $img_error ) {
					add_settings_error(
						'wclsi_settings',
						'bad_wc_to_ls_sync_img_upload',
						$img_error->get_error_message(),
						'error'
					);
				}
			}
		}

		/**
		 * Adds timestamps to LS objects, as well as appends them to
		 * option caches.
		 *
		 * @param $ls_prod
		 * @param $wc_prod_id
		 */
		function persist_ls_data( $ls_prod, $wc_prod_id ) {

			$ls_prod->wc_prod_id           = $wc_prod_id;
			$ls_prod->wclsi_is_synced      = true;
			$ls_prod->wclsi_import_date    = current_time( 'mysql' );
			$ls_prod->wclsi_last_sync_date = current_time( 'mysql' );

			update_post_meta( $wc_prod_id, WCLSI_SYNC_POST_META, true );

			$wclsi_id = WCLSI_Lightspeed_Prod::insert_ls_api_item( $ls_prod );

			$item = new WCLSI_Lightspeed_Prod( $wclsi_id );

			if( $item->is_matrix_product() ) {
				update_post_meta( $wc_prod_id, WCLSI_MATRIX_ID_POST_META, $item->item_matrix_id );
			} elseif ( $item->is_simple_product() ) {
				update_post_meta( $wc_prod_id, WCLSI_SINGLE_ITEM_ID_POST_META, $item->item_id );
			} elseif ( $item->is_variation_product() ) {
				update_post_meta( $wc_prod_id, WCLSI_SINGLE_ITEM_ID_POST_META, $item->item_id );
			}
		}

		/**
		 * Uploads the associated image, if one exists
		 *
		 * @param $ls_prod_id
		 * @param $wc_img_prod_id
		 * @param $matrix_id
		 *
		 * @return boolean
		 */
		function upload_img( $ls_prod_id, $wc_img_prod_id, $matrix_id = 0 ) {
			global $WCLSI_API;

			if ( ! function_exists( 'curl_init' ) || ! function_exists( 'curl_exec' ) || ! class_exists( 'CURLFile' ) ) {
				return false;
			}

			$add_headers = function ( WP_MOScURL &$curl, &$body ) use ( $wc_img_prod_id, $matrix_id ) {

				$headers = array(
					'accept'    => 'application/xml',
					'wc-img-id' => $wc_img_prod_id
				);

				if ( $matrix_id > 0 ) {
					$headers['matrix-id'] = $matrix_id;
				}

				$curl->setHTTPHeader( $headers );
			};

			$item_path = $matrix_id > 0 ? "/ItemMatrix/" : "/Item/";

			$result = $WCLSI_API->make_api_call(
				'Account/' . $WCLSI_API->ls_account_id . $item_path . $ls_prod_id . '/Image',
				'Create',
				null,
				null,
				null,
				$add_headers
			);

			return $result;
		}

		/**
		 * Hack that circumvents body and Content-Length manipulation by WordPress and
		 * allows cURL to build a multi-part using a CURLFile instead of breaking down the body
		 * using http_build_query().
		 *
		 * This is specific to PUT/POST requests.
		 *
		 * @see https://github.com/WordPress/WordPress/blob/master/wp-includes/class-http.php#L327-L340
		 * @see wp-includes/class-http.php
		 *
		 * @param $curl_handle
		 * @param $r
		 * @param $url
		 */
		function fix_curl_opts_for_img_upload( $curl_handle, $r, $url ) {

			if ( isset( $r['headers']['wc-img-id'] ) ) {

				$wc_img_prod_id = (int) $r['headers']['wc-img-id'];

				$file_path = get_attached_file( $wc_img_prod_id );

				// Default to thumbnail file path to optimize large image uploads
				$img_meta_data = wp_get_attachment_metadata( $wc_img_prod_id );
				$thumb         = false;
				if ( isset( $img_meta_data['sizes']['shop_single'] ) ) {
					$thumb = $img_meta_data['sizes']['shop_single']['file'];
				} else if ( isset( $img_meta_data['sizes']['shop_catalog'] ) ) {
					$thumb = $img_meta_data['sizes']['shop_catalog']['file'];
				} else if ( isset( $img_meta_data['sizes']['thumbnail'] ) ) {
					$thumb = $img_meta_data['sizes']['thumbnail']['file'];
				}

				$file_path = $thumb ? str_replace( basename( $file_path ), $thumb, $file_path ) : $file_path;

				$img_file = apply_filters( 'wclsi_sync_to_ls_img_path', $file_path );

				if ( false !== $img_file ) {

					$matrix_id     = isset( $r['headers']['matrix-id'] ) ? (int) $r['headers']['matrix-id'] : false;
					$matrix_id_xml = $matrix_id > 0 ? '<itemMatrixID>' . $matrix_id . '</itemMatrixID>' : '';

					$body = array(
						"data"  => "<Image><filename>" . basename( $img_file ) . "</filename>" . $matrix_id_xml . "</Image>",
						"image" => new CURLFile( $img_file, mime_content_type( $img_file ), basename( $img_file ) )
					);

					unset( $r['headers']['Content-Length'] );
					unset( $r['headers']['wc-img-id'] );
					unset( $r['headers']['matrix-id'] );

					$headers = array();
					foreach ( $r['headers'] as $name => $value ) {
						$headers[] = "{$name}: $value";
					}

					curl_setopt( $curl_handle, CURLOPT_HTTPHEADER, $headers );
					curl_setopt( $curl_handle, CURLOPT_POSTFIELDS, $body );
				}
			}
		}

		/**
		 * Given a WC prod, returns an object representation of LS prod.
		 *
		 * @param WC_Product $wc_prod
		 * @param int $matrix_id
		 *
		 * @return mixed|void
		 */
		function ls_prod_json_builder( WC_Product $wc_prod, $matrix_id = 0 ) {

			/** Setup basic fields **/
			$ls_prod              = new stdClass();
			$ls_prod->customSku   = $wc_prod->get_sku();
			$ls_prod->description = $wc_prod->get_title();
			$ls_prod->defaultCost = empty( $wc_prod->get_regular_price() ) ? '0.00' :  $wc_prod->get_regular_price();

			if ( $matrix_id > 0 ) {
				$attributes           = $wc_prod->get_variation_attributes();
				$ls_prod->description = $ls_prod->description . ' ' . implode( ' ', $attributes );
			}

			if ( $matrix_id > 0 ) {
				$ls_prod->itemMatrixID = $matrix_id;
			}

			/** @todo Categories: handling new categories & multiple categories * */
			$wc_cats  = wp_get_post_terms( $wc_prod->get_id(), 'product_cat' );

			if ( !empty( $wc_cats ) && !is_wp_error( $wc_cats ) ) {

				// Use the first category by default
				if ( isset( $wc_cats[0]->term_id ) ) {
					global $wpdb, $WCLSI_ITEM_CATEGORIES_TABLE;
					$table = $wpdb->prefix . $WCLSI_ITEM_CATEGORIES_TABLE;
					$wc_cat_id = $wc_cats[0]->term_id;
					$ls_cat_id = $wpdb->get_var("SELECT category_id FROM $table WHERE wc_cat_id = $wc_cat_id");
					if ( !is_null( $ls_cat_id ) ) {
						$ls_prod->categoryID = $ls_cat_id;
					}
				}
			}

			$this->build_item_e_commerce( $ls_prod, $wc_prod );

			$this->build_pricing( $ls_prod, $wc_prod, false );

			if ( $wc_prod->is_type( 'simple' ) ) {
				$this->build_shop_data( $ls_prod, $wc_prod );

				return apply_filters( 'wclsi_sync_to_ls_simple_prod', $ls_prod, $wc_prod );
			} else if ( $wc_prod->is_type( 'variable' ) ) {
				return apply_filters( 'wclsi_sync_to_ls_matrix_prod', $ls_prod, $wc_prod );
			} else if ( $wc_prod->is_type( 'variation' ) ) {
				$this->build_shop_data( $ls_prod, $wc_prod );

				return apply_filters( 'wclsi_sync_to_ls_variation_prod', $ls_prod, $wc_prod );
			} else {
				return false;
			}
		}

		/**
		 * Helper function to build shop data for inventory
		 *
		 * @param $ls_prod
		 * @param WC_Product $wc_prod
		 *
		 * @return bool
		 */
		private function build_shop_data( &$ls_prod, WC_Product &$wc_prod ) {
			$shop_data = get_option( 'wclsi_shop_data' );
			if ( isset( $shop_data['ls_store_data'] ) ) {
				$shop_data = $shop_data['ls_store_data'];
			} else {
				$shop_data = false;
			}

			$inventory = $wc_prod->get_stock_quantity();

			$ItemShops = array();
			if ( false !== $shop_data && isset( $shop_data->Shop ) && is_array( $shop_data->Shop ) ) {
				foreach ( $shop_data->Shop as $shop ) {
					$ItemShop                   = new stdClass();
					$ItemShop->ItemShop         = new stdClass();
					$ItemShop->ItemShop->shopID = $shop->shopID;
					$ItemShop->ItemShop->qoh    = empty( $inventory ) ? 0 : $inventory;
					$ItemShops[]                = $ItemShop;
				}
			} else if ( false !== $shop_data && isset( $shop_data->Shop ) && is_object( $shop_data->Shop ) ) {
				$ItemShop                   = new stdClass();
				$ItemShop->ItemShop         = new stdClass();
				$ItemShop->ItemShop->shopID = $shop_data->Shop->shopID;
				$ItemShop->ItemShop->qoh    = empty( $inventory ) ? 0 : $inventory;
				$ItemShops[]                = $ItemShop;
			} else {
				return false;
			}
			$ls_prod->ItemShops = $ItemShops;

			return true;
		}

		/**
		 * Helper function to build meta data to push a LS product
		 *
		 * @param $ls_prod
		 * @param WC_Product $wc_prod
		 */
		private function build_item_e_commerce( &$ls_prod, WC_Product &$wc_prod ) {
			$itemECommerce = new stdClass();
			$itemECommerce->longDescription  = $wc_prod->get_description();
			$itemECommerce->shortDescription = $wc_prod->get_short_description();

			$weight = $wc_prod->get_weight();
			$width  = $wc_prod->get_width();
			$height = $wc_prod->get_height();
			$length = $wc_prod->get_length();

			$itemECommerce->weight      = empty( $weight ) ? 0 : $weight;
			$itemECommerce->width       = empty( $width )  ? 0 : $width;
			$itemECommerce->height      = empty( $height ) ? 0 : $height;
			$itemECommerce->length      = empty( $length ) ? 0 : $length;
			$ls_prod->ItemECommerce     = $itemECommerce;
		}

		/**
		 * Helper function to build pricing modules to push a LS product
		 *
		 * @param $ls_prod
		 * @param WC_Product $wc_prod
		 * @param $set_sale_price
		 */
		private function build_pricing( &$ls_prod, WC_Product &$wc_prod, $set_sale_price = true ) {
			$ls_prod->Prices = array();

			$ItemPriceDefault                     = new stdClass();
			$ItemPriceDefault->ItemPrice          = new stdClass();
			$ItemPriceDefault->ItemPrice->useType = 'default';
			$ItemPriceDefault->ItemPrice->amount  =
				empty( $wc_prod->get_regular_price() ) ? '0.00' :  $wc_prod->get_regular_price();

			$ItemPriceMSRP                     = new stdClass();
			$ItemPriceMSRP->ItemPrice          = new stdClass();
			$ItemPriceMSRP->ItemPrice->useType = 'MSRP';
			$ItemPriceMSRP->ItemPrice->amount  =
				empty( $wc_prod->get_regular_price() ) ? '0.00' :  $wc_prod->get_regular_price();

			$ls_prod->Prices[] = $ItemPriceDefault;
			$ls_prod->Prices[] = $ItemPriceMSRP;

			// Not all stores may have a "Sale" price level set
			if ( $set_sale_price ) {
				$ItemPriceSale                     = new stdClass();
				$ItemPriceSale->ItemPrice          = new stdClass();

				$ItemPriceSale->ItemPrice->amount  =
					empty( $wc_prod->get_sale_price() ) ? '0.00' :  $wc_prod->get_sale_price();

				$ItemPriceSale->ItemPrice->useType = 'Sale';
				$ls_prod->Prices[] = $ItemPriceSale;
			}
		}

		/**
		 * Checks for new LightSpeed products since last import
		 */
		public static function check_for_new_ls_prods() {
			global $WCLSI_API, $WCLSI_SINGLE_PROD_SEARCH_PARAMS;

			$autoload_enabled =
				isset( $WCLSI_API->settings[WCLSI_LS_TO_WC_AUTOLOAD] ) ? $WCLSI_API->settings[WCLSI_LS_TO_WC_AUTOLOAD] : false;

			if ( empty( $autoload_enabled ) ) { return; }

			$last_load_timestamp = get_option( WCLSI_LAST_LOAD_TIMESTAMP );

			if ( !empty( $last_load_timestamp ) && wclsi_oauth_enabled() && !empty( $WCLSI_API->ls_account_id ) ) {

				if( false === get_transient( WCLSI_NEW_PROD_TRANSIENT ) ) {
					$search_params = array(
						'load_relations' => json_encode( $WCLSI_SINGLE_PROD_SEARCH_PARAMS ),
						'createTime' => '>,' . $last_load_timestamp
					);

					$response = $WCLSI_API->make_api_call(
						"Account/$WCLSI_API->ls_account_id/Item/",
						'Read',
						$search_params
					);

					if ( !is_wp_error( $response ) && $response->{'@attributes'}->count > 0 ) {
						if ( is_object( $response->Item ) ) {
							$response->Item = array( $response->Item );
						}

						$ls_api_items = $response->Item;
						$prod_list = '';
						foreach( $ls_api_items as $ls_api_item ) {
							$wclsi_item = new WCLSI_Lightspeed_Prod( self::insert_new_ls_item( $ls_api_item ) );
							$sku = wclsi_get_ls_sku( $wclsi_item );
							$prod_list .= "<li>Description: \"$wclsi_item->description\" SKU: \"$sku\"</li>";
						}

						self::render_add_new_prod_html( $response->{'@attributes'}->count, $prod_list );

						update_option( WCLSI_LAST_LOAD_TIMESTAMP, date( DATE_ATOM, strtotime( 'now' ) ) );
						delete_transient( WCLSI_NEW_PROD_TRANSIENT );
					} else {
						set_transient( WCLSI_NEW_PROD_TRANSIENT, true, 5 ); // expiration of 5 seconds
					}
				}
			}
		}

		/**
		 * @param $prod_count
		 * @param $prod_list
		 */
		private static function render_add_new_prod_html( $prod_count, $prod_list ) {
			$matrix_note =
				__(
					'<p>Note: variation products will be consolidated into their parent matrix/variable product.</p>',
					'woocommerce-lightspeed-pos'
				);

			$notification_msg =
				__(
					'Good news! %d new Lightspeed item(s) have been added to the Lightspeed import table:<br/>%s%s',
					'woocommerce-lightspeed-pos'
				);

			add_settings_error(
				'wclsi_settings',
				'wclsi_new_items',
				sprintf(
					$notification_msg,
					$prod_count,
					"<ul style='margin-left:30px;list-style:square'>$prod_list</ul>",
					$matrix_note
				),
				'updated'
			);
		}

		/**
		 * Inserts newly-added Lightspeed products
		 */
		private static function insert_new_ls_item( $ls_api_item ) {
			if ( empty( $ls_api_item ) ) { return 0; }

			global $WCLSI_API;

			// If the product is a variation and its parent does not exist, insert its parent as well
			if ( $ls_api_item->itemMatrixID > 0 &&
				 ( WCLSI_Lightspeed_Prod::get_mysql_id( null, $ls_api_item->itemMatrixID ) === null ) ) {
				global $WCLSI_MATRIX_PROD_SEARCH_PARAMS;

				$matrix_ls_api_item_result = $WCLSI_API->make_api_call(
					"Account/$WCLSI_API->ls_account_id/ItemMatrix/$ls_api_item->itemMatrixID",
					"Read",
					array( 'load_relations' => json_encode( $WCLSI_MATRIX_PROD_SEARCH_PARAMS ) )
				);

				$matrix_ls_api_item = $matrix_ls_api_item_result->ItemMatrix;

				WCLSI_Lightspeed_Prod::insert_ls_api_item( $matrix_ls_api_item );
			} else {
				$wclsi_id = WCLSI_Lightspeed_Prod::insert_ls_api_item( $ls_api_item );

				$wclsi_parent_prod = new WCLSI_Lightspeed_Prod();
				$wclsi_parent_prod->init_via_item_matrix_id( $ls_api_item->itemMatrixID );

				// If the parent has been imported, then import the variation as well
				if ( $wclsi_parent_prod->wc_prod_id > 0 ) {
					global $WCLSI_PRODS;
					$wclsi_prod = new WCLSI_Lightspeed_Prod( $wclsi_id );
					$WCLSI_PRODS->update_matrix_variations( array( $wclsi_prod ), $wclsi_parent_prod->wc_prod_id );
					delete_transient( 'wc_product_children_' . $wclsi_parent_prod->wc_prod_id  );
				}

				return $wclsi_id;
			}
		}
	}

	global $WCLSI_SYNCER;
	$WCLSI_SYNCER = new LSI_Synchronizer();
	$WCLSI_SYNCER->wclsi_sync_setup_schedule();
endif;
