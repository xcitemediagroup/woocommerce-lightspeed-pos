<?php
/**
 * @class LSI_Import_Product
 * Handles imports/conversions/syncing of LightSpeed items and WooCommerce Products.
 */
if ( !class_exists( 'LSI_Import_Products' ) ) :

	class LSI_Import_Products {

		const MAX_NUM_OF_ATTRIBUTES = 3;

		function __construct() {
			add_action( 'before_delete_post', array( $this, 'sync_deleted_prods') );
			add_action( 'wp_ajax_get_prod_ids_ajax', array( $this, 'get_prod_ids_ajax') );
			add_action( 'wp_ajax_wclsi_load_prods_ajax', array( $this, 'wclsi_load_prods_ajax' ) );

			add_action( 'wp_ajax_import_all_lightspeed_products_ajax', array( $this, 'import_all_lightspeed_products_ajax') );
			add_action( 'wp_ajax_import_and_sync_product_ajax', array( $this, 'import_and_sync_product_ajax' ) );
			add_action( 'wp_ajax_import_product_ajax', array( $this, 'import_product_ajax' ) );

			add_action( 'wp_ajax_get_import_progress_ajax', array( $this, 'get_import_progress_ajax' ) );
			add_action( 'wp_ajax_manual_prod_update_ajax', array( $this, 'manual_prod_update_ajax' ) );

			add_action( 'wp_ajax_set_prod_sync_ajax', array( $this, 'set_prod_sync_ajax' ) );
			add_action( 'updated_post_meta', array( $this, 'set_sync_value'), 10, 4 );
		}

		function import_item( $item, $sync = false, $img_flag = false ) {

			$item = apply_filters( 'wclsi_import_product', $item );
			$post_id = false;

			if ( wclsi_is_matrix_product( $item ) ) {
				set_time_limit( 180 ); // extend to 3 mins for matrix products
				$post_id = $this->import_matrix_item( $item, $sync, $img_flag );
			} elseif ( wclsi_is_simple_product( $item ) ) {
				$post_id = $this->import_single_item( $item, $sync, $img_flag );
			}
			return $post_id;
		}
		
		function import_matrix_item( $wclsi_matrix_item, $sync = false, $img_flag = false ) {
			if ( false !== wc_get_product( $wclsi_matrix_item->wc_prod_id ) ) {
				return $wclsi_matrix_item->wc_prod_id;
			}

			$wclsi_matrix_item = apply_filters( 'wclsi_import_ls_data_matrix_prod', $wclsi_matrix_item );
			$wc_variable_prod  = $this->init_wc_product( $wclsi_matrix_item );
			$this->set_wc_prod_values( $wc_variable_prod, $wclsi_matrix_item );
			$post_id = $wc_variable_prod->save();

			if ( !is_wp_error( $post_id ) ) {
				// Get & set the category
				$this->handle_product_taxonomy( $wclsi_matrix_item, $post_id );

				// Set attributes
				$variations = wclsi_get_matrix_prods( $wclsi_matrix_item->item_matrix_id );

				$attrs = $this->get_attributes( $variations );

				if( !empty( $attrs ) ) {
					$this->set_item_attributes(
						$wclsi_matrix_item->item_attribute_set_id,
						$post_id,
						apply_filters( 'wclsi_import_attributes_matrix_item', $attrs, $post_id )
					);
				}

				$this->create_variations(
					apply_filters( 'wclsi_import_variations_matrix_item', $variations, $post_id),
					$sync,
					$post_id
				);

				// set wc_prod_id so product images can get saved correctly
				$wclsi_matrix_item->wc_prod_id = $post_id;
				if( !$img_flag ) {
					$prod_imgs = $this->save_ls_prod_images( $wclsi_matrix_item );
					if ( !is_wp_error( $prod_imgs ) && !empty( $prod_imgs ) ) {
						$this->set_wc_images( $post_id, $prod_imgs );
					}
				}

				if ( $sync ) {
					update_post_meta( $post_id, WCLSI_SYNC_POST_META, true );
				}

				$this->persist_import_data( $wclsi_matrix_item, $post_id, $sync );
			}

			return $post_id;
		}

		function import_single_item( $item, $sync, $img_flag = false ) {
			if ( false !== wc_get_product( $item->wc_prod_id ) ) {
				return $item->wc_prod_id;
			}

			$item = apply_filters( 'wclsi_import_ls_result_single_prod', $item );
			$wc_simple_prod = $this->init_wc_product( $item );
			$this->set_wc_prod_values( $wc_simple_prod, $item );
			$post_id = $wc_simple_prod->save();

			if ( !is_wp_error( $post_id ) ) {

				$this->handle_product_taxonomy( $item, $post_id );

				// Add a sync flag so our cron-job will know to sync this product
				if ( $sync ) {
					update_post_meta( $post_id, WCLSI_SYNC_POST_META, true );
				}

				// Get the product images
				if( !$img_flag ) {
					$prod_imgs = $this->save_ls_prod_images( $item );
					$prod_imgs = apply_filters( 'wclsi_import_prod_imgs_single_prod', $prod_imgs, $post_id );
					if ( !is_wp_error( $prod_imgs ) && count( $prod_imgs ) > 0 ) {
						$this->set_wc_images( $post_id, $prod_imgs );
					}
				}

				$item->wc_prod_id  = $post_id;
				$item->last_import = current_time('mysql');

				$this->persist_import_data( $item, $post_id, $sync );
			}

			return $post_id;
		}

		function set_item_attributes( $attr_set_id, $post_id, $attr_values ) {

			$attr_set = new WCLSI_Lightspeed_Item_Attribute_Set( $attr_set_id );

			if ( $attr_set->id > 0 ) {

				$attr_data = array();
				$position = 0;

				for( $i = 1; $i <= self::MAX_NUM_OF_ATTRIBUTES; $i++ ) {
					$name = $attr_set->{'attribute_name_' . $i};
					if ( ! is_null( $name ) ) {
						$slug               = sanitize_title( $name );
						$attr_data[ $slug ] = array(
							'name'         => $name,
							'value'        => implode( '|', $attr_values[ $attr_set_id ][ $slug ] ),
							'position'     => $position ++,
							'is_visible'   => 1,
							'is_variation' => 1,
							'is_taxonomy'  => 0
						);
					}
				}

				update_post_meta( $post_id,'_product_attributes', $attr_data );
			}
		}

		function update_wc_prod( WCLSI_Lightspeed_Prod $ls_prod, $update_variations = true ) {

			$ls_prod = apply_filters( 'wclsi_update_product', $ls_prod );

			if ( $ls_prod->is_simple_product() || $ls_prod->is_variation_product() ) {
				return $this->update_single_item(  $ls_prod );
			} elseif ( $ls_prod->is_matrix_product() ) {
				return $this->update_matrix_item( $ls_prod, $update_variations );
			}
			return new WP_Error( 'wclsi_bad_update', __( 'Could not process update, invalid product!', 'woocomerce-lightspeed-pos' ) );
		}

		function update_matrix_variations( $variations, $parent_id ) {
			$new_variations_to_import = array();

			foreach ( $variations as $variation ) {
				if ( $variation->wc_prod_id > 0 ) {
					$this->update_single_item( $variation );
				} elseif ( is_null( $variation->wc_prod_id ) ) {
					$new_variations_to_import[] = $variation;
				}
			}

			if ( !empty( $new_variations_to_import ) ) {
				// Get the itemAttributeSetID from a random variation
				$attr_set_id = $variations[0]->item_attributes->itemAttributeSetID;

				// Re-set attributes in case new variation introduces new attribute(s)
				$wc_parent_prod = new WCLSI_Lightspeed_Prod();
				$wc_parent_prod->init_via_wc_prod_id( $parent_id );
				$all_variations = array_merge($wc_parent_prod->variations, $variations);

				$this->set_item_attributes( $attr_set_id, $parent_id, $this->get_attributes($all_variations) );

				$sync = get_post_meta( $parent_id, WCLSI_SYNC_POST_META, true );
				$this->create_variations( $new_variations_to_import, $sync, $parent_id );
			}
		}

		function save_ls_prod_images( $item ) {
			global $WCLSI_API;
			$selective_sync = $WCLSI_API->settings[ 'wclsi_wc_selective_sync' ];
			if ( empty( $selective_sync[ 'images'] ) ) {
				return array();
			}

			$prod_imgs = !is_null( $item->images ) ? $item->images : null;
			$img_attach_ids = array();

			if ( !is_null( $prod_imgs ) ) {
				foreach( $prod_imgs as $prod_img ) {

					if( $prod_img->wp_attachment_id > 0 ) {
						// Important: if an image got deleted at some point, then we will skip this
						if( wp_attachment_is_image( $prod_img->wp_attachment_id ) ) {
							$img_attach_ids[ $prod_img->ordering ] = $prod_img->wp_attachment_id;
							continue;
						}
					}

					$attach_id = $this->get_wc_attach_id( $prod_img, $item->wc_prod_id );
					if ( is_wp_error( $attach_id ) ) {
						return $attach_id;
					} elseif ( $attach_id > 0 ) {

						// If an order is set on the image, try to add it in order, otherwise just push it
						if ( $prod_img->ordering >= 0 && !isset( $img_attach_ids[ $prod_img->ordering ] ) ) {
							$img_attach_ids[ $prod_img->ordering ] = $attach_id;
						} else {
							$img_attach_ids[] = $attach_id;
						}
					}
				}
				ksort( $img_attach_ids );
			}

			return $img_attach_ids;
		}

		function get_wc_attach_id( $prod_img, $post_id ) {

			if ( isset( $prod_img->filename ) && isset( $prod_img->base_image_url) && isset( $prod_img->public_id ) ) {
				global $wpdb, $WCLSI_ITEM_IMAGES_TABLE;

				$upload_dir = wp_upload_dir();
				$img_ext   = pathinfo( $prod_img->filename, PATHINFO_EXTENSION);
				$img_url   = $prod_img->base_image_url . 'q_auto:eco/' . $prod_img->public_id . '.' . $img_ext;
				$save_path = $upload_dir['path'] . DIRECTORY_SEPARATOR . uniqid() . '.' . $img_ext;

				$result = file_get_contents( $img_url );

				if( false === $result ) {

					$error_msg =
						"There was an issue with downloading images from Lightspeed. " .
						"Please make sure that your server meets the plugin requirements, it could be that " .
						"'allow_url_fopen' is not enabled in your php.ini configuration file.";

					if( is_admin() ) {
						add_settings_error(
							'wclsi_settings',
							'file_get_contents_failed',
							$error_msg
						);
					}

					return new WP_Error( 'file_get_contents_failed', $error_msg );
				}

				file_put_contents( $save_path, $result );
				if ( file_exists( $save_path ) ) {
					$attach_id = wc_ls_create_attachment( $save_path, $post_id, $prod_img->description );

					$table = $wpdb->prefix . $WCLSI_ITEM_IMAGES_TABLE;
					$wpdb->update(
						$table,
						array( 'wp_attachment_id' => $attach_id ),
						array( 'image_id' => $prod_img->image_id ),
						array( '%d' ),
						array( '%d' )
					);

					return $attach_id;
				}
			}
			return 0;
		}

		/**
		 * Uses the 'delete_post' action hook when a product gets deleted and removes the "last_import" and
		 * "last_sync" fields of the product in the wclsi cache.
		 * @param $wc_prod_id
		 */
		function sync_deleted_prods( $wc_prod_id ) {
			$ls_prod = new WCLSI_Lightspeed_Prod();
			$ls_prod->init_via_wc_prod_id( $wc_prod_id );
			$ls_prod->clear_wc_prod_association();
		}

		/**
		 * Check if product id exists in the cache, if it doesn't, add it to the cache
		 * Data that is saved:
		 * - Product title
		 * - Description
		 * - Price
		 * - Images
		 * - Inventory
		 *
		 * Cache data structure:
		 * - Last import (includes date) // date
		 * - Imported products // array of products
		 */
		function save_loaded_products() {

			$wc_ls_prod_cache = get_option( 'wc_ls_imported_prod_cache' ); // imported product cache

			if ( isset( $wc_ls_prod_cache ) && count( $wc_ls_prod_cache->Item ) > 0 ) {
				$curr_timestamp = current_time( 'mysql' );
				foreach( $wc_ls_prod_cache->Item as $item_key => $item ) {
					$item->last_import = $curr_timestamp;
					$wc_ls_prod_cache->Item[ $item->itemID ] = $item;
				}

				$wc_ls_prod_cache->last_import = current_time( 'mysql' );
				update_option( 'wc_ls_imported_prod_cache', $wc_ls_prod_cache );
			}
		}

		function get_prod_ids_ajax() {
			wclsi_verify_nonce();

			if ( isset( $_POST[ 'init_import' ] ) && true == $_POST[ 'init_import' ] ) {
				$prod_ids = wclsi_get_ls_prod_ids( true );
				echo json_encode( $prod_ids );
				exit;
			}
		}

		function import_all_lightspeed_products_ajax(){
			$this->process_product_ajax_request( false );
		}

		function import_product_ajax(){
			$this->process_product_ajax_request( false );
		}

		function import_and_sync_product_ajax(){
			$this->process_product_ajax_request();
		}

		/**
		 * Imports and adds a sync flag to all the loaded products from LightSpeed
		 * @Param - $_POST['init_import'] - Will return all existing prods
		 */
		function process_product_ajax_request( $sync = true ) {
			wclsi_verify_nonce();

			$prod_id = isset( $_POST['prod_id'] ) ? (int) $_POST['prod_id'] : false;

			if ( false !== $prod_id ) {
				$ls_item = new WCLSI_Lightspeed_Prod( $prod_id );

				if ( $ls_item->id > 0 ) {
					$wc_prod_id = $this->import_item( $ls_item, $sync );
					if ( !is_wp_error( $wc_prod_id ) ) {
						echo json_encode( array( 'success' => $wc_prod_id, 'prod_id' => $prod_id ) );
					} else {
						echo json_encode(
							array(
								'errors' => array(
									array( 'message' => $wc_prod_id->get_error_message() )
								)
							)
						);
						exit;
					}
				} else {
					echo json_encode(
						array(
							'errors' => array(
								array( 'message' => sprintf( __( 'Product with ID %d does not exist!', 'woocommerce-lightspeed-pos' ), $prod_id ) )
							)
						)
					);
					exit;
				}
			} else {
				echo json_encode(
					array(
						'errors' => array(
							array( 'message' => __( 'Could not find a product ID to import!', 'woocommerce-lightspeed-pos' ) )
						)
					)
				);
				exit;
			}
			exit;
		}

		/**
		 * Starts the load process via an AJAX call. Returns the total number of products to the JS script.
		 * JS script then makes (at most) 40 calls a minute as to not exceed API throttling.
		 */
		function wclsi_load_prods_ajax() {
			wclsi_verify_nonce();

			$offset = 0;
			if ( isset( $_POST['offset'] ) ) {
				$offset = (int) $_POST['offset'];
			}

			$limit = 1;
			if ( isset( $_POST['limit'] ) ) {
				$limit = (int) $_POST['limit'];
			}

			// Flag to get total count of products to load
			$get_count = $_POST['getCount'] === 'true' ? true : false;

			$load_matrix_prods = $_POST['getMatrix'] == 'true' ? true : false;

			if ( $load_matrix_prods ) {
				$result = $this->pull_matrix_items_by_offset( $offset, $limit );
				$prod_identifier = 'ItemMatrix';
			} else {
				$result = $this->pull_simple_items_by_offset( $offset, $limit );
				$prod_identifier = 'Item';
			}

			if ( isset( $result ) && !is_wp_error( $result ) ) {
				if ( $get_count ) {
					$count = ceil( $result->{'@attributes'}->count / 100 ) * 100;

					echo json_encode(
						array(
							'count' => $count,
							'real_count' => $result->{'@attributes'}->count,
							'errors' => get_settings_errors( 'wclsi_settings' )
						)
					);
				} else {
					if( isset( $result->{$prod_identifier} ) ) {
						$ls_api_items = $result->{$prod_identifier};
					} else {
						$ls_api_items = null;
					}

					if ( isset( $ls_api_items ) ) {
						if( is_object( $ls_api_items ) ) {
							$single_item = $ls_api_items;
							$ls_api_items = array( $single_item );
						}

						foreach( $ls_api_items as $item ) {
							WCLSI_Lightspeed_Prod::insert_ls_api_item( $item );
						}
					}

					if ( ($offset + $limit) >= $result->{'@attributes'}->count ) { // Last chunk condition
						global $WCLSI_API;
						date_default_timezone_set( $WCLSI_API->store_timezone );
						update_option( WCLSI_LAST_LOAD_TIMESTAMP, date( DATE_ATOM ) );

						// We load matrix prods after simple prods, before we finish we should pull the item_attr_sets
						if( $load_matrix_prods ) {
							self::pull_item_attribute_sets();
						}

						echo json_encode(
							array(
								'loaded_chunk_' . $offset => true,
								'redirect' => admin_url('admin.php?page=lightspeed-import-page'),
								'errors' => get_settings_errors( 'wclsi_settings' ),
								'matrix' => $load_matrix_prods
							)
						);

					} else {
						echo json_encode(
							array(
								'loaded_chunk_' . $offset => true,
								'errors' => get_settings_errors( 'wclsi_settings' ),
								'matrix' => $load_matrix_prods
							)
						);
					}
				}

			} elseif ( is_wp_error( $result ) ) {
				echo json_encode(
					array(
						'loaded_chunk_' . $offset => true,
						'errors' => $result->get_error_message(),
						'matrix' => $load_matrix_prods
					)
				);
			}
			exit;
		}

		function pull_simple_items_by_offset( $offset = 0, $limit = 100 ) {
			global $WCLSI_API, $WCLSI_SINGLE_PROD_SEARCH_PARAMS;

			$search_params = array(
				'load_relations' => json_encode( $WCLSI_SINGLE_PROD_SEARCH_PARAMS ),
				'offset' => $offset,
				'limit'  => $limit
			);

			if ( !empty( $WCLSI_API->ls_enabled_stores ) ) {
				$search_params = array_merge( $search_params, array( 'ItemShops.shopID' => 'IN,[' . implode( ',', $WCLSI_API->ls_enabled_stores ) . ']') );
			}

			$search_params = apply_filters( 'wclsi_ls_import_prod_params', $search_params );

			return $WCLSI_API->make_api_call( 'Account/' . $WCLSI_API->ls_account_id . '/Item/', 'Read', $search_params );
		}

		function pull_matrix_items_by_offset( $offset = 0, $limit = 100 ) {
			global $WCLSI_API, $WCLSI_MATRIX_PROD_SEARCH_PARAMS;

			$resource = 'Account/' . $WCLSI_API->ls_account_id . '/ItemMatrix';
			$search_params = array(
				'offset' => $offset,
				'load_relations' => json_encode( $WCLSI_MATRIX_PROD_SEARCH_PARAMS ),
				'limit' => $limit
			);

			if ( !empty( $WCLSI_API->ls_enabled_stores ) ) {
				$search_params = array_merge( $search_params, array( 'ItemShops.shopID' => 'IN,[' . implode( ',', $WCLSI_API->ls_enabled_stores ) . ']') );
			}

			$search_params = apply_filters( 'wclsi_ls_import_matrix_params', $search_params );

			return $WCLSI_API->make_api_call( $resource, 'Read', $search_params );
		}

		function pull_item_attribute_sets() {
			global $WCLSI_API;
			$item_attr_sets = $WCLSI_API->make_api_call( 'Account/' . $WCLSI_API->ls_account_id . '/ItemAttributeSet', 'Read' );

			if ( is_array( $item_attr_sets->ItemAttributeSet ) && !empty( $item_attr_sets->ItemAttributeSet ) ) {
				foreach( $item_attr_sets->ItemAttributeSet as $item_attr_set ) {
					WCLSI_Lightspeed_Item_Attribute_Set::insert_item_attribute_set( $item_attr_set );
				}
			}
		}

		/**
		 * Manually sync a product via the "Manual Sync" button in the LightSpeed metabox
		 * @see self::render_meta_box()
		 */
		function manual_prod_update_ajax() {
			wclsi_verify_nonce();

			if ( !isset( $_POST['prod_id'] ) ) {
				header( "HTTP/1.0 409 " . __('Could not find product ID.', 'woocommerce-lightspeed-pos' ) );
				exit;
			}

			$prod_id = (int) $_POST['prod_id'];
			$ls_prod = new WCLSI_Lightspeed_Prod();

			$ls_prod->init_via_wc_prod_id( $prod_id );

			if ( $ls_prod->id > 0 ) {
				$msg = WC_LS_Import_Table::process_single_update( $ls_prod->id );
				if ($msg['type'] == 'error') {
					header("HTTP/1.0 409 " . $msg['msg']);
					exit;
				} else {
					wp_send_json(
						array(
							'success' => true,
							'errors' => get_settings_errors( 'wclsi_settings' )
						)
					);
				}
			} else {
				header("HTTP/1.0 409 " . __( 'Could not find a product ID to update!', 'woocommerce-lightspeed-pos' ) );
				exit;
			}
			exit;
		}

		function set_prod_sync_ajax() {
			wclsi_verify_nonce();

			if ( !isset( $_POST['prod_id'] ) ) {
				header("HTTP/1.0 409 " . __( 'Could not find product ID.', 'woocommerce-lightspeed-pos' ) );
				exit;
			}

			if ( !isset( $_POST['sync_flag'] ) ) {
				header("HTTP/1.0 409 " . __( 'Could not find sync flag parameter.', 'woocommerce-lightspeed-pos' ) );
				exit;
			}

			$sync_flag = $_POST['sync_flag'] == 'true' ? true : false;
			$prod_id   = (int) $_POST['prod_id'];

			$ls_prod = new WCLSI_Lightspeed_Prod();
			$ls_prod->init_via_wc_prod_id( $prod_id );

			if( $ls_prod->id > 0 && $ls_prod->wc_prod_id > 0 ) {
				$variations = $ls_prod->variations;
				if( !empty( $variations ) ) {
					foreach( $variations as $variation ) {
						update_post_meta( $variation->wc_prod_id, WCLSI_SYNC_POST_META, $sync_flag );
					}
				}

				$success = update_post_meta( $prod_id, WCLSI_SYNC_POST_META, $sync_flag );
				wp_send_json( array( 'success' => $success ) );
			}

			exit;
		}

		/**
		 * Sets 'wclsi_is_synced' in the wclsi_items table when $meta_key = WCLSI_SYNC_POST_META
		 * @param $meta_id
		 * @param $object_id
		 * @param $meta_key
		 * @param $_meta_value
		 */
		function set_sync_value( $meta_id, $object_id, $meta_key, $_meta_value ) {
			if ( $meta_key == WCLSI_SYNC_POST_META ) {
				global $wpdb, $WCLSI_ITEM_TABLE;
				$table = $wpdb->prefix . $WCLSI_ITEM_TABLE;

				$wpdb->update(
					$table,
					array( 'wclsi_is_synced' => $_meta_value ),
					array( 'wc_prod_id' => $object_id ),
					array( '%d' ),
					array( '%d' )
				);
			}
		}

		/*******************
		 * Private Methods *
		 *******************/

		private function init_wc_product( WCLSI_Lightspeed_Prod $wclsi_item, $parent_id = 0 ) {
			global $WCLSI_API;

			if ( $wclsi_item->is_matrix_product() ) {
				$wc_product = new WC_Product_Variable();
			} elseif ( $wclsi_item->is_variation_product() ) {
				$wc_product = new WC_Product_Variation();
			} elseif ( $wclsi_item->is_simple_product() ) {
				$wc_product = new WC_Product_Simple();
			}

			if ( $wclsi_item->is_variation_product() ) {
				$wc_product->set_parent_id( $parent_id );
			}

			// Variation products should inherit status from parent?
			if ( !$wclsi_item->is_variation_product() ) {
				if ( isset( $WCLSI_API->settings[ 'wclsi_import_status' ] ) &&
					 $WCLSI_API->settings[ 'wclsi_import_status' ] == 'publish' ) {
					$wc_product->set_status( 'publish' );
				} else {
					$wc_product->set_status( 'draft' );
				}
			}

			return $wc_product;
		}

		private function set_wc_prod_values( WC_Product &$wc_product, WCLSI_Lightspeed_Prod $wclsi_item ) {
			global $WCLSI_API;
			$selective_sync = $WCLSI_API->settings[ 'wclsi_wc_selective_sync' ];
			$is_update = $wc_product->get_id() > 0;
			$set_wc_prop =
				function( $prop, $arg ) use(&$wc_product, &$selective_sync, &$is_update) {
					if ( is_null( $arg ) ) { return; }

					if( !$is_update ) {
						$wc_product->{"set_$prop"}( $arg );
					} elseif( $is_update && !empty( $selective_sync[ $prop ] ) ) {
						$wc_product->{"set_$prop"}( $arg );
					}
				};

			$set_wc_prop( 'name', $wclsi_item->description );
			$set_wc_prop( 'description', $wclsi_item->item_e_commerce->long_description );
			$set_wc_prop( 'short_description', $wclsi_item->item_e_commerce->short_description );

			$sku = wclsi_get_ls_sku( $wclsi_item );
			if( $this->is_valid_sku( $wclsi_item, $sku, $is_update ) ) {
				$set_wc_prop( 'sku', $sku );
			}

			$price = wclsi_get_lightspeed_price( $wclsi_item );
			$sale_price = wclsi_get_lightspeed_sale_price( $wclsi_item );
			$set_wc_prop( 'regular_price', $price );
			$set_wc_prop( 'price', $price );
			$set_wc_prop( 'sale_price', $sale_price );

			$set_wc_prop( 'weight', $wclsi_item->item_e_commerce->weight );
			$set_wc_prop( 'length', $wclsi_item->item_e_commerce->length );
			$set_wc_prop( 'width', $wclsi_item->item_e_commerce->width );
			$set_wc_prop( 'height', $wclsi_item->item_e_commerce->height );

			$stock_quantity = wclsi_get_lightspeed_inventory( $wclsi_item, true );
			$set_wc_prop( 'stock_quantity', $stock_quantity );

			if ( !$wc_product->is_type( 'variable' ) ) {
				if( $wc_product->get_stock_quantity() > 0 ) {
					$wc_product->set_stock_status( 'instock' );
				} elseif ( $wc_product->get_stock_quantity() <= 0 ) {
					$wc_product->set_stock_status( 'outofstock' );
				}
			}

			if( !$is_update ) {
				$wc_product->set_catalog_visibility( 'visible' );
				$manage_stock = $wclsi_item->is_matrix_product() ? 'no' : 'yes';
				$wc_product->set_manage_stock( $manage_stock );
			}
		}

		private function is_valid_sku( $ls_prod, $sku, $is_update = false ) {
			if( wclsi_is_matrix_product( $ls_prod ) || $is_update ){
				return true;
			}

			$post_id = wc_get_product_id_by_sku( $sku );
			if( $post_id > 0 ) {
				add_settings_error(
					'wclsi_settings',
					'existing_sku',
					__(
						sprintf
						(
							'Could not set SKU on product "%s": SKU value "%s" already exists.',
							$ls_prod->description,
							$sku
						),
						'woocommerce-lightspeed-pos'
					),
					'error'
				);
				return false;
			} else {
				return true;
			}
		}
		
		// @todo move this into WCLSI_Lightspeed_Product
		private function persist_import_data( WCLSI_Lightspeed_Prod $item, $wc_prod_id, $sync = false ) {
			global $wpdb, $WCLSI_ITEM_TABLE;

			$table_name = $wpdb->prefix . $WCLSI_ITEM_TABLE;

			$data = array(
				'wclsi_import_date' => current_time('mysql'),
				'wc_prod_id' => $wc_prod_id
			);

			if( $sync ) {
				$data['wclsi_is_synced'] = true;
			}

			$wpdb->update(
				$table_name,
				$data,
				array( 'id' => $item->id ),
				array( '%s', '%d' ),
				array( '%d' )
			);

			if( $item->is_matrix_product() ) {
				update_post_meta( $wc_prod_id, WCLSI_MATRIX_ID_POST_META, $item->item_matrix_id );
			} elseif ( $item->is_simple_product() ) {
				update_post_meta( $wc_prod_id, WCLSI_SINGLE_ITEM_ID_POST_META, $item->item_id );
			} elseif ( $item->is_variation_product() ) {
				update_post_meta( $wc_prod_id, WCLSI_SINGLE_ITEM_ID_POST_META, $item->item_id );
			}
		}

		private function set_wc_images( $post_id, $prod_imgs ) {
			if ( !empty( $prod_imgs ) ) {
				set_post_thumbnail( $post_id, $prod_imgs[0] );
				unset( $prod_imgs[0] ); // Remove the featured image from the set of gallery images
				update_post_meta( $post_id, '_product_image_gallery', implode( ',', $prod_imgs ) );
			} else {
				delete_post_thumbnail( $post_id );
				update_post_meta( $post_id, '_product_image_gallery', '' );
			}
		}		
		
		/**
		 * Given an array of variation lightspeed products, returns an array where the index is the itemAttributeSetID
		 * and the values are possible attribute values
		 * @param $variations
		 * @return array
		 */
		private function get_attributes( $variations ) {
			$possible_attrs = array();

			foreach( $variations as $variation ) {
				if ( !is_null( $variation->item_attributes ) ) {
					$attr_set_id = $variation->item_attributes->itemAttributeSetID;

					// Filter
					$attr_set = new WCLSI_Lightspeed_Item_Attribute_Set( $attr_set_id );

					$attr_slug_1 = sanitize_title( $attr_set->attribute_name_1 );
					$attr_slug_2 = sanitize_title( $attr_set->attribute_name_2 );
					$attr_slug_3 = sanitize_title( $attr_set->attribute_name_3 );

					// Grab the possible attribute values
					$item_attrs = array(
						$variation->item_attributes->attribute1,
						$variation->item_attributes->attribute2,
						$variation->item_attributes->attribute3
					);

					if ( !isset( $possible_attrs[ $attr_set_id ] ) ) {
						if ( !empty ( $attr_slug_1 ) ) {
							$possible_attrs[ $attr_set_id ][ $attr_slug_1 ] = array( $item_attrs[0] );
						}
						if ( !empty ( $attr_slug_2 ) ) {
							$possible_attrs[ $attr_set_id ][ $attr_slug_2 ] = array( $item_attrs[1] );
						}
						if ( !empty ( $attr_slug_3 ) ) {
							$possible_attrs[ $attr_set_id ][ $attr_slug_3 ] = array( $item_attrs[2] );
						}
					} else {
						if ( isset( $possible_attrs[ $attr_set_id ][ $attr_slug_1 ] ) ) {
							array_push( $possible_attrs[ $attr_set_id ][ $attr_slug_1 ], $item_attrs[0] );
						}
						if ( isset( $possible_attrs[ $attr_set_id ][ $attr_slug_2 ] ) ) {
							array_push( $possible_attrs[ $attr_set_id ][ $attr_slug_2 ], $item_attrs[1] );
						}
						if ( isset( $possible_attrs[ $attr_set_id ][ $attr_slug_3 ] ) ) {
							array_push( $possible_attrs[ $attr_set_id ][ $attr_slug_3 ], $item_attrs[2] );
						}
					}
				}
			}

			// Make values unique
			foreach( $possible_attrs as $id => $attribute_set ) {
				foreach( $attribute_set as $attr_slug => $attr_val ) {
					$possible_attrs[ $id ][ $attr_slug ] = array_filter( array_unique( $possible_attrs[ $id ][ $attr_slug ] ) );
				}
			}

			return $possible_attrs;
		}

		private function create_variations( $variations, $sync, $parent_id ) {
			foreach( $variations as $ls_variation ) {

				$ls_variation = apply_filters( 'wclsi_create_ls_data_variation', $ls_variation );

				$wc_variation_prod = $this->init_wc_product( $ls_variation, $parent_id );
				$this->set_wc_prod_values( $wc_variation_prod, $ls_variation );
				$post_id = $wc_variation_prod->save();

				if ( !is_wp_error( $post_id ) ) {

					// Get & set the category
					$this->handle_product_taxonomy( $ls_variation, $post_id );

					$attr_set =
						new WCLSI_Lightspeed_Item_Attribute_Set( $ls_variation->item_attributes->itemAttributeSetID );

					for( $i = 1; $i <= self::MAX_NUM_OF_ATTRIBUTES; $i++ ) {
						if ( is_null( $attr_set->{'attribute_name_' . $i} ) ) {
							$attr_slug = 'attribute_name_' . $i;
						} else {
							$attr_slug = sanitize_title( $attr_set->{'attribute_name_' . $i} );
						}
						$attr_val  = $ls_variation->item_attributes->{'attribute' . $i};
						update_post_meta( $post_id, 'attribute_' . $attr_slug, $attr_val );
					}

					// Add a sync flag so our cron-job will know to sync this product
					if ( $sync ) {
						update_post_meta( $post_id, WCLSI_SYNC_POST_META, true );
					}

					// we need to set wc_prod_id for saving images
					$ls_variation->wc_prod_id = $post_id;
					$prod_imgs = apply_filters(
						'wclsi_create_prod_imgs_variation', $this->save_ls_prod_images( $ls_variation ), $post_id
					);

					if ( !is_wp_error( $prod_imgs ) && count( $prod_imgs ) > 0 ) {
						set_post_thumbnail( $post_id, $prod_imgs[0] );
					}

					$this->persist_import_data( $ls_variation, $post_id, $sync );
				}
			}
		}
		
		private function handle_product_taxonomy( $prod, $post_id ) {

			if( $prod->category_id > 0 ) {
				global $wpdb, $WCLSI_ITEM_CATEGORIES_TABLE;
				$table = $wpdb->prefix . $WCLSI_ITEM_CATEGORIES_TABLE;

				$ls_cat = $wpdb->get_row("SELECT * FROM $table WHERE category_id=$prod->category_id");

				if ( !is_null( $ls_cat ) &&  $ls_cat->wc_cat_id > 0 ) {
					$cat_id = intval( $ls_cat->wc_cat_id );
					$cat_ids = get_ancestors( $cat_id, 'product_cat' );
					$cat_ids[] = $cat_id;
					wp_set_object_terms( $post_id, $cat_ids, 'product_cat', true );
				}
			}

			// Set the tags
			if ( !empty( $prod->tags ) ) {
				wp_set_object_terms( $post_id, $prod->tags, 'product_tag', true );
			}
		}
		
		//@todo How to update attributes if they've been changed in Lightspeed?
		private function update_matrix_item( WCLSI_Lightspeed_Prod $ls_matrix_item, $update_variations = true ) {
			$post_id = isset( $ls_matrix_item->wc_prod_id ) ? $ls_matrix_item->wc_prod_id : 0;
			if ( $post_id > 0 ) {

				$wc_product = wc_get_product( $post_id );
				if ( !empty( $wc_product ) ) {
					$wc_product = apply_filters( 'wclsi_update_matrix_item', $wc_product, $ls_matrix_item);
					$this->set_wc_prod_values( $wc_product, $ls_matrix_item );
					$wc_product->save();
				}

				// Get & set the category
				$this->handle_product_taxonomy( $ls_matrix_item, $post_id );

				$prod_imgs = apply_filters(
					'wclsi_update_post_imgs_matrix_item',
					$this->save_ls_prod_images( $ls_matrix_item ),
					$post_id
				);

				if ( !is_wp_error( $prod_imgs) && !empty( $prod_imgs ) && is_array( $prod_imgs ) ) {
					$this->set_wc_images( $post_id, $prod_imgs );
				}
			}

			if ( $update_variations ) {
				$variations = apply_filters(
					'wclsi_update_variations_matrix_item',
					$ls_matrix_item->variations,
					$post_id
				);

				if ( !empty( $variations ) ) {
					$this->update_matrix_variations( $variations, $post_id );
				}
			}

			delete_transient( 'wc_product_children_' . $post_id );

			return $post_id;
		}
		
		// @todo Handle category updates - what happens if $new_ls_prod has a new category?
		private function update_single_item( WCLSI_Lightspeed_Prod $wclsi_item ) {
			$wc_prod_id = isset( $wclsi_item->wc_prod_id ) ? $wclsi_item->wc_prod_id : 0;
			if ( $wc_prod_id > 0 ) {
				$wclsi_item = apply_filters( 'wclsi_update_product', $wclsi_item, $wc_prod_id );

				$wc_product = wc_get_product( $wc_prod_id );
				$this->set_wc_prod_values( $wc_product, $wclsi_item );
				$wc_product->save();

				// Set the category & tags
				$this->handle_product_taxonomy( $wclsi_item, $wc_prod_id );

				// Process images
				$prod_imgs = $this->save_ls_prod_images( $wclsi_item );
				$prod_imgs = apply_filters( 'wclsi_update_prod_imgs_single_item', $prod_imgs, $wc_prod_id );

				// Process images for single items
				if ( !is_wp_error( $prod_imgs ) && !empty( $prod_imgs ) && is_array( $prod_imgs ) ) {
					$this->set_wc_images( $wc_prod_id, $prod_imgs );
				}
			}

			return $wc_prod_id;
		}
	}
	global $WCLSI_PRODS;
	$WCLSI_PRODS = new LSI_Import_Products();
endif;
