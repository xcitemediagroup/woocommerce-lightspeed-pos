<?php
/**
 * Integrations Page. Sets up various options to initialize communication with LightSpeed.
 * @class LSI_Init_Settings
 */
if ( !class_exists('LSI_Init_Settings') ) :

	class LSI_Init_Settings extends WC_Integration {

		const WOO_CONNECT_URL = 'https://connect.woocommerce.com/login/lightspeed/';

		private $init_errors = array();

		function __construct() {
			$this->set_header_info();
			$this->init_settings();

			// Setup OAuth
			$this->check_for_lightspeed_token();
			$this->token = get_option( 'wclsi_oauth_token' );

			$account_id = get_option( 'wclsi_account_id' );
			$this->ls_account_id = $account_id;

			$this->init_store_data();
			$this->init_form_fields();

			// Default to OAuth if we can
			if ( !empty( $this->ls_account_id ) ) {
				if( !empty( $this->token ) ){
					$this->MOSAPI = new WP_MOSAPICall(null, $this->ls_account_id, $this->token);
				}
			}

			add_action( 'woocommerce_update_options_integration_' .  $this->id, array( $this, 'process_admin_options' ) );
			add_action( 'admin_notices', array( $this, 'display_init_errors') );
			add_filter( 'woocommerce_settings_api_sanitized_fields_' . $this->id, array( $this, 'sanitize_settings' ) );
			add_action( 'admin_init', array( $this, 'admin_enqueue_resources' ) );
		}

		/**
		 * Override this method and just return settings
		 * @see process_admin_options()
		 */
		function sanitize_settings( $settings ) {
			return $settings;
		}

		function admin_options() {
			$this->display_errors();
			$this->check_for_reqd_fields();
			?>

			<h3><?php echo isset( $this->method_title ) ? $this->method_title : __( 'Settings', 'woocommerce' ) ; ?></h3>

			<?php echo isset( $this->method_description ) ? wpautop( $this->method_description ) : ''; ?>

			<table class="form-table">
				<?php $this->generate_settings_html(); ?>
			</table>

			<!-- Section -->
			<div><input type="hidden" name="section" value="<?php echo $this->id; ?>" /></div>
		<?php
		}

		function admin_enqueue_resources() {
			global $WCLSI_objectL10n;
			$suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';
			wp_enqueue_script(
				'wclsi-admin-js',
				plugins_url( 'assets/js/wcls-admin' . $suffix . '.js', __FILE__ ),
				array(),
				WC_LSI_VERSION
			);

			wp_localize_script( 'wclsi-admin-js', 'wclsi_admin', array(
				'PLUGIN_PREFIX_ID' => $this->plugin_id . $this->id . '_',
				'SCRIPT_DEBUG'    => defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG
			));

			wp_localize_script( 'wclsi-admin-js', 'objectL10n', $WCLSI_objectL10n );
		}

		/**
		 * Will only display errors if the plugin has initialized
		 */
		function check_for_reqd_fields() {
			$wclsi_initialized = get_option( 'wclsi_initialized' );
			if ( !$wclsi_initialized ) {
				return;
			}

			if ( empty( $this->store_timezone ) ) {
				?>
				<div class="error">
					<p><?php echo __('Could not find store timezone. This is required for syncing products. Please click on "Connect to Lightspeed" before starting the syncing process with Lightspeed.', 'woocommerce-lightspeed-pos'); ?></p>
				</div>
				<?php
			}
		}

		function init_form_fields() {
			$form_fields = array(
				'ls_account_id' => array(
					'title'             => __( 'Account ID', 'woocommerce-lightspeed-pos' ),
					'type'              => 'hidden_custom',
					'desc_tip'          => true,
					'default'           => '',
					'description'       => __(
						'Enter your Lightspeed Cloud Account ID.',
						'woocommerce-lightspeed-pos'
					)
				),
				'connect_to_ls' => array(
					'title'             => __( 'Enable Lightspeed API', 'woocommerce-lightspeed-pos' ),
					'type'              => 'wclsi_ls_button_custom',
					'desc_tip'          => true,
					'default'           => '',
					'description'       => __(
						'Click the button to connect your Lightspeed Retail account to WooCommerce.',
						'woocommerce-lightspeed-pos'
					)
				),
				'ls_enabled_stores' => array(
					'title'             => __( 'Enable Stores', 'woocommerce-lightspeed-pos' ),
					'type'              => 'checkbox_custom',
					'custom_attributes' => '',
					'desc_tip'          => true,
					'description'       => __(
						'Enable the stores you would like to import from.',
						'woocommerce-lightspeed-pos'
					)
				),
				'ls_inventory_store' => array(
					'title'             => __( 'Primary Lightspeed Inventory Store', 'woocommerce-lightspeed-pos' ),
					'type'              => 'inventory_radio_custom',
					'custom_attributes' => '',
					'desc_tip'          => true,
					'description'       => __(
						'This plugin is capable of looking up and pushing inventory to only one of your Lightspeed ' .
						'stores, please select which store you\'d like to sync inventory with.',
						'woocommerce-lightspeed-pos'
					)
				),
				'import_categories' => array(
					'title'             => __( 'Import Lightspeed Categories', 'woocommerce-lightspeed-pos' ),
					'type'              => 'wclsi_import_cats_button',
					'custom_attributes' => '',
					'desc_tip'          => true,
					'description'       => __(
						'This is a one time action that will import existing Lightspeed categories. ' .
						'Newly added Lightspeed categories will not be automatically synced! ',
						'woocommerce-lightspeed-pos'
					)
				),
				'ls_to_wc_auto_load' => array(
					'title'             => __(
						'Product auto-loading (Lightspeed to WooCommerce)',
						'woocommerce-lightspeed-pos'
					),
					'type'              => 'wclsi_ls_to_wc_auto_load',
					'custom_attributes' => '',
					'desc_tip'          => true,
					'description'       => __(
						"Enable continuous auto-loading of Lightspeed products into WooCommerce. " .
						"This will not create any new products. Products will only be loaded into the import table.",
						'woocommerce-lightspeed-pos'
					)
				),
				'wclsi_import_status' => array(
					'title'             => __( 'Product status on import', 'woocommerce-lightspeed-pos' ),
					'type'              => 'wclsi_import_status',
					'custom_attributes' => '',
					'desc_tip'          => true,
					'description'       => __(
						'The product status on import. Publish will make the product immediately visible on your ' .
						'store catalog upon import. Draft will keep the product hidden until it is published.' .
						'This setting will apply to all products on import.',
						'woocommerce-lightspeed-pos'
					)
				),
				'wclsi_wc_selective_sync' => array(
					'title'             => __( 'Selective sync for WooCommerce products', 'woocommerce-lightspeed-pos' ),
					'type'              => 'wclsi_wc_selective_sync',
					'custom_attributes' => '',
					'desc_tip'          => true,
					'description'       => __(
						'Select which product fields should get updated for a WooCommerce product when it syncs ' .
						'data from Lightspeed.',
						'woocommerce-lightspeed-pos'
					)
				),
				'wclsi_ls_selective_sync' => array(
					'title'             => __( 'Selective sync for LightSpeed products', 'woocommerce-lightspeed-pos' ),
					'type'              => 'wclsi_ls_selective_sync',
					'custom_attributes' => '',
					'desc_tip'          => true,
					'description'       => __(
						'Select which product fields should get updated for a Lightspeed product when it syncs ' .
						'data from WooCommerce.',
						'woocommerce-lightspeed-pos'
					)
				)
			);

			$deprecated_form_fields = array(
				'api_key' => array(
					'title'             => __( 'API Key', 'woocommerce-lightspeed-pos' ),
					'type'              => 'text',
					'description'       => __( 'Enter your Lightspeed Cloud API Key.', 'woocommerce-lightspeed-pos' ),
					'desc_tip'          => true,
					'default'           => ''
				),
				'init_api_settings_button' => array(
					'title'             => __( 'Initialize API Settings', 'woocommerce-lightspeed-pos' ),
					'type'              => 'button',
					'custom_attributes' => '',
					'desc_tip'          => true,
					'description'       => __(
						'Initializes integration with Lightspeed. Grabs important information like timezone,' .
						'store name, etc.',
						'woocommerce-lightspeed-pos'
					)
				)
			);

			if( defined( 'WCLSI_ENABLED_DEPRECATED_FIELDS' ) && WCLSI_ENABLED_DEPRECATED_FIELDS ){
				array_merge( $form_fields, $deprecated_form_fields );
			}

			$this->form_fields = $form_fields;
		}

		/*****************
		 * Form Elements *
		 *****************/

		function generate_inventory_radio_custom_html( $key, $data ) {
			$shop_data = get_option( 'wclsi_shop_data' );

			if ( false !== $shop_data && is_object( $shop_data[ 'ls_store_data' ] ) ) {
				$shop_data = $shop_data[ 'ls_store_data' ];
				$field    = $this->plugin_id . $this->id . '_' . $key;

				if ( is_array( $shop_data->Shop ) ) {
					$defaults = array(
						'class'             => 'button-secondary',
						'css'               => '',
						'custom_attributes' => array(),
						'desc_tip'          => false,
						'description'       => '',
						'title'             => '',
					);

					$data = wp_parse_args( $data, $defaults );

					ob_start();
					?>
					<tr valign="top">
						<th scope="row" class="titledesc">
							<?php echo $this->get_tooltip_html( $data ); ?>
							<label for="<?php echo esc_attr( $field ); ?>"><?php echo wp_kses_post( $data['title'] ); ?></label>
						</th>
						<td class="forminp">
							<fieldset>
								<?php echo $this->get_description_html( $data ); ?>
								<?php foreach ( $shop_data->Shop as $store ) : ?>
									<label>
										<input
											type="radio"
											name="<?php echo $field ?>"
											id="<?php echo $field ?>"
											value="<?php echo $store->shopID ?>"
											<?php
											if( isset( $this->settings[ WCLSI_INVENTORY_SHOP_ID ] ) ) {
												if( $this->settings[ WCLSI_INVENTORY_SHOP_ID ] == $store->shopID ){
													echo 'checked';
												}
											}
											?>
										/>
										<?php echo $store->name ?>
									</label>
									<br/>
								<?php endforeach; ?>
							</fieldset>
						</td>
					</tr>
					<?php
					return ob_get_clean();
				} else {
					?>
					<input
						type="hidden"
						name="<?php echo $field ?>"
						id="name="<?php echo $field ?>"
						value="<?php echo $this->settings[ WCLSI_INVENTORY_SHOP_ID ] ?>"
					/>
					<?php
				}
			} else {
				return '';
			}
		}

		function generate_checkbox_custom_html( $key, $data ) {
			$shop_data = get_option( 'wclsi_shop_data' );

			if ( false !== $shop_data && is_object( $shop_data['ls_store_data'] ) ) {
				$shop_data = $shop_data['ls_store_data'];
				if ( is_array( $shop_data->Shop ) ) {
					$field    = $this->plugin_id . $this->id . '_' . $key;
					$defaults = array(
						'class'             => 'button-secondary',
						'css'               => '',
						'custom_attributes' => array(),
						'desc_tip'          => false,
						'description'       => '',
						'title'             => '',
					);

					$data = wp_parse_args( $data, $defaults );
					$enabled_stores = empty( $this->settings['ls_enabled_stores'] ) ? array() : $this->settings['ls_enabled_stores'];
					ob_start();
					?>
					<tr valign="top">
						<th scope="row" class="titledesc">
							<label
								for="<?php echo esc_attr( $field ); ?>"><?php echo wp_kses_post( $data['title'] ); ?></label>
							<?php echo $this->get_tooltip_html( $data ); ?>
						</th>
						<td class="forminp">
							<fieldset>
								<?php echo $this->get_description_html( $data ); ?>
								<?php foreach ( $shop_data->Shop as $store ) : ?>
									<label>
										<input
											type="checkbox"
											name="<?php echo $field ?>[<?php echo $store->name ?>]"
											id="<?php echo $field ?>[<?php echo $store->name ?>]"
											value="<?php echo $store->shopID ?>"
											<?php echo isset( $enabled_stores[ $store->name ] ) ? 'checked' : '' ?>
										/>
										<?php echo $store->name ?>
									</label>
									<br/>
								<?php endforeach; ?>
							</fieldset>
						</td>
					</tr>
					<?php
					return ob_get_clean();
				}
			} else {
				$GLOBALS['hide_save_button'] = true;
				return '';
			}
		}

		function generate_hidden_custom_html( $key, $data ) {
			$wclsi_account_id = get_option( 'wclsi_account_id' );
			ob_start();
			if ( false !== $wclsi_account_id ) {
				?>
				<input type="hidden" id="<?php echo $this->plugin_id . $this->id . '_' . $key; ?>"
					   name="<?php echo $this->plugin_id . $this->id . '_' . $key; ?>"
					   value="<? echo $wclsi_account_id ?>"/>
			<?php
			}
			return ob_get_clean();
		}

		function generate_wclsi_import_cats_button_html( $key, $data ){
			if ( !self::wclsi_initialized() ) { return ''; }

			$field = $this->plugin_id . $this->id . '_' . $key;
			$data = wp_parse_args( $data );
			ob_start();
			?>
			<tr valign="top">
				<th scope="row" class="titledesc">
					<label for="<?php echo esc_attr( $field ); ?>"><?php echo wp_kses_post( $data['title'] ); ?></label>
					<?php echo $this->get_tooltip_html( $data ); ?>
				</th>
				<td class="forminp">
					<fieldset>
						<legend class="screen-reader-text">
							<span><?php echo wp_kses_post( $data['title'] ); ?></span>
						</legend>
						<?php echo $this->get_description_html( $data ); ?>
						<button id="wclsi-import-cats" type="button" class="button-secondary" style="margin-top: 10px;">Import Lightspeed Categories</button>
						<div id="wclsi-import-cats-progress" style="display: inline-block; margin-top: 10px;"></div>
						<?php wp_nonce_field( 'wclsi_nonce', 'wclsi_admin_nonce', false ); ?>
					</fieldset>
				</td>
			</tr>
			<?php
			return ob_get_clean();
		}

		function generate_wclsi_ls_to_wc_auto_load_html( $key, $data ){
			if ( !self::wclsi_initialized() ) { return ''; }

			$field = $this->plugin_id . $this->id . '_' . $key;
			$data = wp_parse_args( $data );

			$ls_to_wc_autoload = isset( $this->settings[$key] ) ? $this->settings[$key] : false;
			$checked = !empty( $ls_to_wc_autoload ) ? 'checked' : '';

			ob_start();
			?>
			<tr valign="top">
				<th scope="row" class="titledesc">
					<label for="<?php echo esc_attr( $field ); ?>"><?php echo wp_kses_post( $data['title'] ); ?></label>
					<?php echo $this->get_tooltip_html( $data ); ?>
				</th>
				<td class="forminp">
					<fieldset>
						<legend class="screen-reader-text">
							<span><?php echo wp_kses_post( $data['title'] ); ?></span>
						</legend>
						<?php echo $this->get_description_html( $data ); ?>
						<input
							type="checkbox"
							style="margin-top: 15px;"
							name="<?php echo $field ?>"
							id="<?php echo $field ?>"
							value="true"
							<?php echo $checked ?>
						/>
					</fieldset>
				</td>
			</tr>
			<?php
			return ob_get_clean();
		}

		function generate_wclsi_ls_button_custom_html( $key, $data ){
			$field = $this->plugin_id . $this->id . '_' . $key;
			$data = wp_parse_args( $data );
			$woo_connect_full_url = add_query_arg(
				array(
					'scopes' => 'all',
					'redirect' => urlencode( urlencode( add_query_arg(
						array(
							'page' => 'wc-settings',
							'tab'  => 'integration',
							'section' => 'lightspeed-integration'
						),
						admin_url('admin.php')
					) ) )
				),
				self::WOO_CONNECT_URL
			);

			$connect_text = !empty( $this->token ) ? __('Reconnect to Lightspeed', 'woocommerce-lightspeed-pos') : __('Connect to Lightspeed', 'woocommerce-lightspeedpos');
			ob_start();
			?>
			<tr valign="top">
				<th scope="row" class="titledesc">
					<label
						for="<?php echo esc_attr( $field ); ?>"><?php echo wp_kses_post( $data['title'] ); ?></label>
					<?php echo $this->get_tooltip_html( $data ); ?>
				</th>
				<td class="forminp">
					<fieldset>
						<legend class="screen-reader-text">
							<span><?php echo wp_kses_post( $data['title'] ); ?></span>
						</legend>
						<?php echo $this->get_description_html( $data ); ?>
						<a href="<?php echo $woo_connect_full_url ?>" class="wclsi-connect-ls"><?php echo $connect_text ?></a>
					</fieldset>
				</td>
			</tr>
			<?php $this->render_api_success_message() ?>
			<?php
			return ob_get_clean();
		}

		function generate_wclsi_import_status_html( $key, $data ){
			if ( !self::wclsi_initialized() ) { return ''; }
			$field = $this->plugin_id . $this->id . '_' . $key;
			$data = wp_parse_args( $data );
			$selected = isset( $this->settings[$key] ) ? $this->settings[$key] : 'draft';
			ob_start();
			?>
			<tr valign="top">
				<th scope="row" class="titledesc">
					<label for="<?php echo esc_attr( $field ); ?>"><?php echo wp_kses_post( $data['title'] ); ?></label>
					<?php echo $this->get_tooltip_html( $data ); ?>
				</th>
				<td class="forminp">
					<div style="max-width: 400px;">
					<fieldset>
						<legend class="screen-reader-text">
							<span><?php echo wp_kses_post( $data['title'] ); ?></span>
						</legend>
						<?php echo $this->get_description_html( $data ); ?>
					</fieldset>
					<select	name="<?php echo $field ?>" id="<?php echo $field ?>" style="width: inherit;">
						<option value="draft"
							<?php
								if ( $selected == 'draft' || empty( $selected ) ) {
									echo 'selected';
								}
							?>>
							Draft (default)
						</option>
						<option value="publish" <?php if ( $selected == 'publish' ) { echo 'selected'; } ?>>
							Publish
						</option>
					</select>
					<?php if ( $selected == 'publish') { ?>
					<div>
						<p class="notice notice-warning">
						<?php
						echo __(
							'Warning: this will make your products live and visible on your store catalog upon import.',
							'woocommerce-lightspeed-pos'
						)
						?>
						</p>
					</div>
					<?php } ?>
					</div>
				</td>
			</tr>
			<?php
			return ob_get_clean();
		}

		function generate_wclsi_wc_selective_sync_html( $key, $data ) {
			if ( !self::wclsi_initialized() ) { return ''; }
			global $WC_PROD_SELECTIVE_SYNC_PROPERTIES;
			$field = $this->plugin_id . $this->id . '_' . $key;
			$data = wp_parse_args( $data );
			$sync_properties = $this->settings['wclsi_wc_selective_sync'];
			ob_start();
			?>
			<tr valign="top">
				<th scope="row" class="titledesc">
					<label for="<?php echo esc_attr( $field ); ?>"><?php echo wp_kses_post( $data['title'] ); ?></label>
					<?php echo $this->get_tooltip_html( $data ); ?>
				</th>
				<td class="forminp">
					<div style="max-width: 400px;">
						<fieldset>
							<legend class="screen-reader-text">
								<span><?php echo wp_kses_post( $data['title'] ); ?></span>
							</legend>
							<?php echo $this->get_description_html( $data ); ?>
							<p id="wclsi_select_all_prod_sync_properties">Select all</p>
							<div id="wc_prod_selective_sync_properties">
							<?php foreach ( $WC_PROD_SELECTIVE_SYNC_PROPERTIES as $key => $property ) : ?>
								<input
									type="checkbox"
									name="<?php echo "{$field}[$key]" ?>"
									id="<?php echo "{$field}[$key]" ?>"
									value="true"
									<?php
									if  ( isset($sync_properties[ $key ]) && $sync_properties[ $key ] == 'true' ) {
										echo 'checked';
									}
									?>
								/>
								<label for="<?php echo "{$field}[$key]" ?>"><?php echo $property ?></label>
								<br/>
							<?php endforeach; ?>
							</div>
						</fieldset>
					</div>
				</td>
			</tr>
			<?php
			return ob_get_clean();
		}

		function generate_wclsi_ls_selective_sync_html( $key, $data ) {
			if ( !self::wclsi_initialized() ) { return ''; }
			global $LS_PROD_SELECTIVE_SYNC_PROPERTIES;
			$field = "{$this->plugin_id}{$this->id}_{$key}";
			$data = wp_parse_args( $data );
			if ( isset( $this->settings[ WCLSI_LS_SELECTIVE_SYNC ] ) ) {
				$sync_properties = $this->settings[ WCLSI_LS_SELECTIVE_SYNC ];
			} else {
				$sync_properties = [];
			}

			ob_start();
			?>
			<tr valign="top">
				<th scope="row" class="titledesc">
					<label for="<?php echo esc_attr( $field ); ?>"><?php echo wp_kses_post( $data['title'] ); ?></label>
					<?php echo $this->get_tooltip_html( $data ); ?>
				</th>
				<td class="forminp">
					<div style="max-width: 400px;">
						<fieldset>
							<legend class="screen-reader-text">
								<span><?php echo wp_kses_post( $data['title'] ); ?></span>
							</legend>
							<?php echo $this->get_description_html( $data ); ?>
							<p id="wclsi_select_all_ls_prod_sync_properties">Select all</p>
							<div id="ls_prod_selective_sync_properties">
								<?php foreach ( $LS_PROD_SELECTIVE_SYNC_PROPERTIES as $key => $property ) : ?>
									<input
											type="checkbox"
											name="<?php echo "{$field}[$key]" ?>"
											id="<?php echo "{$field}[$key]" ?>"
											value="true"
										<?php
										if  ( isset($sync_properties[ $key ]) && $sync_properties[ $key ] == 'true' ) {
											echo 'checked';
										}
										?>
									/>
									<label for="<?php echo "{$field}[$key]" ?>"><?php echo $property ?></label>
									<br/>
								<?php endforeach; ?>
							</div>
						</fieldset>
					</div>
				</td>
			</tr>
			<?php
			return ob_get_clean();
		}

		/**
		 * Override validate_settings_fields so we don't default to validate_text_field()
		 * @see validate_settings_fields()
		 */
		function validate_ls_enabled_stores_field( $key ) {
			$value = isset( $_POST[ $this->plugin_id . $this->id . '_' . $key ] ) ? $_POST[ $this->plugin_id . $this->id . '_' . $key ] : null;
			return $value;
		}

		function validate_ls_to_wc_auto_load_field( $key ) {
			$value = isset( $_POST[ $this->plugin_id . $this->id . '_' . $key ] ) ? $_POST[ $this->plugin_id . $this->id . '_' . $key ] : null;
			return $value;
		}

		function validate_wclsi_wc_selective_sync_field( $key ) {
			$value = isset( $_POST[ $this->plugin_id . $this->id . '_' . $key ] ) ? $_POST[ $this->plugin_id . $this->id . '_' . $key ] : null;
			return $value;
		}

		function validate_wclsi_ls_selective_sync_field( $key ) {
			$value = isset( $_POST[ $this->plugin_id . $this->id . '_' . $key ] ) ? $_POST[ $this->plugin_id . $this->id . '_' . $key ] : null;
			return $value;
		}

		function display_errors() {
			if ( is_array( $this->errors ) ) {
				foreach ($this->errors as $key => $value) {
					?>
					<div class="error">
						<p><?php _e('Looks like you made a mistake with the ' . $value . ' field.', 'woocommerce-lightspeed-pos'); ?></p>
					</div>
				<?php
				}
			}
		}

		function display_init_errors(){
			$displayed = wp_cache_get( 'wclsi_init_errors_displayed' );
			if ( is_array( $this->init_errors ) && !$displayed ) {
				foreach ($this->init_errors as $key => $error) {
					?>
					<div class="error is-dismissible'">
						<p><?php echo $error ?></p>
					</div>
					<?php
				};
				wp_cache_set( 'wclsi_init_errors_displayed', true );
			}
		}

		function check_for_lightspeed_token(){
			if( isset( $_GET['lightspeed_access_token'] ) ){
				$token = esc_attr( $_GET['lightspeed_access_token'] );
				$refresh_token = esc_attr( $_GET['lightspeed_refresh_token'] );
				$expires_in = (int) esc_attr( $_GET['expires_in'] );

				// Setup a separate MerchantOS object since we may not have the account ID yet..
				$ls_api = new WP_MOSAPICall( '', '', $token );

				$this->init_wclsi_settings_with_token( $ls_api, $token, $refresh_token, $expires_in );
			}
		}

		function init_wclsi_settings_with_token( $ls_api, $token, $refresh_token = null, $expires_in = null ){

			// Save account settings
			try {
				$account = $ls_api->makeAPICall('Account', 'Read');
			} catch( Exception $e ) {
				$this->init_errors[] = $e->getMessage();
				return;
			}

			if ( isset( $account->httpCode ) && $account->httpCode != '200' ) {
				$this->init_errors[] =
					sprintf(
						'%s %d %s %s %s %s',
						'Lightspeed API Error - ',
						$account->httpCode,
						$account->message,
						'Read',
						' - Payload: ',
						'/Account'
					);
				return;
			} else {
				$this->check_for_ls_account( $account, $token );
			}

			// Get Shop info
			try {
				$shop = $ls_api->makeAPICall( "Account/{$this->ls_account_id}/Shop/", 'Read' );
			} catch( Exception $e ) {
				$this->init_errors[] = $e->getMessage();
				return;
			}

			if ( isset( $shop->httpCode ) && $shop->httpCode != '200' ) {
				$this->init_errors[] =
					sprintf(
						'%s %d %s %s %s %s',
						'Lightspeed API Error - ',
						$shop->httpCode,
						$shop->message,
						'Read',
						' - Payload: ',
						'/Shop'
					);
				return;
			} else {
				$this->check_for_ls_shop_data( $shop );
			}


			// Plugin is no longer clean, display errors if certain options are not available
			update_option( 'wclsi_initialized', true );
			update_option( 'wclsi_oauth_token', $token );
			update_option( 'wclsi_refresh_token', $refresh_token );

			if ( $expires_in > 0) {
				$wclsi_expires_in = date( "Y-m-d H:i:s", time() + $expires_in );
				update_option( 'wclsi_expires_in', $wclsi_expires_in );
			}
		}

		public static function wclsi_initialized() {
			$wclsi_account_id = get_option( 'wclsi_account_id' );
			return false !== $wclsi_account_id;
		}

		/**
		 * @param $controlname
		 * @param $action
		 * @param string $query_str
		 * @param array $data
		 * @param null $unique_id
		 * @param Closure|null $callback
		 *
		 * @return array|mixed|object|WP_Error
		 */

		function make_api_call( $controlname, $action, $query_str = '', $data = array(), $unique_id = null, Closure $callback = null ) {

			if ( isset( $this->MOSAPI ) ) {
				try{
					$controlname = apply_filters( 'wclsi_api_call_controlname', $controlname);
					$action      = apply_filters( 'wclsi_api_call_action', $action);
					$query_str   = apply_filters( 'wclsi_api_call_query_str', $query_str );
					$data        = apply_filters( 'wclsi_api_call_data', $data );
					$unique_id   = apply_filters( 'wclsi_api_call_unique_id', $unique_id );

					$result = $this->MOSAPI->makeAPICall( $controlname, $action, $unique_id, $data, $query_str, $callback );

					// Handle refresh tokens
					if ( isset( $result->httpCode ) && ( $result->httpCode == '401' ) ) {
						$token_result = $this->refresh_access_token();

						if ( !is_wp_error( $token_result ) ) {
							$this->MOSAPI = new WP_MOSAPICall( '', $this->ls_account_id, $token_result);

							// Try the request again
							$result = $this->MOSAPI->makeAPICall( $controlname, $action, $unique_id, $data, $query_str, $callback );
						} else {
							if( is_admin() ) {
								add_settings_error(
									'wclsi_settings',
									'wclsi_bad_refresh_token_attempt',
									'Error: ' . $token_result->get_error_message(),
									'error'
								);
							}

							return new WP_Error(
								'wclsi_bad_api_call',
								$token_result->get_error_message()
							);
						}
					}

					if ( isset( $result->httpCode ) && ( $result->httpCode == '503' ) ) {
						sleep(61); // Wait for a minute (+1 second, for good measure), and make the call again
						$result = $this->MOSAPI->makeAPICall( $controlname, $action, $unique_id, $data, $query_str, $callback );
					}

					if ( isset( $result->httpCode ) && $result->httpCode != '200' ) {

						$error_msg = sprintf(
							'%s %d %s %s %s %s %s %s',
							__( 'Lightspeed API Error - ', 'woocommerce-lightspeed-pos' ),
							$result->httpCode,
							$result->message,
							$action,
							__( ' - Payload: ', 'woocommerce-lightspeed-pos' ),
							$controlname,
							$unique_id,
							print_r( $query_str, true ),
							print_r( $data, true )
						);

						if( is_admin() ) {
							add_settings_error(
								'wclsi_settings',
								'wclsi_bad_api_call',
								$error_msg,
								'error'
							);
						}

						// Request dump if WP_DEBUG is on
						if ( WP_DEBUG ) {
							global $WCLSI_WC_Logger;
							$WCLSI_WC_Logger->add(
								WCLSI_WC_LOGGER_HANDLE,
								'ERROR: BAD LIGHTSPEED REQUEST: ' .
								print_r( $result, true ) .
								PHP_EOL
							);
						}

						return new WP_Error( 'wclsi_bad_api_call', $error_msg );
					}
					return $result;
				} catch( Exception $e ) {
					if( is_admin() ) {
						add_settings_error(
							'wclsi_settings',
							'wclsi_bad_api_call',
							__( $e->getMessage(), 'woocommerce-lightspeed-pos' ),
							'error'
						);
					}
					return new WP_Error( 'wclsi_bad_api_call', __( $e->getMessage(), 'woocommerce-lightspeed-pos' ) );
				}
			} else {
				$error_msg = __(
					'Could not find a MOSAPI instance to initiate an API call!',
					'woocommerce-lightspeed-pos'
				);

				if( is_admin() ) {
					add_settings_error(
						'wclsi_settings',
						'wclsi_load_ls_cats',
						$error_msg,
						'error'
					);
				}
				return new WP_Error( 'wc_ls_error', $error_msg );
			}
		}

		public function init_settings() {
			parent::init_settings();

			$this->seed_selective_sync();
			$this->seed_autoload();
		}

		/*******************
		 * Private Methods *
		 *******************/

		private function seed_autoload(){
			if ( !array_key_exists( WCLSI_LS_TO_WC_AUTOLOAD, $this->settings ) ) {
				$this->settings[ WCLSI_LS_TO_WC_AUTOLOAD ] = 'true';
			}
		}

		private function seed_selective_sync() {

			if ( !array_key_exists( 'wclsi_wc_selective_sync', $this->settings ) ) {
				global $WC_PROD_SELECTIVE_SYNC_PROPERTIES;

				// default all selective sync properties to true
				$selective_sync_default = array();
				$selective_ls_sync_default = array();

				foreach ( $WC_PROD_SELECTIVE_SYNC_PROPERTIES as $key => $property ) {
					$selective_sync_default[ $key ] = 'true';
				}

				$this->settings['wclsi_wc_selective_sync'] = $selective_sync_default;
			}

			if ( !array_key_exists( WCLSI_LS_SELECTIVE_SYNC, $this->settings ) ) {
				global $LS_PROD_SELECTIVE_SYNC_PROPERTIES;

				// default all selective sync properties to true
				$selective_ls_sync_default[ 'stock_quantity' ] = 'true';
				$this->settings[ WCLSI_LS_SELECTIVE_SYNC ] = $selective_ls_sync_default;
			}
		}

		private function set_header_info() {
			$this->id                 = 'lightspeed-integration';
			$this->method_title       = __( 'WooCommerce Lightspeed POS Integration', 'woocommerce-lightspeed-pos' );
			$this->method_description =
				'<p>' .
				__( 'Import Lightspeed Cloud data to your WooCommerce instance', 'woocommerce-lightspeed-pos' ) . ' | ' .
				'<a href="' . WCLSI_ADMIN_URL . '">Lightspeed Import Page</a>' . ' | ' .
				'<a href="' . WCLSI_DOCS_URL . '" target="_blank">Documentation</a>' . ' | ' .
				'<i>v' . WC_LSI_VERSION . '</i>' .
				'</p>';

			$lightspeed_api_status_url = "http://status.lightspeedhq.com/";
			$lightspeed_api_status_href = "<a href=\"$lightspeed_api_status_url\">$lightspeed_api_status_url</a>";
			$lightspeed_api_notice = __(
				"Note: this plugin is dependent on the Lightspeed Retail API. " .
				"We recommend you subscribe to the Lightspeed Retail API status page: ",
				"woocommerce-lightspeed-pos"
			);

			$lightspeed_api_notice .= $lightspeed_api_status_href;
			$this->method_description .= "<p><b>$lightspeed_api_notice</b></p>";
		}

		private function init_store_data(){
			$shop_data = get_option( 'wclsi_shop_data' );
			if ( !empty( $shop_data ) ) {
				$this->store_timezone = $shop_data[ 'store_timezone' ];
				$this->store_name     = $shop_data[ 'store_name' ];
				if ( isset( $this->settings['ls_enabled_stores'] ) ) {
					$this->ls_enabled_stores = $this->settings['ls_enabled_stores'];
				}
			}
		}

		private function render_api_success_message(){
			if ( wclsi_oauth_enabled() && !empty( $this->store_name ) && !empty( $this->store_timezone ) ) {
				echo sprintf(
					'<p id="wclsi-api-status" style="%s">%s<strong>%s</strong>, %s<strong>%s</strong></p>',
					'color: green; display: inline-block;',
					__( ' API Settings successfully initialized! Store name(s): ', 'woocommerce-lightspeed-pos' ),
					$this->store_name,
					__( 'Time Zone: ', 'woocommerce-lightspeed-pos' ),
					$this->store_timezone
				);
			}
		}

		private function check_for_ls_account( $account_response, $token ){
			if ( isset( $account_response->Account->accountID ) ) {

				// Save the token
				$this->token = $token;
				$wc_options  = get_option( $this->plugin_id . $this->id . '_settings' );

				// Delete the deprecated API Key if it exists
				if( isset( $wc_options['api_key'] ) ){
					unset($wc_options['api_key']);
				}

				// Reset multi-store setting if we're changing accounts
				if ( isset( $this->ls_account_id ) && $this->ls_account_id != $account_response->Account->accountID ) {
					unset( $wc_options['ls_enabled_stores'] );
				}

				update_option( $this->plugin_id . $this->id . '_settings', $wc_options );

				$this->ls_account_id = $account_response->Account->accountID;

				// Save the account ID
				update_option( 'wclsi_account_id', $account_response->Account->accountID );
			} else {
				$this->init_errors[] = __( 'Could not find an account associated with this API key.', 'woocommerce-lightspeed-pos' );
				return;
			}
		}

		private function check_for_ls_shop_data( $shop_response ){

			// Note: in the case of multiple shops, the time-zone will default to the first store
			if ( !empty( $shop_response ) ) {

				// Add it to the settings
				$shop_data = array();
				$shop_data['store_timezone'] = is_array( $shop_response->Shop ) ? $shop_response->Shop[0]->timeZone : $shop_response->Shop->timeZone;
				$shop_data['store_name'] = is_array( $shop_response->Shop ) ? 'Multiple' : $shop_response->Shop->name;
				$shop_data['ls_store_data'] = $shop_response;

				if ( is_object( $shop_response->Shop ) ) {
					$wc_options = get_option( $this->plugin_id . $this->id . '_settings' );
					$wc_options['ls_enabled_stores'] = array($shop_response->Shop->name => $shop_response->Shop->shopID);
					$wc_options[WCLSI_INVENTORY_SHOP_ID] = $shop_response->Shop->shopID;
					update_option( $this->plugin_id . $this->id . '_settings', $wc_options );
				}

				update_option( 'wclsi_shop_data', $shop_data );
			}
		}

		private function refresh_access_token() {
			$woo_connector_url = add_query_arg(
				array( 'refresh_token' => get_option('wclsi_refresh_token') ),
				WCLSI_REFRESH_CONNECTOR_URL
			);

			$result = wp_remote_post( $woo_connector_url );

			if ( $result['response']['code'] == 200 ) {
				$response = json_decode( $result['body'] );

				if ( isset( $response->access_token ) ) {
					update_option('wclsi_oauth_token', $response->access_token );
					return $response->access_token;
				} elseif ( isset( $response->error ) && isset( $response->reason ) ) {
					return new WP_Error('could_not_retrieve_access_token', $response->reason, $result);
				}
			}

			return new WP_Error(
				'could_not_retrieve_access_token',
				'Something went wrong with refreshing you access token. Please contact support!',
				$result
			);
		}
	}
	global $WCLSI_API;
	$WCLSI_API = new LSI_Init_Settings();
endif;
