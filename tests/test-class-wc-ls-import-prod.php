<?php
/**
 * Class LSI_Import_Products_Test
 *
 * @package Woocommerce_Lightspeed_Integration
 */

class LSI_Import_Products_Test extends WP_UnitTestCase {

	protected $single_item_api_respone = null;
	protected $matrix_item_api_respone = null;

	/**
	 * @var WCLSI_Lightspeed_Prod
	 */
	protected $wclsi_single_prod = null;

	/**
	 * @var WCLSI_Lightspeed_Prod
	 */
	protected $single_prod_with_empty_props = null;

	/**
	 * @var WC_Product_Simple
	 */
	protected $wc_simple_prod = null;

	/**
	 * @var WCLSI_Lightspeed_Prod
	 */
	protected $wclsi_matrix_prod = null;

	/**
	 * @var WC_Product_Variation
	 */
	protected $wc_variable_prod = null;

	/**
	 * Setup
	 */

	function setUp(){
		$this->single_item_api_respone = $this->load_single_item_xml();
		$this->matrix_item_api_respone = $this->load_matrix_item_xml();
		$this->single_prod_with_empty_props = $this->load_single_item_with_empty_props_xml();

		$wclsi_single_prod_id          = WCLSI_Lightspeed_Prod::insert_ls_api_item( $this->single_item_api_respone );
		$wclsi_matrix_prod_id          = WCLSI_Lightspeed_Prod::insert_ls_api_item( $this->matrix_item_api_respone );

		$this->wclsi_single_prod       = new WCLSI_Lightspeed_Prod( $wclsi_single_prod_id );
		$this->wclsi_matrix_prod       = new WCLSI_Lightspeed_Prod( $wclsi_matrix_prod_id );
	}

	function tearDown(){
		if ( !is_null( $this->wclsi_single_prod ) ) {
			$this->wclsi_single_prod->delete();
			$this->wclsi_single_prod = null;
		}

		if ( !is_null( $this->wclsi_matrix_prod ) ) {
			$this->wclsi_matrix_prod->delete();
			$this->wclsi_matrix_prod = null;
		}

		if ( !is_null( $this->wc_simple_prod ) ) {
			$this->wc_simple_prod->delete(true);
			$this->wc_simple_prod = null;
		}

		if ( !is_null( $this->wc_variable_prod ) ) {
			$this->wc_variable_prod->delete(true);
			$this->wc_variable_prod = null;
		}
	}

	public static function setUpBeforeClass(){
		self::setUpCategories();
	}

	public static function tearDownAfterClass(){
		global $wpdb, $WCLSI_ITEM_CATEGORIES_TABLE;
		$table_name = $wpdb->prefix . $WCLSI_ITEM_CATEGORIES_TABLE;
		$wpdb->query( "TRUNCATE TABLE $table_name" );
	}

	private static function setUpCategories(){
		$ls_api_cat_result = self::load_cat_xml();
		foreach( $ls_api_cat_result->Category as $ls_cat) {
			LSI_Import_Categories::insert_ls_api_cat( $ls_cat );
		}

		$ls_import_cat_utility = new LSI_Import_Categories;
		$ls_import_cat_utility->generate_ls_categories();
	}

	private function load_single_item_xml() {
		$result = simplexml_load_file( dirname( __FILE__ ) . '/fixtures/item.xml' );
		return json_decode( json_encode( $result ) );
	}

	private function load_single_item_with_empty_props_xml() {
		$result = simplexml_load_file( dirname( __FILE__ ) . '/fixtures/item.empty_props.xml' );
		return json_decode( json_encode( $result ) );
	}

	private function load_matrix_item_xml() {
		$result = simplexml_load_file( dirname( __FILE__ ) . '/fixtures/item_matrix.xml' );
		return json_decode( json_encode( $result ) );
	}

	private static function load_cat_xml() {
		$result = simplexml_load_file( dirname( __FILE__ ) . '/fixtures/item_categories.xml' );
		return json_decode( json_encode( $result ) );
	}

	function import_product($prod, $sync = false, $img_flag = true) {
		$import_utility = new LSI_Import_Products;
		return $import_utility->import_item( $prod, $sync, $img_flag );
	}

	function update_product ($prod ) {
		$import_utility = new LSI_Import_Products;
		return $import_utility->update_wc_prod( $prod );
	}

	/**
	 * Tests
	 */

	function test_import_single_product(){
		$post_id = $this->import_product( $this->wclsi_single_prod );
		$post = get_post( $post_id );
		$wclsi_item_id_post_meta = get_post_meta( $post_id, WCLSI_SINGLE_ITEM_ID_POST_META, true );

		$this->assertNotNull( $post );
		$this->assertEquals( $wclsi_item_id_post_meta, $this->wclsi_single_prod->item_id );
		$this->assertEquals( $post->post_title, $this->wclsi_single_prod->description );

		$product = new WC_Product($post_id);

		$this->assertEquals( $this->wclsi_single_prod->prices[0]->amount, $product->get_regular_price() );
		$this->assertEquals( $this->wclsi_single_prod->prices[1]->amount, $product->get_sale_price() );
	}

	function test_inserted_item_price(){
		$this->assertEquals(
			$this->wclsi_single_prod->prices[0]->amount,
			$this->single_item_api_respone->Prices->ItemPrice[0]->amount
		);

		$wclsi_sale_price = wclsi_get_lightspeed_sale_price( $this->wclsi_single_prod );
		$this->assertEquals( $wclsi_sale_price, '10.99' );
	}

	function test_import_categories(){
		$terms = get_terms( array(
			'taxonomy' => 'product_cat',
			'hide_empty' => false,
			'number' => 0
		) );

		// includes "Uncategorized"
		$this->assertEquals( count($terms), 101);
	}

	function test_product_categories(){
		$post_id = $this->import_product( $this->wclsi_single_prod );
		$terms = wp_get_post_terms($post_id, 'product_cat');

		// includes "Uncategorized"
		$this->assertEquals( count($terms), 3 );
	}

	function test_selective_sync_on_import() {
		global $WCLSI_API;
		$WCLSI_API->settings[ 'wclsi_wc_selective_sync' ] = array( 'name' => 'true' );

		$post_id = $this->import_product( $this->wclsi_single_prod );
		$wc_prod = wc_get_product( $post_id );

		$this->assertEquals( $wc_prod->get_name(), $this->wclsi_single_prod->description );

		$item_e_commerce = $this->wclsi_single_prod->item_e_commerce;

		$this->assertEquals( $wc_prod->get_description(), $item_e_commerce->long_description );
		$this->assertEquals( $wc_prod->get_short_description(), $item_e_commerce->short_description );
		$this->assertEquals( $wc_prod->get_weight(), $item_e_commerce->weight );
		$this->assertEquals( $wc_prod->get_height(), $item_e_commerce->height );
		$this->assertEquals( $wc_prod->get_length(), $item_e_commerce->length );
		$this->assertEquals( $wc_prod->get_width(), $item_e_commerce->width );

		$wclsi_sale_price = wclsi_get_lightspeed_sale_price( $this->wclsi_single_prod );
		$wclsi_regular_price = wclsi_get_lightspeed_price( $this->wclsi_single_prod );
		$this->assertEquals( $wc_prod->get_regular_price(), $wclsi_regular_price );
		$this->assertEquals( $wc_prod->get_sale_price(), $wclsi_sale_price );

		$stocky_quantity = wclsi_get_lightspeed_inventory( $this->wclsi_single_prod );
		$this->assertEquals( $wc_prod->get_stock_quantity(), $stocky_quantity );
	}

	/**
	 * Product updates
	 */

	function test_single_product_update() {
		$prod_id = $this->import_product( $this->wclsi_single_prod );
		$wc_prod = wc_get_product( $prod_id );

		$this->wclsi_single_prod->reload();

		$item_e_commerce = $this->wclsi_single_prod->item_e_commerce;
		$this->assertEquals( $this->wclsi_single_prod->wc_prod_id, $prod_id );
		$this->assertEquals( $wc_prod->get_description(), $item_e_commerce->long_description );
		$this->assertEquals( $wc_prod->get_short_description(), $item_e_commerce->short_description );

		$update_api_item = $this->load_single_item_with_empty_props_xml();
		WCLSI_Lightspeed_Prod::update_via_api_item( $update_api_item, $this->wclsi_single_prod );

		$wc_prod->set_description('foo');
		$wc_prod->set_short_description('bar');
		$wc_prod->save();

		$this->update_product( $this->wclsi_single_prod );

		// Empty props from the Lightspeed side will not overwrite existing wc props
		$this->assertEquals( $wc_prod->get_description(), 'foo' );
		$this->assertEquals( $wc_prod->get_short_description(), 'bar' );
	}
}